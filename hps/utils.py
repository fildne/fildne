"""Functions to operate with hyper parameter search."""
import importlib
import json
import os
from pathlib import Path

import hyperopt as hopt
import matplotlib
import networkx as nx
import numpy as np
from matplotlib import pyplot as plt
from sklearn import metrics as sk_mtr
from tqdm import tqdm

import hps.visualisation as hpsv
from dgem.tasks import graph_reconstruction as gr
from dgem.tasks import models as models
from dgem.tools import io


def output_result(
    trials: hopt.Trials, best_configuration: dict,
    output_path: str, target: str
) -> None:
    """Plot and save all results from the hyperparameter search."""
    output_path = Path(output_path)

    previous_plt_backend = matplotlib.get_backend()
    if len(os.environ.get('DISPLAY', '')) == 0:
        plt.switch_backend('Agg')

    # Save best config
    best_configuration = {k: v.item() for k, v in best_configuration.items()}
    (output_path / 'best_config.json').write_text(
        json.dumps(best_configuration)
    )

    # Save metrics for best config
    (output_path / 'metrics.txt').write_text(
        json.dumps(trials.best_trial['result'])
    )

    # Save results in time
    all_runs = trials.results

    fig, _ = hpsv.get_learning_curves_for_budgets(all_runs, 'loss')
    fig.savefig((output_path / "losses_over_time.pdf").as_posix())

    if target == 'gr':
        fig, _ = hpsv.get_learning_curves_for_budgets(all_runs, 'map')
        fig.savefig((output_path / "map_over_time.pdf").as_posix())

        fig, _ = hpsv.get_learning_curves_for_budgets(all_runs, 'distortion')
        fig.savefig((output_path / "dist_over_time.pdf").as_posix())

    elif target == 'lp':
        fig, _ = hpsv.get_learning_curves_for_budgets(all_runs, 'auc')
        fig.savefig((output_path / "auc_over_time.pdf").as_posix())

    plt.switch_backend(previous_plt_backend)


def create_embedding_model(module, config, emb_dim, model_args):
    """Imports class from given module."""
    module, cls = module.rsplit('.', maxsplit=1)
    model_cls = getattr(importlib.import_module(module), cls)
    model = model_cls(dimensions=emb_dim, **config, **model_args)
    return model


def eval_metrics_per_graph(graph_path, sp_path, emb):
    """Evaluates metrics per graph."""
    graph, graph_dists, mapping = gr.preprocessing.preprocess_data(
        graph_path=graph_path, sp_path=sp_path
    )

    emb_mtx = gr.preprocessing.convert_embedding_to_mtx(emb, mapping)

    mtx = gr.preprocessing.calculate_gr_metrics(
        graph=graph, graph_dists=graph_dists, emb_mtx=emb_mtx
    )

    return mtx['mAP'], mtx['distortion']


def list_dir(path):
    """Lists `path` ignoring entries starting with `.`, such as `.gitignore`."""
    dirs = []
    for directory in os.listdir(path):
        if directory[0] != '.':
            dirs.append(directory)

    return dirs


def calculate_sram(ds_name, path):
    """Calculates spectral radius of adjacency matrix of a given dataset."""
    min_sram = np.Inf
    for type in ['partial', 'full']:
        d_path = os.path.join(path, type, ds_name)
        for ss in tqdm(
                list_dir(d_path),
                desc=f'{type.upper()} initial processing',
                leave=False
        ):
            graph = io.readers.read_pickle(os.path.join(d_path, ss))
            if nx.is_directed(graph):
                graph = nx.DiGraph(graph)
            else:
                graph = nx.Graph(graph)
            adj = nx.to_numpy_matrix(graph)
            min_sram = np.min(
                [np.max(np.abs(np.linalg.eigvals(adj))), min_sram]
            )
    return min_sram


def calculate_on2v_params(ds_name):
    """Calculates OnlineNode2vec params for a given dataset."""
    g = nx.read_gpickle(f'data/graphs/{ds_name}.gpkl')
    eg = list(g.edges(data=True))
    eg = [e[2]['timestamp'] for e in eg]
    time_span = max(eg) - min(eg)

    half_life = time_span / (15 * 12)
    interval = time_span / 15

    return half_life, interval


def eval_lp(emb, dataset):
    """Evaluates link prediction."""
    x_train, y_train = dataset[0], dataset[1]
    x_test, y_test = dataset[2], dataset[3]

    # Prepare validation embedding
    vm = models.LogisticRegressionModel(emb)
    vm.fit(x_train, y_train)
    y_pred_score = vm.predict_proba(x_test)[:, 1]
    auc = sk_mtr.roc_auc_score(
        y_true=y_test,
        y_score=y_pred_score,
    )
    return auc
