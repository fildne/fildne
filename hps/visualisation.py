"""Functions to visualise hyper-parameter search."""
from typing import Sequence, Tuple

import numpy as np
from matplotlib import pyplot as plt


def get_learning_curves_for_budgets(
    runs: Sequence[dict],
    measure: str,
) -> Tuple[plt.Figure, plt.Axes]:
    """Plots AUC curve over time during hyperparameter search.

    :param runs: List of result dicts.
    :param measure: Name of measurement.
    :return: A figure and an axis which was to plot the results on.
    """
    vals = [res[measure] if res['status'] == 'ok' else np.nan for res in runs]
    fig, ax = plt.subplots(figsize=(18, 12))
    ax.plot(vals)

    ax.set_title(f"{measure.upper()} in time")
    ax.set_xlabel("Step")
    ax.set_ylabel(f"{measure.upper()}")
    return fig, ax
