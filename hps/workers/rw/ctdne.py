"""Worker for CTDNE embedding."""
from typing import Dict, Any

import hyperopt as hopt

from hps.workers import base


class CTDNEWorker(base.BaseWorker):
    """Definition of hyperparameters search space for CTDNE embedding."""

    def __init__(self, *args, **kwargs):
        """Inits CTDNEWorker."""
        super().__init__(**kwargs)
        self.module = 'dgem.embedding.random_walk.ctdne.CTDNEEmbedding'

    def get_configspace(self) -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
        cs = {
            'nb_walks_per_node': hopt.hp.choice(
                'nb_walks_per_node', list(range(10, 50 + 1, 10))
            ),
            'min_length': hopt.hp.uniformint('min_length', 5, 7),
            'max_length': hopt.hp.choice(
                'max_length', list(range(10, 100 + 1, 10))
            ),
        }

        return cs
