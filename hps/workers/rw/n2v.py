"""Worker for Node2vec embedding."""
from typing import Dict, Any

import hyperopt as hopt

from hps.workers import base


class N2VWorker(base.BaseWorker):
    """Definition of hyperparameters search space for Node2vec embedding."""

    def __init__(self, *args, **kwargs):
        """Inits HPS Worker."""
        super().__init__(**kwargs)
        self.module = 'dgem.embedding.random_walk.n2v.Node2VecEmbedding'

    def get_configspace(self) -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
        cs = {
            'p': hopt.hp.quniform('p', 0, 4.0, 0.25),
            'q': hopt.hp.quniform('q', 0, 4.0, 0.25),
            'nb_walks_per_node': hopt.hp.choice(
                'nb_walks_per_node', list(range(10, 100 + 1, 10))
            ),
            'walk_length': hopt.hp.choice(
                'walk_length', list(range(10, 100 + 1, 10))
            )
        }

        return cs
