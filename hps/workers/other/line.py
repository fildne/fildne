"""Worker for LINE embedding."""
from typing import Dict, Any

import hyperopt as hopt

from hps.workers import base


class LINEWorker(base.BaseWorker):
    """Definition of hyperparameters search space for HOPE embedding."""

    def __init__(self, *args, **kwargs):
        """Inits HPS Worker."""
        super().__init__(**kwargs)
        self.module = 'dgem.embedding.other.line.LINE'

    def get_configspace(self) -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
        cs = {
            'negative_samples': hopt.hp.choice(
                'negative_samples', list(range(2, 10 + 1, 1))
            ),
            'epochs': hopt.hp.uniformint('epochs', 10, 100),
        }

        return cs
