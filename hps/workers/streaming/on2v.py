"""Worker for OnlineNode2vec embedding."""
import abc
import os
from typing import Dict, Any

import hyperopt as hopt
import networkx as nx
import numpy as np

import hps.utils as hpsu
from dgem.tools import io
from dgem.embedding.onlinen2v import wrappers as on2v_wrap
from dgem.embedding import base as km


class OnlineNode2VecWorker:
    """Worker used for running hyperparameter search in OnlineNode2vec."""

    def __init__(
            self,
            model_name: str,
            dataset_name: str,
            snapshots_path: str,
            lp_dataset_paths: str,
            emb_dim: int,
            budget_size: int,
            half_life: float,
            interval: float,
            model_args: Dict = None,
            **kwargs
    ):
        """Instantiates the class."""
        self.dataset_name = dataset_name
        self.model_name = model_name
        self.model_args = model_args or {}
        self.budget_size = budget_size
        self.dim = emb_dim

        self._snapshots_path = snapshots_path
        self._lp_ds_path = lp_dataset_paths

        self._half_life = half_life
        self._interval = interval

    def step(self, config: Dict[str, Any]) -> Dict[str, Any]:
        """Perform hyperparameter set evaluation.

        The loss is always minimized in the function.

        :param config: Dict of new parameters sampled from the space
            configuration.
        :return: Dict of loss under `loss` key, and any additional info that
            is not used during hyperparameter computation per se.
        """
        # Read original configuration
        print("Config:")
        print(config)

        try:
            # Prepare output metrics
            results = []
            last_idxs = np.random.choice(
                a=range(2, 10),
                size=self.budget_size,
                replace=False,
            )

            for last_idx in last_idxs:
                graphs = [
                    io.readers.read_pickle(os.path.join(
                        f'{self._snapshots_path}/partial/'
                        f'{self.dataset_name}/G_{idx}_{idx + 1}'
                    ))
                    for idx in range(last_idx)
                ]
                dataset = io.readers.read_pickle(
                    f'{self._lp_ds_path}/DS_{last_idx}_{last_idx + 1}'
                )

                if isinstance(graphs[0], (nx.DiGraph, nx.MultiDiGraph)):
                    mirror = False
                else:
                    mirror = True

                on2v = on2v_wrap.create_model(
                    model_name=self.model_name,
                    args={**self.model_args, **config},
                    emb_dim=self.dim,
                    mirror=mirror,
                )
                partial_data = on2v_wrap.init(
                    on2v=on2v,
                    graph_df=on2v_wrap.snapshots_to_df(graphs),
                )

                emb = on2v_wrap.embed(
                    on2v=on2v,
                    snapshot_data=partial_data
                )
                emb = emb.set_index('index')
                emb = {
                    str(vec[0]): np.array(vec[1:])
                    for vec in emb[1:].itertuples()
                }
                emb = km.KeyedModel(
                    size=self.dim,
                    node_emb_vectors=emb,
                )
                results.append(hpsu.eval_lp(emb, dataset))

            output = {
                "loss": np.mean([1 - result for result in results]),
                "auc": np.mean(results),
                'status': hopt.STATUS_OK,
            }

        except Exception as e:  # noqa: E722
            output = {
                'status': hopt.STATUS_FAIL,
            }
            print(e)

        print("Output")
        print(output)

        return output

    @abc.abstractmethod
    def get_configspace(self) -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
