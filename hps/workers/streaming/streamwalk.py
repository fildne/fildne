"""Worker for OnlineNode2vec (Streamwalk) embedding."""
from typing import Any, Dict

import hyperopt as hopt

from hps.workers.streaming import on2v as base


class StreamWalkWorker(base.OnlineNode2VecWorker):
    """Definition of hyperparameters search space for StreamWalk embedding."""

    def __init__(self, *args, **kwargs):
        """Inits StreamWalkWorker."""
        super().__init__(*args, **kwargs)

    def get_configspace(self) -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
        cs = {
            'half-life': hopt.hp.uniform(
                'half-life', 0.5 * self._half_life, 2 * self._half_life
            ),
            'interval': hopt.hp.uniform(
                'interval', 0.5 * self._interval, 2 * self._interval
            ),
            'is-decayed': hopt.hp.choice('is-decayed', [False, True]),
            'is-fw': hopt.hp.choice('is-fw', [False, True]),
            'onlymirror': hopt.hp.choice('onlymirror', [False, True]),
            'max-length': hopt.hp.choice('max-length', [2, 3, 4]),
            'k': hopt.hp.choice('k', [4, 8, 12]),
            'beta': hopt.hp.uniform('beta', 0.5, 0.9),
            'lr-rate': hopt.hp.quniform('lr-rate', 0.0001, 0.1, 0.01),
            'neg-rate': hopt.hp.choice('neg-rate', [5, 10, 15]),
        }

        return cs
