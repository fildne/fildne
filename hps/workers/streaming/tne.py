"""Worker for Node2vec embedding."""
import os
from copy import deepcopy as copy
from typing import Dict, Any

import hyperopt as hopt
import networkx as nx
import numpy as np

from dgem.embedding.tnodeembed.src.wrapper import TNEEmbedding
from dgem.tools import io


class TNEWorker:
    """Worker used for running hyperparameter search in tNodeEmbed."""

    def __init__(
            self,
            model_name: str,
            dataset_name: str,
            snapshots_path: str,
            lp_dataset_paths: str,
            model_args: Dict = None,
            budget_size: int = 3,
            **kwargs
    ):
        """Instantiates the class.

        :param config_path: Path to the original config file of the model.
        :param model_name: Name of the model. It should be one of the names
            used across the project, ex. 'xgb' or 'lgb'.
        :param dataset_name: Name of features to use during parameter search.
            It should be of the names used across the project, ex. 'pure'.
        :param args: Additional positional parameters for Worker configuration.
        :param kwargs: Additional keyword parameters for Worker configuration.
        """
        self.dataset_name = dataset_name
        self.model_name = model_name
        self.model_args = model_args or {}
        self.budget_size = budget_size

        self.data = []
        self._lp_ds_path = lp_dataset_paths

    def step(self, config: Dict[str, Any]) -> Dict[str, Any]:
        """Perform hyperparameter set evaluation.

        The loss is always minimized in the function.

        :param config: Dict of new parameters sampled from the space
            configuration.
        :return: Dict of loss under `loss` key, and any additional info that
            is not used during hyperparameter computation per se.
        """
        # Read original configuration
        print("Config:")
        print(config)

        try:
            # Prepare output metrics
            results = []

            for last_idx in np.random.choice(
                a=range(2, 10),
                size=self.budget_size,
                replace=False,
            ):
                run_cfg = copy(config)

                embs = [
                    io.readers.read_pickle(
                        f'./data/emb/rw/n2v/partial/'
                        f'{self.dataset_name}/0/F_0_1'
                    )
                ]
                embs.extend([
                    io.readers.read_pickle(
                        f'./data/emb/rw/n2v/full/'
                        f'{self.dataset_name}/0/F_0_{idx + 1}'
                    )
                    for idx in range(1, last_idx)
                ])
                graphs = [
                    io.readers.read_pickle(os.path.join(
                        f'./data/snapshots/partial/'
                        f'{self.dataset_name}/G_{idx}_{idx + 1}'
                    ))
                    for idx in range(last_idx)
                ]
                dataset = io.readers.read_pickle(
                    f'{self._lp_ds_path}/DS_{last_idx}_{last_idx + 1}'
                )
                dataset = {
                    'x_train': dataset[0],
                    'y_train': dataset[1],
                    'x_test': dataset[2],
                    'y_test': dataset[3]
                }
                directed = False
                if nx.is_directed(graphs[-1]):
                    directed = True

                batch_size = run_cfg.pop('batch_size')
                tne = TNEEmbedding(
                    args=run_cfg,
                    directed=directed,
                    verbose=0
                )
                res, times = tne.eval(
                    snapshots=graphs,
                    embeddings=embs,
                    lp_dataset=dataset,
                    batch_size=batch_size,
                    task='TLP'
                )

                results.append(res.as_flat_dict()['auc'])

            output = {
                "loss": np.mean([1 - result for result in results]),
                "auc": np.mean(results),
                'status': hopt.STATUS_OK,
            }

        except Exception as e:  # noqa: E722
            output = {
                'status': hopt.STATUS_FAIL,
            }
            print(e)

        print("Output")
        print(output)

        return output

    @staticmethod
    def get_configspace() -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
        cs = {
            'batch_size': hopt.hp.choice(
                'batch_size', [8, 16, 32, 64, 128, 256]
            ),
            'epochs': hopt.hp.uniformint(
                'epochs', 5, 20
            ),
            'learning_rate': hopt.hp.quniform(
                'learning_rate', 0.0001, 0.1, 0.01
            ),
            'do_align': hopt.hp.choice(
                'do_align', [True, False]
            ),
        }

        return cs
