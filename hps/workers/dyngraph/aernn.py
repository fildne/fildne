"""Worker for Node2vec embedding."""
import os
from copy import deepcopy as copy
from typing import Dict, Any

import hyperopt as hopt
import numpy as np

import hps.utils as hpsu
from dgem.embedding.dyngraph.aernn import DynGraphAERNN
from dgem.tools import io


class DynGraphWorker:
    """Worker used for running hyperparameter search in tNodeEmbed."""

    def __init__(
            self,
            model_name: str,
            dataset_name: str,
            snapshots_path: str,
            lp_dataset_paths: str,
            emb_dim: int,
            model_args: Dict = None,
            budget_size: int = 3,
            **kwargs
    ):
        """Instantiates the class.

        :param config_path: Path to the original config file of the model.
        :param model_name: Name of the model. It should be one of the names
            used across the project, ex. 'xgb' or 'lgb'.
        :param dataset_name: Name of features to use during parameter search.
            It should be of the names used across the project, ex. 'pure'.
        :param args: Additional positional parameters for Worker configuration.
        :param kwargs: Additional keyword parameters for Worker configuration.
        """
        self.dataset_name = dataset_name
        self.model_name = model_name
        self.model_args = model_args or {}
        self.budget_size = budget_size
        self.dim = emb_dim
        self._lp_ds_path = lp_dataset_paths

    def step(self, config: Dict[str, Any]) -> Dict[str, Any]:
        """Perform hyperparameter set evaluation.

        The loss is always minimized in the function.

        :param config: Dict of new parameters sampled from the space
            configuration.
        :return: Dict of loss under `loss` key, and any additional info that
            is not used during hyperparameter computation per se.
        """
        # Read original configuration
        print("Config:")
        print(config)

        try:
            # Prepare output metrics
            results = []

            for last_idx in np.random.choice(
                    a=range(2, 10),
                    size=self.budget_size,
                    replace=False
            ):
                run_cfg = copy(config)

                graphs = [
                    io.readers.read_pickle(os.path.join(
                        f'./data/snapshots/partial/'
                        f'{self.dataset_name}/G_{idx}_{idx + 1}'
                    ))
                    for idx in range(last_idx)
                ]
                dataset = io.readers.read_pickle(
                    f'{self._lp_ds_path}/DS_{last_idx}_{last_idx + 1}'
                )
                graph_nodes = list(io.readers.read_pickle(
                    os.path.join(
                        f'./data/snapshots/full/',
                        f'{self.dataset_name}/G_0_{last_idx}')
                ).nodes())

                model = DynGraphAERNN(
                    dimensions=self.dim,
                    **run_cfg
                )
                emb = model.embed(graphs, graph_nodes)
                auc = hpsu.eval_lp(emb, dataset)
                results.append(auc)

            output = {
                "loss": np.mean([1 - result for result in results]),
                "auc": np.mean(results),
                'status': hopt.STATUS_OK,
            }

        except Exception as e:  # noqa: E722
            output = {
                'status': hopt.STATUS_FAIL,
            }
            print(e)
            breakpoint()

        print("Output")
        print(output)

        return output

    @staticmethod
    def get_configspace() -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
        cs = {
            'beta': hopt.hp.uniformint(
                'beta', 5, 200
            ),
            'n_batch': hopt.hp.choice(
                'n_batch', [64, 128, 256]
            ),
            'n_iter': hopt.hp.uniformint(
                'n_iter', 3, 10
            ),
            'xeta': hopt.hp.quniform(
                'xeta', 0.0001, 0.1, 0.01
            )
        }

        return cs
