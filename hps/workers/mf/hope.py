"""Worker for HOPE embedding."""
from typing import Dict, Any

import hyperopt as hopt

from hps.workers import base


class HOPEWorker(base.BaseWorker):
    """Definition of hyperparameters search space for HOPE embedding."""

    def __init__(self, max_beta, *args, **kwargs):
        """Inits HPS Worker."""
        super().__init__(**kwargs)
        self.max_beta = max_beta
        self.module = 'dgem.embedding.mf.HOPEEmbedding'

    def get_configspace(self) -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
        cs = {
            'beta': hopt.hp.uniform('beta', 0, self.max_beta),
        }

        return cs
