"""Handles base hyper-parameter search worker."""
import abc
import os
from typing import Dict, Any

import hyperopt as hopt
import numpy as np

import hps.utils as hpsu
from hps import workers as hpsw
from dgem.tools import io


class BaseWorker:
    """Base worker used for running hyperparameter search."""

    def __init__(
        self,
        model_name: str,
        dataset_name: str,
        snapshots_path: str,
        lp_dataset_paths: str,
        emb_dim: int,
        budget_size: int,
        model_args: Dict = None,
        *args,
        **kwargs
    ):
        """Instantiates the class.

        :param config_path: Path to the original config file of the model.
        :param model_name: Name of the model. It should be one of the names
            used across the project, ex. 'xgb' or 'lgb'.
        :param dataset_name: Name of features to use during parameter search.
            It should be of the names used across the project, ex. 'pure'.
        :param args: Additional positional parameters for Worker configuration.
        :param kwargs: Additional keyword parameters for Worker configuration.
        """
        super().__init__(**kwargs)

        self.dataset_name = dataset_name
        self.model_name = model_name
        self.model_args = model_args or {}
        self.budget_size = budget_size

        self.dim = emb_dim

        self.data = []

        for split_type in ['partial', 'full']:
            g_path = os.path.join(snapshots_path, split_type, self.dataset_name)

            ds_path = lp_dataset_paths
            for ss in hpsu.list_dir(g_path):
                if split_type == 'partial' and ss.split('_')[-1] == '10':
                    continue
                if split_type == 'full' and ss.split('_')[-1] == '9':
                    continue
                graph_path = os.path.join(g_path, ss)

                ng_s = int(ss.split('_')[2])
                ng_e = ng_s + 1
                lp_ds_path = os.path.join(
                    ds_path,
                    f'DS_{ng_s}_{ng_e}'
                )

                self.data.append((graph_path, lp_ds_path))
        self.data = np.array(self.data)

    def step(self, config: Dict[str, Any]) -> Dict[str, Any]:
        """Perform hyperparameter set evaluation.

        The loss is always minimized in the function.

        :param config: Dict of new parameters sampled from the space
            configuration.
        :return: Dict of loss under `loss` key, and any additional info that
            is not used during hyperparameter computation per se.
        """
        # Read original configuration
        print("Config:")
        print(config)

        try:
            # Prepare output metrics
            results = []

            for graph_path, lp_ds_path in self.data[np.random.choice(
                len(self.data),
                size=self.budget_size,
                replace=False
            )]:

                graph = io.readers.read_pickle(graph_path)
                dataset = io.readers.read_pickle(lp_ds_path)

                emb_model = hpsu.create_embedding_model(
                    self.module,
                    config,
                    self.dim,
                    self.model_args
                )

                emb = emb_model.embed(graph)
                auc = hpsu.eval_lp(emb, dataset)

                results.append(auc)

            output = {
                "loss": np.mean([1 - result for result in results]),
                "auc": np.mean(results),
                'status': hopt.STATUS_OK,
            }

        except:  # noqa: E722
            output = {
                'status': hopt.STATUS_FAIL,
            }

        print("Output")
        print(output)

        return output

    @abc.abstractmethod
    def get_configspace(self):
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """


def make_worker(key, **class_kwargs):
    """Initialize HPS Worker."""
    my_classes = {
        'dgi': hpsw.DGIWorker,
        'hope': hpsw.HOPEWorker,
        'n2v': hpsw.N2VWorker,
        'ctdne': hpsw.CTDNEWorker,
        'line': hpsw.LINEWorker,
        'tne': hpsw.TNEWorker,
        'streamwalk': hpsw.StreamWalkWorker,
        'sor': hpsw.SORWorker,
        'generalisedfildne': hpsw.GeneralisedFILDNEWorker,
        'basicfildne': hpsw.BasicFILDNEWorker,
        'aernn': hpsw.DynGraphWorker
    }

    return my_classes[key](**class_kwargs)
