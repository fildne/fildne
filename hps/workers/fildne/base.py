"""Worker for FILDNE embedding."""
import os
from copy import deepcopy as copy
from typing import Dict, Any

import hyperopt as hopt
import numpy as np

import hps.utils as hpsu
from dgem.embedding import KeyedModel
from dgem.embedding.incremental import calibration as cl
from dgem.embedding.incremental.fildne import GeneralisedFILDNE, BasicFILDNE
from dgem.tools import io


class GeneralisedFILDNEWorker:
    """Worker used for running hyperparameter search in Generalised FILDNE."""

    def __init__(
            self,
            model_name: str,
            dataset_name: str,
            snapshots_path: str,
            lp_dataset_paths: str,
            emb_path: str,
            activity_path: str,
            budget_size: int,
            model_args: Dict = None,
            **kwargs
    ):
        """Instantiates the class.

        :param config_path: Path to the original config file of the model.
        :param model_name: Name of the model. It should be one of the names
            used across the project, ex. 'xgb' or 'lgb'.
        :param dataset_name: Name of features to use during parameter search.
            It should be of the names used across the project, ex. 'pure'.
        :param args: Additional positional parameters for Worker configuration.
        :param kwargs: Additional keyword parameters for Worker configuration.
        """
        self.dataset_name = dataset_name
        self.model_name = model_name
        self.model_args = model_args or {}
        self.budget_size = budget_size

        self.lp_ds = {}
        self.activity = []
        self.snapshots = []
        self.emb = []

        for s_id in range(2, len(hpsu.list_dir(lp_dataset_paths)) + 1):
            ds_key = f'DS_{s_id}_{s_id + 1}'
            self.lp_ds[ds_key] = io.readers.read_pickle(
                os.path.join(lp_dataset_paths, ds_key)
            )

        for s_id in range(len(hpsu.list_dir(activity_path))):
            self.activity.append(io.readers.read_pickle(
                os.path.join(activity_path, f'G_{s_id}_{s_id + 1}')
            ))
            self.snapshots.append(io.readers.read_nx_gpickle(
                os.path.join(
                    snapshots_path,
                    f'partial/{dataset_name}/G_{s_id}_{s_id + 1}'
                )
            ))
            self.emb.append(KeyedModel.from_file(
                os.path.join(emb_path, f'F_{s_id}_{s_id + 1}')
            ))

    @staticmethod
    def run_fildne(
            embeddings,
            activity,
            calibration_args,
            dataset,
            last_snapshot,
            cfg
    ):
        """Runs FILDNE and returns AUC Score."""
        calibrated_embeddings = [embeddings[0]]

        for emb_idx in range(1, len(embeddings)):
            emb, _ = cl.calibrate(
                embeddings=[calibrated_embeddings[-1], embeddings[emb_idx]],
                activity=[activity[emb_idx - 1], activity[emb_idx]],
                ref_nodes_config=calibration_args,
                num_workers=-1,
            )
            calibrated_embeddings.append(emb[-1])

        fildne = GeneralisedFILDNE(window_size=len(embeddings))
        fildne.fit(
            calibrated_embeddings,
            last_snapshot,
            prior_distribution=cfg['prior']
        )
        fildne_embedding = fildne.predict(calibrated_embeddings)
        auc = hpsu.eval_lp(fildne_embedding, dataset)

        return auc

    def step(self, config: Dict[str, Any]) -> Dict[str, Any]:
        """Perform hyperparameter set evaluation.

        The loss is always minimized in the function.

        :param config: Dict of new parameters sampled from the space
            configuration.
        :return: Dict of loss under `loss` key, and any additional info that
            is not used during hyperparameter computation per se.
        """
        # Read original configuration
        print("Config:")
        print(config)

        try:
            # Prepare output metrics
            results = []

            for last_idx in np.random.choice(
                    range(2, len(self.snapshots) + 1),
                    size=self.budget_size,
                    replace=False
            ):
                config = copy(config)

                res = self.run_fildne(
                    dataset=self.lp_ds[f'DS_{last_idx}_{last_idx + 1}'],
                    calibration_args={
                        **config['activity'],
                    },
                    cfg=config,
                    last_snapshot=self.snapshots[last_idx - 1],
                    embeddings=self.emb[:last_idx],
                    activity=self.activity[:last_idx]
                )

                results.append(res)

            output = {
                "loss": np.mean([1 - result for result in results]),
                "auc": np.mean(results),
                'status': hopt.STATUS_OK,
            }

        except Exception as e:  # noqa: E722
            output = {
                'status': hopt.STATUS_FAIL,
            }
            print(e)

        print("Output")
        print(output)

        return output

    @staticmethod
    def get_configspace() -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
        cs = {
            'activity': hopt.hp.choice('activity', [
                {
                    'name': 'percent',
                    'percent': hopt.hp.uniform(
                        'percent', 0.2, 1
                    )
                },
                {
                    'name': 'firstN',
                    'multiplier': hopt.hp.uniformint(
                        'multiplier', 1, 5
                    ),
                    'max-percent': hopt.hp.uniform(
                        'max_percent', 0.3, 1
                    )
                },
                {
                    'name': 'threshold',
                    'threshold': hopt.hp.uniform('threshold', 0.2, 1)
                }
            ]),
            'prior': hopt.hp.choice(
                'prior', ['uniform', 'increase']
            )
        }

        return cs


class BasicFILDNEWorker(GeneralisedFILDNEWorker):
    """Worker used for running hyperparameter search in Basic FILDNE."""

    @staticmethod
    def run_fildne(
            embeddings,
            activity,
            calibration_args,
            dataset,
            last_snapshot,
            cfg
    ):
        """Runs Basic FILDNE and returns AUC Score."""
        calibrated_embeddings, _ = cl.calibrate(
            embeddings[0:2],
            activity[0:2],
            ref_nodes_config=calibration_args,
            num_workers=-1,
        )
        fildne = BasicFILDNE(
            window_size=2,
            alpha=cfg['alpha']
        )
        fildne_embedding = fildne.predict(calibrated_embeddings)
        for emb_idx in range(2, len(embeddings)):
            calibrated_embeddings, _ = cl.calibrate(
                embeddings=[fildne_embedding, embeddings[emb_idx]],
                activity=[activity[emb_idx - 1], activity[emb_idx]],
                ref_nodes_config=calibration_args,
                num_workers=-1,
            )
            fildne_embedding = fildne.predict(calibrated_embeddings)

        auc = hpsu.eval_lp(fildne_embedding, dataset)

        return auc

    @staticmethod
    def get_configspace() -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
        cs = {
            'activity': hopt.hp.choice('activity', [
                {
                    'name': 'percent',
                    'percent': hopt.hp.uniform(
                        'percent', 0.2, 1
                    )
                },
                {
                    'name': 'firstN',
                    'multiplier': hopt.hp.uniformint(
                        'multiplier', 1, 5
                    ),
                    'max-percent': hopt.hp.uniform(
                        'max_percent', 0.3, 1
                    )
                },
                {
                    'name': 'threshold',
                    'threshold': hopt.hp.uniform('threshold', 0.2, 1)
                }
            ]),
            'alpha': hopt.hp.quniform(
                'alpha', 0.0, 1.0, 0.05
            )
        }

        return cs
