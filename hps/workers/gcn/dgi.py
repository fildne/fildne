"""Worker for Deep Graph Infomax embedding."""
from typing import Dict, Any

import hyperopt as hopt

from hps.workers import base


class DGIWorker(base.BaseWorker):
    """Definition of hyperparameters search space for HOPE embedding."""

    def __init__(self, *args, **kwargs):
        """Inits HPS Worker."""
        super().__init__(**kwargs)
        self.module = 'dgem.embedding.gcn.dgi.DGIEmbedding'

    def get_configspace(self) -> Dict[str, Any]:
        """Get space of hyperparameter search.

        The method is necessary for the Worker.
        """
        cs = {
            'lr': hopt.hp.quniform('lr', 0.0001, 0.1, 0.01),
            'l2_coef': hopt.hp.uniform('l2_coef', 0, 3.0),
            'nb_epochs': hopt.hp.choice(
                'nb_epochs', list(range(20, 500 + 1, 20))
            )
        }

        return cs
