from .base import BaseWorker, make_worker
from .mf.hope import HOPEWorker
from .gcn.dgi import DGIWorker
from .rw.n2v import N2VWorker
from .rw.ctdne import CTDNEWorker
from .other.line import LINEWorker
from .streaming.tne import TNEWorker
from .streaming.streamwalk import StreamWalkWorker
from .streaming.sor import SORWorker
from .fildne.base import BasicFILDNEWorker, GeneralisedFILDNEWorker
from .dyngraph.aernn import DynGraphWorker

__all__ = [
    'BaseWorker',
    'make_worker',
    'HOPEWorker',
    'DGIWorker',
    'N2VWorker',
    'CTDNEWorker',
    'LINEWorker',
    'TNEWorker',
    'StreamWalkWorker',
    'SORWorker',
    'BasicFILDNEWorker',
    'GeneralisedFILDNEWorker',
    'DynGraphWorker'
]
