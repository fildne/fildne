#!/bin/bash

docker build -t fildne:1.0 $PWD


docker run --name fildne-python37 \
	   -d \
	   -v $PWD:/fildne \
	   fildne:1.0 /bin/bash -c "trap : TERM INT; sleep infinity & wait"
