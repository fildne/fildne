#!/bin/bash

docker run -d --runtime=nvidia \
  --name fildne-python36-gpu \
  -e NVIDIA_VISIBLE_DEVICES=all \
  -e NUM_WORKERS=12 \
  -v $PWD:/fildne \
  tensorflow/tensorflow:latest-gpu-py3 \
  /bin/bash -c "trap : TERM INT; sleep infinity & wait"
