FROM python:3.7

ADD . /fildne
WORKDIR /fildne

RUN git remote set-url origin https://gitlab.com/fildne/fildne.git

RUN apt-get update -y && \
    apt-get install -y tmux htop vim less gcc libmpc-dev texlive texlive-latex-extra texlive-fonts-recommended dvipng

RUN apt-get install -y zsh && \
    chsh -s /bin/zsh && \
    zsh -c "$(wget https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

RUN pip install -r ./requirements.txt

CMD exec /bin/zsh -c "trap : TERM INT; sleep infinity & wait


