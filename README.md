All latex tables can be found in `data/tables/tables.html`  
Gains plots can be found in `data/plots/gains.html`  
Calibration sensitivity plot can be found in 
`data/plots/sensitivity/calibration.html`  
Alpha sensitivity plot can be found in `data/plots/sensitivity/alpha.html`  