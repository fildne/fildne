#!/bin/bash
dvc run --no-exec -f Dvcfile \
        -d data/plots \
        -d data/tables \
        -d data/hparams-sensitivity/ \
        -d data/results-aggregated/metrics.pkl \
        echo "Dummy stage"
