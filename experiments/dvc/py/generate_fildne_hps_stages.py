"""Generates dvc stages for FILDNE hps."""
import os
import shlex
import subprocess

import yaml

EMB_MODELS = [
    'gcn/dgi',
    'mf/lle',
    'mf/sge',
    'mf/hope',
    'other/line',
    'rw/n2v',
    'rw/ctdne'
]

COMMON_CFG = {
    'n-iterations': 100,
    'budget': 3
}


def main():
    """Main function."""
    for fildne_method in ['generalisedfildne', 'basicfildne']:
        for emb_model in EMB_MODELS:
            datasets = [
                'fb-forum',
                'fb-messages',
                'bitcoin-otc',
                'bitcoin-alpha',
                'ia-hypertext',
                'ia-radoslaw-email',
                'ia-enron-employees'
            ]

            if emb_model == 'mf/lle':
                datasets = [
                    'fb-forum',
                    'ia-hypertext',
                    'ia-radoslaw-email',
                    'ia-enron-employees'
                ]

            for ds in datasets:
                cfg_path = f'experiments/configs/hps/{fildne_method}/' \
                           f'{emb_model}/{ds}.yml'
                stage_path = f'experiments/dvc/stages/hps/{fildne_method}/' \
                             f'{emb_model}/{ds}.dvc'

                os.makedirs(os.path.dirname(cfg_path), exist_ok=True)
                os.makedirs(os.path.dirname(stage_path), exist_ok=True)

                if not os.path.isfile(stage_path):
                    emb_path = f'data/emb/{emb_model}/partial/{ds}/0/'
                    activity_path = f'data/snapshots/activity/{ds}/'
                    output_path = f'data/hps/{fildne_method}/{emb_model}/{ds}/'

                    with open(cfg_path, 'w') as f:
                        yaml.dump(
                            data={
                                **COMMON_CFG,
                                'emb-path': emb_path,
                                'activity-path': activity_path,
                                'output-path': output_path
                            },
                            stream=f
                        )

                    py_command = [
                        'PYTHONPATH=. python3',
                        'experiments/scripts/hps/hp-search.py',
                        f'-n {fildne_method}',
                        f'-d {ds}',
                        f'--config {cfg_path}',
                    ]

                    dvc_command = [
                        'dvc run',
                        '--no-exec',
                        f'-f {stage_path}',
                        '-d experiments/scripts/hps/hp-search.py',
                        '-d hps/workers/base.py',
                        '-d hps/workers/fildne/base.py',
                        f'-o {output_path}',
                        ' '.join(py_command)
                    ]

                    subprocess.run(shlex.split(' '.join(dvc_command)))
                else:
                    print(f'Stage {stage_path} already exists!')


if __name__ == '__main__':
    main()
