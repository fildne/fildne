#!/usr/bin/env bash

for model in 'gcn/dgi'; do
    STAGE_FILE="experiments/dvc/stages/train-embeddings/${model}.dvc"

    if [ -f "${STAGE_FILE}" ]; then
        echo "Stage ${STAGE_FILE} already exists!"
        continue
    fi

    # Dependencies
    PY_SCRIPT="experiments/scripts/train-embeddings.py"
    COMMON_CONFIG="experiments/configs/train-embeddings/common.yml"
    MODEL_CONFIG="experiments/configs/train-embeddings/${model}.yml"
    SNAPSHOTS_INPUT="data/snapshots/"

    # Outputs
    OUT_EMBEDDINGS_DIR="data/emb/${model}/"
    OUT_TIMES_FILE="data/times/${model}/embedding.pkl"

    # DVC command
    echo "Generating stage ${model}"
    dvc run --no-exec -f "${STAGE_FILE}" \
        -d "${PY_SCRIPT}" \
        -d "${COMMON_CONFIG}" \
        -d "${MODEL_CONFIG}" \
        -d "${SNAPSHOTS_INPUT}" \
        -o "${OUT_EMBEDDINGS_DIR}" \
        -o "${OUT_TIMES_FILE}" \
        PYTHONPATH=. python3 "${PY_SCRIPT}" --common-config "${COMMON_CONFIG}" --config "${MODEL_CONFIG}"
done

