#!/usr/bin/env bash

for model in 'gcn/dgi' 'mf/lle' 'mf/hope' 'mf/sge' 'rw/n2v' 'rw/ctdne' 'other/line'; do
    STAGE_FILE="experiments/dvc/stages/run-fildne/generalised/${model}.dvc"

    if [ -f "${STAGE_FILE}" ]; then
        echo "Stage ${STAGE_FILE} already exists!"
        continue
    fi

    # Dependencies
    PY_SCRIPT="experiments/scripts/run-fildne.py"
    COMMON_CONFIG="experiments/configs/run-fildne/generalised/common.yml"
    CONFIG="experiments/configs/run-fildne/generalised/${model}.yml"
    IN_EMBEDDINGS_DIR="data/emb/${model}/partial/"
    IN_SNAPSHOTS_DIR="data/snapshots/partial/"

    # Outputs
    OUT_EMBEDDINGS_DIR="data/emb/fildne/${model}/generalised/"
    OUT_CL_TIMES_FILE="data/times/${model}/generalised-fildne-calibration.pkl"
    OUT_FILDNE_TIMES_FILE="data/times/${model}/generalised-fildne-embedding.pkl"

    # DVC command
    echo "Generating stage ${model}"
    dvc run --no-exec -f "${STAGE_FILE}" \
        -d "${PY_SCRIPT}" \
        -d "${COMMON_CONFIG}" \
        -d "${CONFIG}" \
        -d "${IN_EMBEDDINGS_DIR}" \
        -d "${IN_SNAPSHOTS_DIR}" \
        -o "${OUT_EMBEDDINGS_DIR}" \
        -o "${OUT_CL_TIMES_FILE}" \
        -o "${OUT_FILDNE_TIMES_FILE}" \
        PYTHONPATH=. python3 "${PY_SCRIPT}" --common-config "${COMMON_CONFIG}" --config "${CONFIG}" --workers -1
done

