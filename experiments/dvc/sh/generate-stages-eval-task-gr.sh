#!/usr/bin/env bash

for model in 'rw/n2v' 'rw/ctdne' 'mf/lle' 'mf/hope' 'mf/sge' 'gcn/dgi' 'other/line' ; do

    STAGE_FILE="experiments/dvc/stages/eval-task/gr/${model}.dvc"

    if [ -f "${STAGE_FILE}" ]; then
      echo "Stage ${STAGE_FILE} already exists!"
      continue
    fi

    # Dependencies
    PY_SCRIPT="experiments/scripts/gr/eval-task.py"
    CONFIG="experiments/configs/eval-task/gr/${model}.yml"

    IN_SNAPSHOTS_DIR="data/snapshots/"
    IN_SP_DIR="data/intermediate/sp/"
    IN_EMB="data/emb/${model}"
    IN_FILDNE_EMB="data/emb/fildne/${model}"

    # Outputs
    OUT_DIR="data/task/gr/results/${model}"

    # DVC command
    echo "Generating stage ${STAGE_FILE}"
    dvc run --no-exec -f "${STAGE_FILE}" \
        -d "${PY_SCRIPT}" \
        -d "${CONFIG}" \
        -d "${IN_SNAPSHOTS_DIR}" \
        -d "${IN_SP_DIR}" \
        -d "${IN_EMB}" \
        -d "${IN_FILDNE_EMB}" \
        -o "${OUT_DIR}" \
        PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"

done

for model in 'streaming/streamwalk' 'streaming/sor' 'dyngraph/aernn'; do
  STAGE_FILE="experiments/dvc/stages/eval-task/gr/${model}.dvc"

    if [ -f "${STAGE_FILE}" ]; then
      echo "Stage ${STAGE_FILE} already exists!"
      continue
    fi

     PY_SCRIPT="experiments/scripts/gr/eval-task.py"
    CONFIG="experiments/configs/eval-task/gr/${model}.yml"

    IN_SNAPSHOTS_DIR="data/snapshots/"
    IN_SP_DIR="data/intermediate/sp/"
    IN_EMB="data/emb/${model}"

    # Outputs
    OUT_DIR="data/task/gr/results/${model}"

      # DVC command
    echo "Generating stage ${STAGE_FILE}"
    dvc run --no-exec -f "${STAGE_FILE}" \
        -d "${PY_SCRIPT}" \
        -d "${CONFIG}" \
        -d "${IN_SNAPSHOTS_DIR}" \
        -d "${IN_SP_DIR}" \
        -d "${IN_EMB}" \
        -o "${OUT_DIR}" \
        PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"

done
