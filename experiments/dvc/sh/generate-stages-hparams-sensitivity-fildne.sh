#!/usr/bin/env bash

STAGE_FILE="experiments/dvc/stages/hparams-sensitivity-fildne.dvc"

if [ -f "${STAGE_FILE}" ]; then
    echo "Stage ${STAGE_FILE} already exists!"
    exit 1
fi

# Dependencies
PY_SCRIPT="experiments/scripts/hparams-sensitivity-fildne.py"
CONFIG="experiments/configs/hparams-sensitivity-fildne.yml"
IN_SNAPSHOTS="data/snapshots/partial/"
IN_ACTIVITY="data/snapshots/activity/"
IN_EMBEDDINGS="data/emb/"
IN_DATASETS="data/task/lp/datasets/"

# Outputs
OUTPUT="data/hparams-sensitivity/fildne.pkl"

# DVC command
echo "Generating stage ${STAGE_FILE}"
dvc run --no-exec -f "${STAGE_FILE}" \
    -d "${PY_SCRIPT}" \
    -d "${CONFIG}" \
    -d "${IN_SNAPSHOTS}" \
    -d "${IN_ACTIVITY}" \
    -d "${IN_EMBEDDINGS}" \
    -d "${IN_DATASETS}" \
    -o "${OUTPUT}" \
    PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
