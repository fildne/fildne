#!/usr/bin/env bash

STAGE_FILE="experiments/dvc/stages/aggregate/times.dvc"

if [ -f "${STAGE_FILE}" ]; then
    echo "Stage ${STAGE_FILE} already exists!"
else
    # Dependencies
    PY_SCRIPT="experiments/scripts/aggregate-times.py"
    CONFIG="experiments/configs/aggregate/times.yml"

    IN_TIMES_PATH="data/times/"

    # Outputs
    OUT_AGGREGATED_TIMES_FILE="data/times-aggregated/times.pkl"

    # DVC command
    echo "Generating stage ${STAGE_FILE}"
    dvc run --no-exec -f "${STAGE_FILE}" \
        -d "${PY_SCRIPT}" \
        -d "${CONFIG}" \
        -d "${IN_TIMES_PATH}" \
        -o "${OUT_AGGREGATED_TIMES_FILE}" \
        PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
fi
