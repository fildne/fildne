#!/usr/bin/env bash

STAGE_FILE="experiments/dvc/stages/split-graphs.dvc"

if [ -f "${STAGE_FILE}" ]; then
    echo "Stage ${STAGE_FILE} already exists!"
    continue
fi

# Dependencies
PY_SCRIPT="experiments/scripts/split-graphs.py"
CONFIG="experiments/configs/split-graphs.yml"
IN_GRAPHS_DIR="data/graphs/"

# Outputs
OUT_SNAPSHOTS_DIR="data/snapshots/"
OUT_ACTIVITY_TIMES_DIR="data/times/activity/"

# DVC command
echo "Generating stage ${STAGE_FILE}"
dvc run --no-exec -f "${STAGE_FILE}" \
    -d "${PY_SCRIPT}" \
    -d "${CONFIG}" \
    -d "${IN_GRAPHS_DIR}" \
    -o "${OUT_SNAPSHOTS_DIR}" \
    -o "${OUT_ACTIVITY_TIMES_DIR}" \
    PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
