#!/usr/bin/env bash

STAGE_FILE="experiments/dvc/stages/aggregate-all-metrics.dvc"

if [ -f "${STAGE_FILE}" ]; then
    echo "Stage ${STAGE_FILE} already exists!"
    continue
fi

# Dependencies
PY_SCRIPT="experiments/scripts/aggregate-all-metrics.py"
CONFIG="experiments/configs/aggregate/all.yml"
LP_METRICS="data/task/lp/results-aggregated/metrics.pkl"
EC_METICS="data/task/ec/results-aggregated/metrics.pkl"
GR_METRICS="data/task/gr/results-aggregated/metrics.pkl"
MM_METRICS="data/mm-aggregated/metrics.pkl"
TIMES="data/times-aggregated/times.pkl"
TNE_TIMES="data/times/streaming/tne/lp/e2e.pkl"
N2V_TIMES="data/times/rw/n2v/embedding.pkl"

# Outputs
OUTPUT="data/results-aggregated/metrics.pkl"

# DVC command
echo "Generating stage ${STAGE_FILE}"
dvc run --no-exec -f "${STAGE_FILE}" \
    -d "${PY_SCRIPT}" \
    -d "${CONFIG}" \
    -d "${LP_METRICS}" \
    -d "${EC_METICS}" \
    -d "${GR_METRICS}" \
    -d "${MM_METRICS}" \
    -d "${TIMES}" \
    -d "${TNE_TIMES}" \
    -d "${N2V_TIMES}" \
    -o "${OUTPUT}" \
    PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
