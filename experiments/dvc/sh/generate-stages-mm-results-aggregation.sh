#!/usr/bin/env bash

STAGE_FILE="experiments/dvc/stages/aggregate-mm-results.dvc"

if [ -f "${STAGE_FILE}" ]; then
    echo "Stage ${STAGE_FILE} already exists!"
    continue
fi

# Dependencies
PY_SCRIPT="experiments/scripts/aggregate-mm-results.py"
CONFIG="experiments/configs/aggregate/mm.yml"
MM_RESULTS="data/mm/"

# Outputs
OUTPUT="data/mm-aggregated/metrics.pkl"

# DVC command
echo "Generating stage ${STAGE_FILE}"
dvc run --no-exec -f "${STAGE_FILE}" \
    -d "${PY_SCRIPT}" \
    -d "${CONFIG}" \
    -d "${MM_RESULTS}" \
    -o "${OUTPUT}" \
    PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
