#!/usr/bin/env bash

STAGE_FILE="experiments/dvc/stages/convert-edgelists-to-gpickles.dvc"

if [ -f "${STAGE_FILE}" ]; then
    echo "Stage ${STAGE_FILE} already exists!"
    continue
fi

# Dependencies
PY_SCRIPT="experiments/scripts/convert-edgelists-to-gpickles.py"
CONFIG="experiments/configs/convert-edgelists-to-gpickles.yml"
IN_EDGELISTS_DIR="data/edgelists/"

# Outputs
OUT_GRAPHS_DIR="data/graphs/"

# DVC command
echo "Generating stage ${STAGE_FILE}"
dvc run --no-exec -f "${STAGE_FILE}" \
    -d "${PY_SCRIPT}" \
    -d "${CONFIG}" \
    -d "${IN_EDGELISTS_DIR}" \
    -o "${OUT_GRAPHS_DIR}" \
    PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
