#!/usr/bin/env bash

for task in 'ec' 'lp'; do
    STAGE_FILE="experiments/dvc/stages/aggregate/metrics/${task}.dvc"

    if [ -f "${STAGE_FILE}" ]; then
        echo "Stage ${STAGE_FILE} already exists!"
        continue
    fi

    # Dependencies
    PY_SCRIPT="experiments/scripts/aggregate-metrics.py"
    CONFIG="experiments/configs/aggregate/metrics/${task}.yml"
    IN_RESULTS_DIR="data/task/${task}/results/"

    # Outputs
    OUT_AGGREGATED_RESULTS_FILE="data/task/${task}/results-aggregated/metrics.pkl"

    # DVC command
    echo "Generating stage ${STAGE_FILE}"
    dvc run --no-exec -f "${STAGE_FILE}" \
        -d "${PY_SCRIPT}" \
        -d "${CONFIG}" \
        -d "${IN_RESULTS_DIR}" \
        -o "${OUT_AGGREGATED_RESULTS_FILE}" \
        PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
done
