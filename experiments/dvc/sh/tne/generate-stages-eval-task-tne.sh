#!/usr/bin/env bash

for task in 'ec' 'lp'; do
  for model in 'streaming/tne'; do
      STAGE_FILE="experiments/dvc/stages/eval-task/${task}/${model}.dvc"

      if [ -f "${STAGE_FILE}" ]; then
          echo "Stage ${STAGE_FILE} already exists!"
          continue
      fi

      # Dependencies
      PY_SCRIPT="experiments/scripts/eval-task-$(echo ${model} | tr '/' '-').py"
      CONFIG="experiments/configs/eval-task/${task}/${model}.yml"
      IN_SNAPSHOTS_DIR="data/snapshots/partial/"
      IN_LP_DATASETS_DIR="data/task/lp/datasets/"

      # Outputs
      OUT_LP_RESULTS_DIR="data/task/${task}/results/${model}/"
      OUT_TIMES_FILE="data/times/${model}/${task}/e2e.pkl"

      # DVC command
      echo "Generating stage ${model}"
      dvc run --no-exec -f "${STAGE_FILE}" \
          -d "${PY_SCRIPT}" \
          -d "${CONFIG}" \
          -d "${IN_SNAPSHOTS_DIR}" \
          -d "${IN_LP_DATASETS_DIR}" \
          -o "${OUT_LP_RESULTS_DIR}" \
          -o "${OUT_TIMES_FILE}" \
          PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
  done
done
