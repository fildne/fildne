#!/usr/bin/env bash

STAGE_FILE="experiments/dvc/stages/calculate-sp.dvc"

if [ -f "${STAGE_FILE}" ]; then
  echo "Stage ${STAGE_FILE} already exists!"
  exit
fi

# Dependencies
PY_SCRIPT="experiments/scripts/calculate-shortest-paths.py"
CONFIG="experiments/configs/calculate-sp.yml"

# Change paths after changing DVC structure to calculate same walks per all tasks
IN_SNAPSHOTS="data/snapshots/"

# Outputs
OUT_SP="data/intermediate/sp/"

# DVC command
echo "Generating stage ${STAGE_FILE}"
dvc run --no-exec -f "${STAGE_FILE}" \
    -d "${PY_SCRIPT}" \
    -d "${CONFIG}" \
    -d "${IN_SNAPSHOTS}" \
    -o "${OUT_SP}" \
    PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"

