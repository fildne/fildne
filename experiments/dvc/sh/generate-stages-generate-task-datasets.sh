#!/usr/bin/env bash

for task in 'ec' 'lp'; do
    STAGE_FILE="experiments/dvc/stages/generate-task-datasets/${task}.dvc"

    if [ -f "${STAGE_FILE}" ]; then
        echo "Stage ${STAGE_FILE} already exists!"
        continue
    fi

    # Dependencies
    PY_SCRIPT="experiments/scripts/${task}/generate-task-datasets.py"
    CONFIG="experiments/configs/generate-task-datasets/${task}.yml"
    IN_SNAPSHOTS_DIR="data/snapshots/partial/"

    # Outputs
    OUT_DATASETS_DIR="data/task/${task}/datasets/"

    # DVC command
    echo "Generating stage ${STAGE_FILE}"
    dvc run --no-exec -f "${STAGE_FILE}" \
        -d "${PY_SCRIPT}" \
        -d "${CONFIG}" \
        -d "${IN_SNAPSHOTS_DIR}" \
        -o "${OUT_DATASETS_DIR}" \
        PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
done

