#!/usr/bin/env bash

STAGE_FILE="experiments/dvc/stages/hparams-sensitivity-calibration.dvc"

if [ -f "${STAGE_FILE}" ]; then
    echo "Stage ${STAGE_FILE} already exists!"
    continue
fi

# Dependencies
PY_SCRIPT="experiments/scripts/hparams-sensitivity-calibration.py"
CONFIG="experiments/configs/hparams-sensitivity-calibration.yml"
SNAPSHOTS_DIR="data/snapshots/partial/"
EMBEDDINGS_DIR="data/emb/rw/n2v/partial/"
ACTIVITY_DIR="data/snapshots/activity/"
LP_DATASETS_DIR="data/task/lp/datasets/"

# Outputs
OUTPUT="data/hparams-sensitivity/calibration.pkl"

# DVC command
echo "Generating stage ${STAGE_FILE}"
dvc run --no-exec -f "${STAGE_FILE}" \
    -d "${PY_SCRIPT}" \
    -d "${CONFIG}" \
    -d "${SNAPSHOTS_DIR}" \
    -d "${EMBEDDINGS_DIR}" \
    -d "${ACTIVITY_DIR}" \
    -d "${LP_DATASETS_DIR}" \
    -o "${OUTPUT}" \
    PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
