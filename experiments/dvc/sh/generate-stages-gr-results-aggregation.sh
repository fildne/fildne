#!/usr/bin/env bash

STAGE_FILE="experiments/dvc/stages/aggregate-gr-results.dvc"

if [ -f "${STAGE_FILE}" ]; then
    echo "Stage ${STAGE_FILE} already exists!"
    continue
fi

# Dependencies
PY_SCRIPT="experiments/scripts/aggregate-gr-results.py"
CONFIG="experiments/configs/aggregate/metrics/gr.yml"
GR_RESULTS="data/task/gr/results/"

# Outputs
OUTPUT="data/task/gr/results-aggregated/"

# DVC command
echo "Generating stage ${STAGE_FILE}"
dvc run --no-exec -f "${STAGE_FILE}" \
    -d "${PY_SCRIPT}" \
    -d "${CONFIG}" \
    -d "${GR_RESULTS}" \
    -o "${OUTPUT}" \
    PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
