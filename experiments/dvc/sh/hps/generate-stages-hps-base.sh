#!/usr/bin/env bash

PY_SCRIPT="experiments/scripts/hps/hp-search.py"
BASE_WORKER="hps/workers/base.py"

for method in 'hope' 'dgi' 'ctdne' 'n2v' 'line' 'tne' 'streamwalk' 'sor' 'aernn'; do
    if [ "${method}" == "dgi" ]; then
        EMB_TYPE="gcn"
    fi

    if [ "${method}" == "hope" ]; then
        EMB_TYPE="mf"
    fi

    if [ "${method}" == "ctdne" ] || [ "${method}" == "n2v" ]; then
        EMB_TYPE="rw"
    fi

    if [ "${method}" == "line" ]; then
        EMB_TYPE="other"
    fi

    if [ "${method}" == "tne" ]; then
        EMB_TYPE="streaming"
    fi

    if [ "${method}" == "streamwalk" ] || [ "${method}" == "sor" ]; then
        EMB_TYPE="streaming"
    fi

    if [ "${method}" == "aernn" ]; then
        EMB_TYPE="dyngraph"
    fi


    for ds in 'fb-forum' 'fb-messages' 'ia-hypertext' 'ia-enron-employees' 'ia-radoslaw-email' 'bitcoin-alpha' 'bitcoin-otc'; do

        CONFIG="experiments/configs/hps/${EMB_TYPE}/${method}/${ds}.yml"

        if [ -f "${CONFIG}" ]; then
            STAGE_PATH="experiments/dvc/stages/hps/${method}/"
            STAGE_FILE="${STAGE_PATH}/${ds}.dvc"

            if ! [ -d "${STAGE_PATH}" ]; then
                mkdir "${STAGE_PATH}"
            fi

            if [ -f "${STAGE_FILE}" ]; then
                echo "Stage ${STAGE_FILE} already exists!"
                continue
            fi

            # Dependencies
            WORKER="hps/workers/${EMB_TYPE}/${method}.py"

            # Outputs
            OUT_DIR="data/hps/${method}/${ds}/"

            # DVC command
            echo "Generating stage ${STAGE_FILE}"
            dvc run --no-exec -f "${STAGE_FILE}" \
                -d "${PY_SCRIPT}" \
                -d "${CONFIG}" \
                -d "${BASE_WORKER}" \
                -d "${WORKER}" \
                -o "${OUT_DIR}" \
                PYTHONPATH=. python3 "${PY_SCRIPT}" -n "${method}" -d "${ds}" --config "${CONFIG}"
        fi
    done
done
