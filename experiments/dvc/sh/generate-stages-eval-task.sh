#!/usr/bin/env bash

for task in 'ec' 'lp'; do
    for model in 'rw/n2v' 'rw/ctdne' 'mf/lle' 'mf/hope' 'mf/sge' 'gcn/dgi' 'other/line'; do

        STAGE_FILE="experiments/dvc/stages/eval-task/${task}/${model}.dvc"

        if [ -f "${STAGE_FILE}" ]; then
            echo "Stage ${STAGE_FILE} already exists!"
            continue
        fi

        # Dependencies
        PY_SCRIPT="experiments/scripts/eval-task.py"
        CONFIG="experiments/configs/eval-task/${task}/${model}.yml"
        IN_EMBEDDINGS_DIR="data/emb/${model}/"
        IN_EMBEDDINGS_FILDNE_DIR="data/emb/fildne/${model}/"
        IN_TASK_DATASETS_DIR="data/task/${task}/datasets/"

        # Outputs
        OUT_TASK_RESULTS_DIR="data/task/${task}/results/${model}/"
        OUT_TASK_RESULTS_FILDNE_DIR="data/task/${task}/results/fildne/${model}/"
        OUT_TASK_TIMES_DIR="data/times/${model}/validation-times/${task}/"

        # DVC command
        echo "Generating stage ${model}"
        dvc run --no-exec -f "${STAGE_FILE}" \
            -d "${PY_SCRIPT}" \
            -d "${CONFIG}" \
            -d "${IN_EMBEDDINGS_DIR}" \
            -d "${IN_EMBEDDINGS_FILDNE_DIR}" \
            -d "${IN_TASK_DATASETS_DIR}" \
            -o "${OUT_TASK_RESULTS_DIR}" \
            -o "${OUT_TASK_RESULTS_FILDNE_DIR}" \
            -o "${OUT_TASK_TIMES_DIR}" \
            PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
    done
done

for task in 'ec' 'lp'; do
    for model in 'streaming/streamwalk' 'streaming/sor' 'dyngraph/aernn'; do

        STAGE_FILE="experiments/dvc/stages/eval-task/${task}/${model}.dvc"

        if [ -f "${STAGE_FILE}" ]; then
            echo "Stage ${STAGE_FILE} already exists!"
            continue
        fi

        # Dependencies
        PY_SCRIPT="experiments/scripts/eval-task.py"
        CONFIG="experiments/configs/eval-task/${task}/${model}.yml"
        IN_EMBEDDINGS_DIR="data/emb/${model}/"
        IN_TASK_DATASETS_DIR="data/task/${task}/datasets/"

        # Outputs
        OUT_TASK_RESULTS_DIR="data/task/${task}/results/${model}/"
        OUT_TASK_TIMES_DIR="data/times/${model}/validation-times/${task}/"

        # DVC command
        echo "Generating stage ${model}"
        dvc run --no-exec -f "${STAGE_FILE}" \
            -d "${PY_SCRIPT}" \
            -d "${CONFIG}" \
            -d "${IN_EMBEDDINGS_DIR}" \
            -d "${IN_TASK_DATASETS_DIR}" \
            -o "${OUT_TASK_RESULTS_DIR}" \
            -o "${OUT_TASK_TIMES_DIR}" \
            PYTHONPATH=. python3 "${PY_SCRIPT}" --config "${CONFIG}"
    done
done
