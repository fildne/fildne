"""Applies FILDNE on previously trained embeddings."""
import argparse
import os
import pickle
from collections import defaultdict
from timeit import default_timer as timer

import yaml
from tqdm import tqdm

from dgem.embedding import KeyedModel
from dgem.embedding.incremental import calibration as cl
from dgem.embedding.incremental.fildne import BasicFILDNE, GeneralisedFILDNE
from dgem.tools.io import readers, writers


def get_args():
    """Parses arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--common-config',
        help='Common config path',
        required=True
    )
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )
    parser.add_argument(
        '--workers',
        help='Workers count for training embeddings',
        type=int,
        required=True
    )

    return parser.parse_args()


def read_activity(path):
    activity = {}
    for snapshot in os.listdir(path):
        with open(os.path.join(path, snapshot), 'rb') as f:
            activity[snapshot] = pickle.load(f)
    return activity


def read_embeddings(path):
    unsorted = [
        (snapshot, KeyedModel.from_file(os.path.join(path, snapshot)))
        for snapshot in os.listdir(path)
    ]
    return list(sorted(unsorted, key=lambda x: int(x[0][2])))


def template_path(path, model_info):
    """Replaces templates with model group and name in given path."""
    return (path
            .replace('${group}', model_info['group'])
            .replace('${name}', model_info['name']))


def main():
    # Get args
    args = get_args()

    with open(args.common_config, 'r') as fobj:
        common_cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    activity_path = common_cfg['directories']['activity']
    input_path = template_path(
        path=common_cfg['directories']['input'],
        model_info=cfg['model'],
    )
    output_path = template_path(
        path=common_cfg['directories']['output'],
        model_info=cfg['model'],
    )
    cl_times_path = template_path(
        path=common_cfg['directories']['cl-times-output'],
        model_info=cfg['model'],
    )
    fildne_times_path = template_path(
        path=common_cfg['directories']['fildne-times-output'],
        model_info=cfg['model'],
    )

    fildne_type = cfg['fildne-type']

    activity = {
        dataset: read_activity(os.path.join(activity_path, dataset))
        for dataset in os.listdir(activity_path)
    }

    ds_emb = {
        ds_name: os.path.join(input_path, ds_name)
        for ds_name in cfg['args'].keys()
    }

    cl_times = dict()
    fildne_times = dict()
    for ds_name, ds_path in ds_emb.items():
        cl_times[ds_name] = defaultdict(list)
        fildne_times[ds_name] = defaultdict(list)

        calibration_args = cfg['args'][ds_name]['calibration']
        fildne_args = cfg['args'][ds_name]['fildne']

        print(f'Dataset: {ds_name}')

        if fildne_type == 'generalised':
            snapshots_path = os.path.join(
                template_path(
                    path=common_cfg['directories']['input-snapshots'],
                    model_info=cfg['model'],
                ),
                ds_name
            )
            snapshots = [
                readers.read_pickle(os.path.join(snapshots_path, s))
                for s in sorted(os.listdir(snapshots_path))
            ]

        for run_path in tqdm(sorted(os.listdir(ds_path))):
            emb_path = os.path.join(ds_path, run_path)
            embeddings = read_embeddings(emb_path)

            os.makedirs(os.path.join(
                output_path, ds_name, run_path
            ), exist_ok=True)
            embeddings[0][1].to_pickle(filepath=os.path.join(
                output_path, ds_name, run_path, 'F_0_1'
            ))

            ds_activity = [
                activity[ds_name][x[0].replace('F', 'G')]
                for x in embeddings
            ]
            embeddings = [emb[1] for emb in embeddings]
            calibrated_times = []

            if fildne_type == 'generalised':
                calibrated_embeddings = [embeddings[0]]

                for emb_idx in range(1, len(embeddings)):
                    emb, times = cl.calibrate(
                        embeddings=[calibrated_embeddings[-1],
                                    embeddings[emb_idx]],
                        activity=[ds_activity[emb_idx - 1],
                                  ds_activity[emb_idx]],
                        ref_nodes_config=calibration_args,
                        num_workers=-1,
                    )
                    calibrated_embeddings.append(emb[-1])
                    calibrated_times.append(times[0])

                for cl_time_id, cl_time in enumerate(calibrated_times):
                    cl_emb_key = f'F_{cl_time_id + 1}_{cl_time_id + 2}'
                    cl_times[ds_name][cl_emb_key].append(cl_time)

                for ws in range(2, len(embeddings) + 1):
                    fildne_st_time = timer()

                    fildne = GeneralisedFILDNE(window_size=ws)
                    fildne.fit(
                        calibrated_embeddings[:ws],
                        snapshots[ws - 1],
                        fildne_args['prior']
                    )
                    fildne_embedding = fildne.predict(
                        calibrated_embeddings[:ws]
                    )

                    fildne_en_time = timer()

                    fildne_embedding.to_pickle(filepath=os.path.join(
                        output_path, ds_name, run_path, f'F_0_{ws}'
                    ))
                    fildne_times[ds_name][f'F_0_{ws}'].append(
                        fildne_en_time - fildne_st_time
                    )

            elif fildne_type == 'basic':
                fildne_embeddings = [embeddings[0]]

                for emb_idx in range(1, len(embeddings)):
                    emb, times = cl.calibrate(
                        embeddings=[
                            fildne_embeddings[-1], embeddings[emb_idx]
                        ],
                        activity=[
                            ds_activity[emb_idx - 1], ds_activity[emb_idx]
                        ],
                        ref_nodes_config=calibration_args,
                        num_workers=-1,
                    )
                    fildne_st_time = timer()
                    calibrated_times.append(times[-1])

                    fildne = BasicFILDNE(
                        window_size=2,
                        alpha=fildne_args['alpha']
                    )
                    fildne_embeddings.append(fildne.predict(emb))
                    fildne_en_time = timer()

                    fildne_embeddings[-1].to_pickle(filepath=os.path.join(
                        output_path, ds_name, run_path, f'F_0_{emb_idx+1}'
                    ))
                    fildne_times[ds_name][f'F_0_{emb_idx+1}'].append(
                        fildne_en_time - fildne_st_time
                    )

                for cl_time_id, cl_time in enumerate(calibrated_times):
                    cl_emb_key = f'F_{cl_time_id + 1}_{cl_time_id + 2}'
                    cl_times[ds_name][cl_emb_key].append(cl_time)
            else:
                raise Exception(f'No such FILDNE as {fildne_type}')

    os.makedirs(os.path.dirname(cl_times_path), exist_ok=True)
    os.makedirs(os.path.dirname(fildne_times_path), exist_ok=True)

    writers.export_obj_to_pickle(obj=cl_times, filepath=cl_times_path)
    writers.export_obj_to_pickle(obj=fildne_times, filepath=fildne_times_path)


if __name__ == '__main__':
    main()
