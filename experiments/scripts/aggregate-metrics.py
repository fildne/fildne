import argparse
from copy import deepcopy
import os
import yaml

import numpy as np
import pandas as pd

from dgem.tools.io import readers, writers


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )

    return parser.parse_args()


def extract_metric(in_metrics, metric_name):
    out_metrics = deepcopy(in_metrics)

    for k in out_metrics.keys():
        for ds_name in out_metrics[k].keys():
            for run_id in out_metrics[k][ds_name].keys():
                mtrs = out_metrics[k][ds_name][run_id]

                if isinstance(mtrs, dict):
                    val = mtrs[metric_name]
                elif isinstance(mtrs, list):
                    val = [m[metric_name] for m in mtrs]
                else:
                    raise ValueError(f'Unsupported results type {type(mtrs)}')

                out_metrics[k][ds_name][run_id] = val

    return out_metrics


def aggregate_across_runs(in_metrics):
    out_metrics = {}

    for key in in_metrics.keys():
        out_metrics[key] = {}
        for ds_name in in_metrics[key].keys():
            mtrs = [
                in_metrics[key][ds_name][run_id]
                for run_id in in_metrics[key][ds_name].keys()
            ]

            out_metrics[key][ds_name] = {
                'mean': np.mean(mtrs, axis=0).tolist(),
                'std': np.std(mtrs, axis=0, ddof=1).tolist(),
                'values': mtrs
            }

    return out_metrics


def make_df(in_metrics, streaming=False):
    entries = []

    for key in in_metrics.keys():
        for ds_name in in_metrics[key].keys():
            if key == 'baseline':
                val = in_metrics[key][ds_name]
                entries.append((
                    key, ds_name, 'G_0_9', val['mean'], val['std']
                ))
            else:
                vals = in_metrics[key][ds_name]
                for s_idx in range(len(vals['mean'])):
                    s_id_1 = s_idx
                    s_id_2 = s_idx + 1
                    if streaming:
                        if len(vals['mean']) == 9 and s_idx == 0:
                            # Handle cases whether F_0_1 were evaluated (tne)
                            continue

                        # Calculation of F_0_1 should be skipped for streaming
                        s_id_1 += 1
                        s_id_2 += 1
                    entries.append((
                        key, ds_name, f'G_{s_id_1}_{s_id_2}',
                        vals['mean'][s_idx], vals['std'][s_idx],
                        [it[s_idx] for it in vals['values']]
                    ))

    df = pd.DataFrame(entries, columns=[
        'experiment', 'dataset', 'snapshot', 'value_mean', 'value_std', 'values'
    ])
    return df


def run_aggregation(in_paths, models, metric_name):
    results = {
        'generalised-fildne': pd.DataFrame(),
        'basic-fildne': pd.DataFrame(),
    }
    if 'baselines' in in_paths:
        results['baselines'] = pd.DataFrame()

    for key in results.keys():
        for model in models:
            if not model['fildne'] and key in (
                    'generalised-fildne', 'basic-fildne'
            ):
                continue

            path = (
                in_paths[key]
                .replace('${group}', model['group'])
                .replace('${name}', model['name'])
            )
            metrics = readers.read_pickle(path)
            metrics_ext = extract_metric(metrics, metric_name)
            metrics_agg = aggregate_across_runs(metrics_ext)
            if model['group'] == 'streaming':
                metrics_df = make_df(metrics_agg, streaming=True)
            else:
                metrics_df = make_df(metrics_agg)

            metrics_df['group'] = model['group']
            metrics_df['name'] = model['name']

            metrics_df = metrics_df[[
                'experiment', 'dataset',
                'group', 'name',
                'snapshot', 'value_mean', 'value_std', 'values'
            ]]

            results[key] = pd.concat([
                results[key], metrics_df
            ]).reset_index(drop=True)

    return results


def main():
    args = get_args()

    print('Loading configuration...')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    in_paths = cfg['directories']['metrics']
    out_path = cfg['directories']['output']

    models = cfg['models']
    metric_name = cfg['metric-name']

    aggregated = run_aggregation(in_paths, models, metric_name)

    os.makedirs(os.path.dirname(out_path), exist_ok=True)
    writers.export_obj_to_pickle(
        obj=aggregated,
        filepath=out_path
    )


if __name__ == '__main__':
    main()
