import argparse
import os

import yaml
from tqdm import tqdm

from dgem.embedding.base import KeyedModel
from dgem.tasks import graph_reconstruction as gr
from dgem.tools import io


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config',
                        help='Config path',
                        required=True)
    return parser.parse_args()


def eval_metrics_per_graph(graph_path, sp_path, emb_path, emb_id):
    graph, graph_dists, mapping = gr.preprocessing.preprocess_data(
        graph_path=graph_path, sp_path=sp_path
    )

    emb_runs = os.listdir(emb_path)
    map_scores = []
    distortion_scores = []

    for emb_run_id in emb_runs:
        emb_run_path = os.path.join(emb_path, f'{emb_run_id}/{emb_id}')

        emb = KeyedModel.from_file(emb_run_path)
        emb_mtx = gr.preprocessing.convert_embedding_to_mtx(emb, mapping)

        mtx = gr.preprocessing.calculate_gr_metrics(
            graph=graph, graph_dists=graph_dists, emb_mtx=emb_mtx
        )

        map_scores.append(mtx['mAP'])
        distortion_scores.append(mtx['distortion'])

    return map_scores, distortion_scores


def eval_datasets(snap_path, sp_path, emb_path, cfg):
    results = {
        'mAP': dict(),
        'distortion': dict()
    }

    for ds_name in tqdm(cfg['datasets'], desc='Datasets', leave=False):

        ds_snap_path = os.path.join(snap_path, ds_name)
        ds_sp_path = os.path.join(sp_path, ds_name)
        ds_emb_path = os.path.join(emb_path, ds_name)

        results['mAP'][ds_name] = {}
        results['distortion'][ds_name] = {}

        for snapshot_id in tqdm(os.listdir(ds_snap_path),
                                desc='Snapshots',
                                leave=False):
            emb_id = snapshot_id.replace('G', 'F')

            # Skip last snapshot (used for task evaluation)
            if emb_id == 'F_9_10':
                continue

            emb_map_scores, emb_distortion_scores = eval_metrics_per_graph(
                graph_path=os.path.join(ds_snap_path, snapshot_id),
                sp_path=os.path.join(ds_sp_path, snapshot_id),
                emb_path=ds_emb_path,
                emb_id=emb_id
            )

            results['mAP'][ds_name][emb_id] = emb_map_scores
            results['distortion'][ds_name][emb_id] = emb_distortion_scores

    return results


def main():
    args = get_args()
    print('Loading configuration...')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    results = {}

    os.makedirs(cfg['directories']['out-results'])

    print('Evaluating baselines')
    stype = 'full'
    results[stype] = eval_datasets(
        snap_path=os.path.join(cfg['directories']['snapshots'], stype),
        sp_path=os.path.join(cfg['directories']['sp'], stype),
        emb_path=os.path.join(cfg['directories']['embeddings'], stype),
        cfg=cfg,
    )

    io.writers.export_obj_to_pickle(
        obj=results,
        filepath=os.path.join(cfg['directories']['out-results'], 'baseline.pkl')
    )

    if 'fildne-emb' in cfg['directories']:
        print('Evaluating FILDNE')
        fildne_results = {}
        for fildne_type in tqdm(['basic', 'generalised'], desc='Full/partial'):
            fildne_results[fildne_type] = eval_datasets(
                snap_path=os.path.join(cfg['directories']['snapshots'], 'full'),
                sp_path=os.path.join(cfg['directories']['sp'], 'full'),
                emb_path=os.path.join(
                    cfg['directories']['fildne-emb'], fildne_type
                ),
                cfg=cfg,
            )
        io.writers.export_obj_to_pickle(
            obj=fildne_results,
            filepath=os.path.join(
                cfg['directories']['out-results'], 'fildne.pkl'
            )
        )


if __name__ == '__main__':
    main()
