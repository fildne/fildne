import argparse
import os
from collections import defaultdict

import numpy as np
import pandas as pd
import yaml


def get_correct_emb_name(emb_name, emb_group):
    """Corrects embedding names."""
    if emb_group == 'baselines':
        return emb_name
    return f'{emb_group}_{emb_name}'


def aggregate_times_over_snapshots(input_df, emb_group):
    """Aggregates times over snapshots."""

    def aggregate_fn(rows):
        aggregated_dict = {}
        for row_idx, row in rows.iterrows():
            if row.emb == 'F_0_1':
                continue
            aggregated_dict[row.emb] = row.value_values
        return aggregated_dict

    out_df = input_df.groupby(['name', 'dataset']).apply(
        aggregate_fn
    ).to_frame().reset_index()
    out_df['name'] = [
        get_correct_emb_name(emb_group=emb_group, emb_name=it)
        for it in out_df['name']
    ]
    return out_df.set_index(['name', 'dataset'])


def aggregate_task_results_over_snapshots(input_df, emb_group, experiment):
    """Aggregates task results over snapshots."""

    def aggregate_fn(partial_df):
        id_increment = 1
        emb_name = partial_df.name[0]
        if emb_name in ['sor', 'streamwalk'] or 'fildne' in emb_group:
            id_increment = 0
        elif emb_name == 'tne':
            id_increment = -1

        aggregated_dict = {}
        for row_idx, row in partial_df.iterrows():
            snap_last_id = int(row.snapshot.split('_')[-1])
            snap_last_id += id_increment
            if snap_last_id == 1:
                continue
            aggregated_dict[f'F_0_{snap_last_id}'] = row['values']

        return aggregated_dict

    out_df = input_df[input_df.experiment == experiment].copy()
    out_df = out_df.groupby(['name', 'dataset']).apply(
        aggregate_fn
    ).to_frame().reset_index()
    out_df['name'] = [
        get_correct_emb_name(emb_group=emb_group, emb_name=it)
        for it in out_df['name']
    ]
    return out_df.set_index(['name', 'dataset'])


def aggregate_memory_results_over_snapshots(input_df):
    """Aggregates task results over snapshots."""

    def aggregate_fn(partial_df):
        aggregated_dict = {}
        for row_idx, row in partial_df.iterrows():
            snapshot_id = row.snapshot.replace('G', 'F')
            if snapshot_id == 'F_0_1':
                continue
            aggregated_dict[snapshot_id] = row['memory usage']

        return aggregated_dict

    def fix_name(x):
        group = x.group
        if x.group == 'basic-fildne':
            group = 'basic-fildne'  # TODO: kamtag
        return f'{group}_{x.method}' if 'fildne' in x.group else x.method

    input_df['name'] = input_df.apply(
        lambda x: fix_name(x),
        axis=1
    )
    out_df = input_df.groupby(['name', 'dataset']).apply(
        aggregate_fn
    ).to_frame().reset_index()
    out_df = out_df.set_index(['name', 'dataset'])
    out_df.columns = ['memory-measurement']

    return out_df


def aggregate_tne_times(tne_times_paths):
    """Aggregates times from tne evaluation."""
    tne_times = pd.read_pickle(tne_times_paths['tne'])
    n2v_times = pd.read_pickle(tne_times_paths['n2v'])

    parsed_tne_times = {}
    for ds_key, ds_times in tne_times['next'].items():
        parsed_tne_times[ds_key] = defaultdict(list)
        for run_id, run_times in ds_times.items():
            for emb_id, time in enumerate(run_times):
                parsed_tne_times[ds_key][f'F_0_{emb_id + 1}'].append(
                    time + n2v_times[ds_key][f'F_0_{emb_id + 1}'][0]
                )

    raw_tne_times_df = pd.DataFrame.from_dict(
        parsed_tne_times, orient='index'
    ).applymap(lambda x: (np.mean(x), np.std(x), x)).transpose()
    parsed_df = []
    for ds_name, ds_scores in raw_tne_times_df.to_dict().items():
        for emb_id, emb_scores in ds_scores.items():
            parsed_df.append({
                'dataset': ds_name,
                'group': 'streaming',
                'name': 'tne',
                'emb': emb_id,
                'value_mean': emb_scores[0],
                'value_std': emb_scores[1],
                'value_values': emb_scores[2]
            })
    return pd.DataFrame(parsed_df)


def aggregate_task_results(path, mt_name):
    """Aggregates task results."""
    task_data = pd.read_pickle(path)
    mt_keys = list(task_data.keys())
    out_df = pd.concat([
        aggregate_task_results_over_snapshots(
            task_data[mt_key], emb_group=mt_key, experiment='next'
        )
        for mt_key in mt_keys
    ])
    out_df.columns = [mt_name]
    return out_df


def aggregate_times(times_path, tne_times_paths):
    """Agregates all times."""
    times = pd.read_pickle(times_path)
    times_keys = list(times.keys())
    tne_times_df = aggregate_tne_times(tne_times_paths)
    times['baselines'] = pd.concat(
        [times['baselines'], tne_times_df], ignore_index=True, sort=False
    )
    times_df = pd.concat([
        aggregate_times_over_snapshots(
            times[mt_key], emb_group=mt_key
        )
        for mt_key in times_keys
    ])

    times_df.columns = ['calculation-time']
    return times_df


def aggregate_all_results(metrics_path):
    """Aggregates all results into single DataFrame."""
    lp_metrics = aggregate_task_results(metrics_path['lp'], mt_name='lp')
    ec_metrics = aggregate_task_results(metrics_path['ec'], mt_name='ec')
    gr_metrics = pd.read_pickle(metrics_path['gr'])
    if 'mm' in metrics_path:
        mm_metrics = aggregate_memory_results_over_snapshots(
            pd.read_pickle(metrics_path['mm'])
        )
    else:
        mm_metrics = pd.DataFrame()
    if 'times' in metrics_path:
        times = aggregate_times(
            metrics_path['times'],
            metrics_path['times-tne']
        )
    else:
        times = pd.DataFrame()

    aggregated = pd.concat(
        [lp_metrics, ec_metrics, gr_metrics, mm_metrics, times],
        axis=1
    )
    aggregated.index = aggregated.index.rename(['method', 'dataset'])
    return aggregated


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )

    return parser.parse_args()


def main():
    args = get_args()

    print('Loading configuration...')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    metrics_paths = cfg['directories']['metrics']
    out_path = cfg['directories']['output']

    print('Aggregating all metrics..')
    aggregated = aggregate_all_results(metrics_paths)

    os.makedirs(os.path.dirname(out_path), exist_ok=True)
    aggregated.to_pickle(
        path=out_path
    )


if __name__ == '__main__':
    main()
