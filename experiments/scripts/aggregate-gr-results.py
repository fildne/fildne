import argparse
import os

import pandas as pd
import yaml


def get_all_gr_result_files(results_path):
    """Returns all listed result files."""
    results_files = []

    for emb_group in os.listdir(results_path):
        emb_group_path = os.path.join(results_path, emb_group)

        for emb_method in os.listdir(emb_group_path):
            emb_path = os.path.join(emb_group_path, emb_method)

            if not os.path.isdir(emb_path):
                continue
            for result_file in os.listdir(emb_path):
                results_files.append(os.path.join(emb_path, result_file))

    return results_files


def parse_results_dict(input_dict, method):
    """Parses gr results dict."""
    parsed_data = {}
    for metric, metric_scores in input_dict.items():
        for ds_name, ds_scores in metric_scores.items():
            if ds_name not in parsed_data:
                parsed_data[ds_name] = {
                    'method': method,
                    'dataset': ds_name
                }
            parsed_data[ds_name][f'gr-{metric}'] = ds_scores

    return pd.DataFrame.from_dict(
        parsed_data, orient='index'
    ).reset_index(drop=True)


def parse_baseline_file(input_file, emb_key='full'):
    """Parses baseline file."""
    method = input_file.split('/')[-2]
    data = pd.read_pickle(input_file)[emb_key]
    return parse_results_dict(data, method)


def parse_fildne_file(input_file):
    """Parses FILDNE file."""

    method = input_file.split('/')[-2]
    data = pd.read_pickle(input_file)
    basic_fildne_df = parse_results_dict(
        data['basic'], method
    )
    basic_fildne_df.method = [
        f'basic-fildne_{it}' for it in basic_fildne_df.method
    ]
    generalised_fildne_df = parse_results_dict(
        data['generalised'], method
    )
    generalised_fildne_df.method = [
        f'generalised-fildne_{it}' for it in generalised_fildne_df.method
    ]

    return pd.concat(
        [basic_fildne_df, generalised_fildne_df],
        ignore_index=True
    )


def aggregate_all_results(input_path):
    """Aggregates all results."""
    output_data = []
    results_files = get_all_gr_result_files(input_path)

    for file in results_files:
        if 'baseline.pkl' in file:
            output_data.append(parse_baseline_file(file))
        else:
            output_data.append(parse_fildne_file(file))

    output_data = pd.concat(output_data, ignore_index=True)
    return output_data.set_index(['method', 'dataset'])


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )

    return parser.parse_args()


def main():
    args = get_args()

    print('Loading configuration...')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    in_path = cfg['directories']['metrics']
    out_path = cfg['directories']['output']

    aggregated = aggregate_all_results(in_path)

    os.makedirs(os.path.dirname(out_path), exist_ok=True)
    aggregated.to_pickle(
        path=out_path
    )


if __name__ == '__main__':
    main()
