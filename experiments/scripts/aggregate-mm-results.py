import argparse
import os

import numpy as np
import pandas as pd
import yaml
from collections import defaultdict


def aggregate_snapshots_results(path, emb_group_key):
    """Aggregates result from snapshots."""
    parsed_results = []

    results_keys = path.split('/')
    ds = results_keys[-1]
    emb = results_keys[-2]

    memory_usage = defaultdict(list)

    for run_id in os.listdir(path):
        curr_path = os.path.join(path, run_id)
        for file in os.listdir(curr_path):
            filepath = os.path.join(curr_path, file)
            if os.path.isdir(filepath):
                continue

            data = pd.read_pickle(filepath)
            memory_usage[file].append(np.max(data))

    if 'fildne' in emb_group_key:
        name = emb_group_key.split('fildne')
        emb_group_key = f'{name[0]}-fildne'

    for snapshot_key, snapshot_results in memory_usage.items():
        parsed_results.append({
            'group': emb_group_key,
            'dataset': ds,
            'method': emb,
            'snapshot': snapshot_key,
            'memory usage': snapshot_results
        })

    return pd.DataFrame(parsed_results)


def aggregate_all_results(path):
    """Agregates all memory measurement results."""

    def aggregate_method_group_results(input_path):
        """Aggregate results from emb group."""
        partial_results = []
        for emb_method in os.listdir(input_path):
            emb_method_path = os.path.join(input_path, emb_method)
            for ds in os.listdir(emb_method_path):
                results_path = os.path.join(emb_method_path, ds)
                partial_results.append(aggregate_snapshots_results(
                    path=results_path,
                    emb_group_key=emb_group_key
                ))
        return partial_results

    results = []
    for emb_group in os.listdir(path):
        if emb_group == 'results-aggregated':
            continue

        emb_group_path = os.path.join(path, emb_group)

        if emb_group in ['gcn', 'rw', 'other', 'mf']:
            emb_group_key = 'baselines'
        else:
            emb_group_key = emb_group

        if 'fildne' in emb_group:
            for base_emb_group in os.listdir(emb_group_path):
                base_emb_group_path = os.path.join(
                    emb_group_path, base_emb_group
                )
                results.extend(
                    aggregate_method_group_results(
                        base_emb_group_path
                    )
                )
        else:
            results.extend(
                aggregate_method_group_results(
                    emb_group_path
                )
            )

    return pd.concat(results, ignore_index=True)


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )

    return parser.parse_args()


def main():
    args = get_args()

    print('Loading configuration...')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    in_path = cfg['directories']['metrics']
    out_path = cfg['directories']['output']

    aggregated = aggregate_all_results(in_path)

    os.makedirs(os.path.dirname(out_path), exist_ok=True)
    aggregated.to_pickle(
        path=out_path
    )


if __name__ == '__main__':
    main()
