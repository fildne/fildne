"""Base embedding methods hp-search."""
import argparse
import os
from pathlib import Path

import hyperopt as hopt
import networkx as nx
import yaml

import hps.utils as hpsu
import hps.workers as hpsw
from dgem.tasks.link_prediction import dataset as lp_ds
from dgem.tools.io import writers

SNAPSHOTS_PATH = 'data/snapshots/'
EMB_DIMS = 'experiments/configs/train-embeddings/common.yml'


def get_args() -> argparse.Namespace:
    """Get arguments from the console to configure hyperparameter search."""
    parser = argparse.ArgumentParser(
        description="Script for hyperparameter optimisation"
    )
    parser.add_argument(
        "-n",
        "--model_name",
        required=True,
        help="Model name to perform hyperparameter search, ex. 'lgb'",
    )
    parser.add_argument(
        "-d",
        "--dataset_name",
        required=True,
        help="Dataset name",
    )
    parser.add_argument(
        "--config",
        required=True,
        help="Path to configuration file",
    )

    return parser.parse_args()


def create_lp_test_datasets(ds_name: str, output_path: str):
    sp = os.path.join(SNAPSHOTS_PATH, f'partial/{ds_name}')
    full_sp = os.path.join(SNAPSHOTS_PATH, f'full/{ds_name}')

    for snap_name in os.listdir(sp):
        if snap_name == 'G_0_1':
            continue
        snap_id = (
            snap_name.split('_')[-2],
            snap_name.split('_')[-1]
        )
        prev_snap_id = f'G_0_{snap_id[0]}'
        if prev_snap_id == 'G_0_1':
            prev_nodes = set(
                nx.read_gpickle(os.path.join(sp, prev_snap_id)).nodes()
            )
        else:
            prev_nodes = set(
                nx.read_gpickle(os.path.join(full_sp, prev_snap_id)).nodes()
            )
        ds = lp_ds.mk_link_prediction_dataset(
            graph=nx.read_gpickle(os.path.join(sp, snap_name)),
            prev_nodes=prev_nodes,
            split_proportion=0.75
        )
        writers.export_obj_to_pickle(
            obj=ds,
            filepath=os.path.join(output_path, f'DS_{snap_id[0]}_{snap_id[1]}')
        )


def main():
    """Execute hyperparameter search."""
    args = get_args()

    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    with open(EMB_DIMS, 'r') as fobj:
        emb_dim = yaml.load(
            fobj,
            Loader=yaml.FullLoader
        )['emb-dims'][args.dataset_name]

    output_path = cfg.get(
        'output-path',
        Path(f'data/hps/{args.model_name}/{args.dataset_name}/')
    )

    os.makedirs(output_path, exist_ok=True)

    lp_datasets_path = os.path.join(output_path, 'lp_datasets')
    os.makedirs(lp_datasets_path, exist_ok=True)

    create_lp_test_datasets(args.dataset_name, lp_datasets_path)

    worker_params = {
        'model_name': args.model_name,
        'dataset_name': args.dataset_name,
        'snapshots_path': SNAPSHOTS_PATH,
        'lp_dataset_paths': lp_datasets_path,
        'emb_dim': emb_dim,
        'model_args': cfg.get('model-args'),
        'budget_size': cfg.get('budget'),
    }

    if args.model_name == 'hope':
        min_sram = hpsu.calculate_sram(args.dataset_name, SNAPSHOTS_PATH)
        worker_params['max_beta'] = min_sram
    elif args.model_name in ('streamwalk', 'sor'):
        half_life, interval = hpsu.calculate_on2v_params(args.dataset_name)
        worker_params.update({'half_life': half_life, 'interval': interval})
    elif args.model_name in ('generalisedfildne', 'basicfildne'):
        worker_params.update({
            'emb_path': cfg['emb-path'],
            'activity_path': cfg['activity-path']
        })

    worker = hpsw.make_worker(args.model_name, **worker_params)

    trials = hopt.base.Trials()
    algo = hopt.tpe.suggest

    best = hopt.fmin(
        fn=worker.step,
        space=worker.get_configspace(),
        max_evals=cfg['n-iterations'],
        algo=algo,
        trials=trials,
    )

    hpsu.output_result(trials, best, output_path, 'lp')


if __name__ == "__main__":
    main()
