import argparse
import os

import networkx as nx
import yaml
from tqdm import tqdm

from dgem.tools.io import readers
from dgem.tools.io import writers


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )

    return parser.parse_args()


def read_graph(graph_path, graph_delimeter, weighted=False, directed=False):
    graph_data = (('timestamp', float),)
    create_using = nx.MultiGraph

    if weighted:
        graph_data = (('weight', int), ('timestamp', float))

    if directed:
        create_using = nx.MultiDiGraph

    graph = readers.read_nx_multi_graph_from_edgelist(
        filepath=graph_path,
        delimiter=graph_delimeter,
        data=graph_data,
        create_using=create_using,
    )

    return graph


def main():
    args = get_args()

    print('Loading configuration...')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    in_path = cfg['directories']['input']
    out_path = cfg['directories']['output']

    os.makedirs(out_path, exist_ok=True)

    print('Converting edge lists to graph pickles...')
    for ds in tqdm(cfg['datasets']):
        filename = ds['file']
        directed = ds['directed']
        weighted = ds['weighted']
        delimiter = ds['delimiter']

        graph = read_graph(
            graph_path=os.path.join(in_path, filename),
            graph_delimeter=delimiter,
            weighted=weighted,
            directed=directed,
        )

        writers.export_nxgraph_to_gpickle(
            nx_graph=graph,
            filepath=os.path.join(out_path, filename.replace('txt', 'gpkl'))
        )


if __name__ == '__main__':
    main()
