"""Script for evaluating embedding in given tasks."""
import argparse
import os
from timeit import default_timer as timer

import yaml
from tqdm import tqdm

from dgem import embedding as emb_model
from dgem.tools import utils
from dgem.tools.io import readers, writers


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config',
                        help='Config path',
                        required=True)
    return parser.parse_args()


def eval_task_model(data, embedding, cfg):
    st_time = timer()
    model_cls = utils.import_fn(cfg['clf']['module'])
    clf = model_cls(embedding, cfg['clf'].get('args', {}))
    clf.fit(data['x_train'], data['y_train'])
    end_time = timer()
    report = clf.validate(data['x_test'], data['y_test']).as_dict()
    return report, end_time - st_time


def eval_embeddings(datasets, emb_path, cfg, val_type, last_snapshot=False):
    results = []
    times = []

    emb_num = len(os.listdir(emb_path))
    if val_type == 'streaming':
        emb_num += 1  # Skipped F_0_1 evaluation

    for emb_id in range(emb_num):
        if val_type == 'streaming' and emb_id == 0:
            continue  # skipped F_0_1 evaluation
        task_ds = datasets[-1] if last_snapshot else datasets[emb_id]

        emb = emb_model.KeyedModel.from_file(filepath=os.path.join(
            emb_path,
            f'F_{emb_id if val_type == "snapshot" else 0}_{emb_id + 1}'
        ))
        res, ctime = eval_task_model(data=task_ds, embedding=emb, cfg=cfg)
        results.append(res)
        times.append(ctime)

    return results, times


def list_dir(path):
    """Lists `path` ignoring entries starting with `.`, such as `.gitignore`."""
    for directory in os.listdir(path):
        if directory[0] != '.':
            yield directory


def run_fildne_eval(cfg, directory):
    in_emb_path = directory['input-emb']
    in_task_ds = directory['input-task-ds']

    out_file = directory['output']
    times_out_file = directory.get('output-times', None)

    ds_emb = {
        ds_name: os.path.join(in_emb_path, ds_name)
        for ds_name in cfg['datasets']
    }

    results = {
        'next': {},
    }

    times = {
        'next': {},
    }

    for ds_name in tqdm(ds_emb.keys(), desc='Dataset'):
        num_emb_runs = len(os.listdir(ds_emb[ds_name]))

        task_datasets_path = os.path.join(in_task_ds, ds_name)
        task_datasets_runs = [
            os.path.join(task_datasets_path, it)
            for it in list_dir(task_datasets_path)
        ]

        # In case of Edge Classification task we have only one task dataset.
        # We need to duplicate it num_emb_runs times.
        if len(task_datasets_runs) == 1:
            task_datasets_runs = task_datasets_runs * num_emb_runs

        results['next'][ds_name] = {}

        times['next'][ds_name] = {}

        for idx in tqdm(range(num_emb_runs), desc='Run', leave=False):
            task_datasets = [
                readers.read_pickle(
                    os.path.join(task_datasets_runs[idx], f'DS_{i + 1}_{i + 2}')
                )
                for i in range(len(os.listdir(task_datasets_runs[idx])))
            ]

            emb_path = os.path.join(ds_emb[ds_name], str(idx))

            ns_res, ns_times = eval_embeddings(
                datasets=task_datasets,
                emb_path=emb_path,
                cfg=cfg,
                val_type='fildne',
                last_snapshot=False
            )
            results['next'][ds_name][idx] = ns_res
            times['next'][ds_name][idx] = ns_times

    os.makedirs(os.path.dirname(out_file), exist_ok=True)
    writers.export_obj_to_pickle(obj=results, filepath=out_file)
    if times_out_file:
        os.makedirs(os.path.dirname(times_out_file), exist_ok=True)
        writers.export_obj_to_pickle(obj=times, filepath=times_out_file)


def run_snapshot_eval(cfg, directory):
    in_emb_path = directory['input-emb']

    in_full_emb_path = os.path.join(in_emb_path, 'full/')
    in_task_ds = directory['input-task-ds']

    out_path = directory['output']
    times_out_file = directory['output-times']

    eval_emb_key = cfg['emb-eval-key']
    ds_emb = {
        'full': {
            ds_name: os.path.join(in_full_emb_path, ds_name)
            for ds_name in cfg['datasets']
        }
    }
    val_type = 'streaming'

    if eval_emb_key == 'partial':
        in_snapshot_emb_path = os.path.join(in_emb_path, 'partial/')
        ds_emb['partial'] = {
            ds_name: os.path.join(in_snapshot_emb_path, ds_name)
            for ds_name in cfg['datasets']
        }
        val_type = 'snapshot'

    results = {
        'next': {},
        'baseline': {},
    }

    times = {
        'next': {},
        'baseline': {},
    }

    for ds_name in tqdm(ds_emb['full'].keys(), desc='Dataset'):
        num_emb_runs = len(os.listdir(ds_emb['full'][ds_name]))

        task_datasets_path = os.path.join(in_task_ds, ds_name)
        task_datasets_runs = [
            os.path.join(task_datasets_path, it)
            for it in os.listdir(task_datasets_path)
        ]

        # In case of Edge Classification task we have only one task dataset.
        # We need to duplicate it num_emb_runs times.
        if len(task_datasets_runs) == 1:
            task_datasets_runs = task_datasets_runs * num_emb_runs

        assert len(task_datasets_runs) == num_emb_runs

        results['next'][ds_name] = {}
        results['baseline'][ds_name] = {}

        times['next'][ds_name] = {}
        times['baseline'][ds_name] = {}

        for idx in tqdm(range(num_emb_runs), desc='Run', leave=False):
            task_datasets = [
                readers.read_pickle(
                    os.path.join(task_datasets_runs[idx], f'DS_{i + 1}_{i + 2}')
                )
                for i in range(len(os.listdir(task_datasets_runs[idx])))
            ]

            emb_path = os.path.join(ds_emb[eval_emb_key][ds_name], str(idx))

            ns_res, ns_times = eval_embeddings(
                datasets=task_datasets,
                emb_path=emb_path,
                cfg=cfg,
                val_type=val_type,
                last_snapshot=False,
            )
            results['next'][ds_name][idx] = ns_res
            times['next'][ds_name][idx] = ns_times

            full_emb_path = os.path.join(ds_emb['full'][ds_name], str(idx))
            full_emb = emb_model.KeyedModel.from_file(
                os.path.join(full_emb_path, f'F_0_{len(task_datasets)}')
            )

            base_res, base_times = eval_task_model(
                data=task_datasets[-1],
                embedding=full_emb,
                cfg=cfg
            )
            results['baseline'][ds_name][idx] = base_res
            times['baseline'][ds_name][idx] = base_times

    os.makedirs(os.path.dirname(out_path), exist_ok=True)
    writers.export_obj_to_pickle(obj=results, filepath=out_path)

    os.makedirs(os.path.dirname(times_out_file), exist_ok=True)
    writers.export_obj_to_pickle(obj=times, filepath=times_out_file)


def main():
    # Get args
    args = get_args()

    print('Loading configuration')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    for directory in cfg['directories']:
        if directory['type'] == 'fildne':
            print('FILDNE embedding evaluation...')
            run_fildne_eval(cfg, directory)
        elif directory['type'] == 'snapshot':
            print('Snapshots embedding evaluation...')
            run_snapshot_eval(cfg, directory)


if __name__ == '__main__':
    main()
