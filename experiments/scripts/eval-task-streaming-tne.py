"""Training and evaluation of tNodeEmbed method."""
import argparse
import os

import yaml
from networkx import nx
from tqdm import tqdm

from dgem.embedding.tnodeembed.src.wrapper import TNEEmbedding
from dgem.tools.io import readers, writers


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )
    return parser.parse_args()


def main():
    # Get args
    args = get_args()

    print('Loading configuration')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    partial_graph_path = cfg['directories']['input']
    in_task_ds = cfg['directories']['input-ds']
    n2v_emb_path = cfg['directories']['input-n2v-emb']
    out_path = cfg['directories']['output']
    task = cfg['task']

    model_args = cfg['args']

    ds_rw = {
        ds_name: os.path.join(partial_graph_path, ds_name)
        for ds_name in cfg['args'].keys()
    }

    results = {
        'next': {},
        'baseline': {}
    }

    times = {
        'next': {},
        'baseline': {}
    }

    print('Snapshots embedding training...')
    for ds_name, ds_path in tqdm(ds_rw.items(), desc='Datasets'):
        print(f'Dataset: {ds_name}')
        num_emb_runs = len(os.listdir(
            os.path.join(n2v_emb_path, f'partial/{ds_name}/')
        ))
        task_datasets_path = os.path.join(in_task_ds, ds_name)
        task_datasets_runs = [
            os.path.join(task_datasets_path, it)
            for it in os.listdir(task_datasets_path)
        ]
        if len(task_datasets_runs) == 1:
            task_datasets_runs = task_datasets_runs * num_emb_runs

        batch_size = model_args[ds_name].pop('batch_size')

        snapshots = [
            nx.read_gpickle(os.path.join(ds_path, f'G_{idx}_{idx + 1}'))
            for idx in range(9)
        ]

        directed = False
        if nx.is_directed(snapshots[-1]):
            directed = True

        results['next'][ds_name] = {}
        results['baseline'][ds_name] = {}

        times['next'][ds_name] = {}
        times['baseline'][ds_name] = {}

        for run_id in tqdm(range(num_emb_runs), desc='Run', leave=False):
            lp_datasets = [
                readers.read_pickle(os.path.join(
                    task_datasets_runs[run_id], lp_ds
                ))
                for lp_ds in sorted(os.listdir(task_datasets_runs[run_id]))
            ]
            embeddings = [readers.read_pickle(
                os.path.join(n2v_emb_path, f'partial/{ds_name}/{run_id}/F_0_1')
            )]
            embeddings.extend([
                readers.read_pickle(
                    os.path.join(
                        n2v_emb_path,
                        f'full/{ds_name}/{run_id}/F_0_{it + 1}'
                    )
                )
                for it in range(1, 9)
            ])
            results_next = []
            times_next = []

            it = tqdm(lp_datasets, desc='Link prediction dataset', leave=False)
            for lp_ds_id, lp_dataset in enumerate(it):
                tne_model = TNEEmbedding(
                    args=model_args[ds_name],
                    directed=directed,
                    verbose=0
                )
                res, time = tne_model.eval(
                    snapshots=snapshots[0:lp_ds_id + 1],
                    embeddings=embeddings[0:lp_ds_id + 1],
                    lp_dataset=lp_dataset,
                    batch_size=batch_size,
                    task=task
                )

                results_next.append(res.as_dict())
                times_next.append(time)

            results['next'][ds_name][run_id] = results_next
            times['next'][ds_name][run_id] = times_next

            tne_model = TNEEmbedding(
                args=model_args[ds_name],
                directed=directed,
                verbose=0
            )

            baseline, time = tne_model.eval(
                snapshots=snapshots,
                embeddings=embeddings,
                batch_size=batch_size,
                lp_dataset=lp_datasets[-1],
                task=task
            )
            results['baseline'][ds_name][run_id] = baseline.as_dict()
            times['baseline'][ds_name][run_id] = time

        os.makedirs(os.path.dirname(out_path), exist_ok=True)
        writers.export_obj_to_pickle(obj=results, filepath=out_path)

        times_path = cfg['directories']['times-output']
        os.makedirs(os.path.dirname(times_path), exist_ok=True)
        writers.export_obj_to_pickle(obj=times, filepath=times_path)


if __name__ == '__main__':
    main()
