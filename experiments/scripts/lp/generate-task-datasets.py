import argparse
import os

import yaml
from tqdm import tqdm

from dgem.tasks.link_prediction import dataset as clp_ds
from dgem.tools.io import readers, writers


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config',
                        help='Config path',
                        required=True)
    return parser.parse_args()


def generate_lp_dataset_per_snap(snap_path, prev_snap_path, cfg):
    """Generates link prediction datasets over snapshots."""
    snapshot = readers.read_nx_gpickle(snap_path)
    prev_snapshot = readers.read_nx_gpickle(prev_snap_path)

    prev_nodes = set(prev_snapshot.nodes())
    x_tr, y_tr, x_ts, y_ts = clp_ds.mk_link_prediction_dataset(
        graph=snapshot,
        prev_nodes=prev_nodes,
        **cfg['cfg']
    )
    lp_dataset = {
        'x_train': x_tr,
        'y_train': y_tr,
        'x_test': x_ts,
        'y_test': y_ts
    }
    return lp_dataset


def main():
    args = get_args()
    print('Loading configuration...')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    datasets = {
        ds_name: {
            'partial': os.path.join(
                cfg['directories']['partial-input'], ds_name
            ),
            'full': os.path.join(
                cfg['directories']['full-input'], ds_name
            ),
        }
        for ds_name in cfg['datasets']
    }

    print('Generating link prediction datasets...')
    for run_id in tqdm(range(cfg['realizations']), desc='Run'):
        for ds_name, ds_path in tqdm(datasets.items(),
                                     desc='Dataset',
                                     leave=False):

            out_path = os.path.join(
                cfg['directories']['output'], f'{ds_name}/{run_id}/'
            )
            os.makedirs(out_path, exist_ok=True)

            for sid in range(2, 11):
                snap_id = f'G_{sid-1}_{sid}'
                prev_snap_id = f'G_0_{sid-1}'

                snap_path = os.path.join(
                    ds_path['partial'], snap_id
                )
                if prev_snap_id == 'G_0_1':
                    prev_snap_path = os.path.join(
                        ds_path['partial'], 'G_0_1'
                    )
                else:
                    prev_snap_path = os.path.join(
                        ds_path['full'], prev_snap_id
                    )
                lp_dataset = generate_lp_dataset_per_snap(
                    snap_path, prev_snap_path, cfg
                )
                writers.export_obj_to_pickle(
                    lp_dataset,
                    os.path.join(out_path, f'DS_{sid - 1}_{sid}')
                )


if __name__ == '__main__':
    main()
