import argparse
import os
import yaml

import numpy as np
import pandas as pd

from dgem.tools.io import readers, writers


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )

    return parser.parse_args()


def load_template(path, model):
    """Templates values into path and loads pickle."""
    return readers.read_pickle(
        path
        .replace('${group}', model['group'])
        .replace('${name}', model['name'])
    )


def read_times(in_paths, model):
    """Loads raw times from given paths and model."""
    times = {
        'activity': readers.read_pickle(in_paths['activity']),
        'embedding': load_template(in_paths['embedding'], model),
    }

    if model['fildne']:
        times['fildne'] = {
            k: {
                'calibration': load_template(in_paths[k]['calibration'], model),
                'embedding': load_template(in_paths[k]['embedding'], model),
            }
            for k in ('basic', 'generalised')
        }
    return times


def sum_stages(in_times, streaming):
    """Calculates the total times of given embeddings."""
    datasets = list(in_times['embedding'].keys())

    out_times = {
        'baselines': {}
    }

    first_idx = 2 if streaming else 1
    for ds_name in datasets:
        out_times['baselines'][ds_name] = {}
        for idx in range(first_idx, 10):
            emb_id = f'F_0_{idx}'
            tms = in_times['embedding'][ds_name][emb_id]

            out_times['baselines'][ds_name][emb_id] = {
                'mean': np.mean(tms, axis=0).tolist(),
                'std': np.std(tms, axis=0, ddof=1).tolist(),
                'values': tms
            }

    if 'fildne' in in_times.keys():
        for fildne_type, fildne_key in [
            ('basic', 'basic-fildne'), ('generalised', 'generalised-fildne')
        ]:
            out_times[fildne_key] = {}

            for ds_name in datasets:
                out_times[fildne_key][ds_name] = {}
                for idx in range(9):
                    emb_id = f'F_{idx}_{idx + 1}'
                    ie_emb_id = f'F_0_{idx + 1}'

                    et = np.array(in_times['embedding'][ds_name][emb_id])

                    at = in_times['activity'][ds_name][emb_id.replace('F', 'G')]
                    at = np.array([at, ] * len(et))
                    if idx == 0:
                        total = at + et
                    else:
                        i_times = in_times['fildne'][fildne_type]
                        ic = np.array(i_times['calibration'][ds_name][emb_id])
                        ie = np.array(i_times['embedding'][ds_name][ie_emb_id])
                        total = at + et + ic + ie

                    out_times[fildne_key][ds_name][ie_emb_id] = {
                        'mean': np.mean(total, axis=0).tolist(),
                        'std': np.std(total, axis=0, ddof=1).tolist(),
                        'values': total
                    }

    return out_times, ['mean', 'std', 'values']


def make_df(in_times, vk):
    """Constructs DataFrame from given times dict."""
    entries = []

    for ds_name in in_times.keys():
        for emb_id in in_times[ds_name].keys():
            val = [in_times[ds_name][emb_id][k] for k in vk]
            entries.append((ds_name, emb_id, *val))

    df = pd.DataFrame(
        entries,
        columns=['dataset', 'emb', *[f'value_{k}' for k in vk]]
    )
    return df


def aggregate(in_paths, models):
    """Aggregates time for various models."""
    results = {
        'baselines': pd.DataFrame(),
        'generalised-fildne': pd.DataFrame(),
        'basic-fildne': pd.DataFrame(),
    }

    for model in models:
        times_raw = read_times(in_paths, model)
        time_sum, value_keys = sum_stages(
            times_raw,
            streaming=(
                model['group'] == 'streaming' or
                model['group'] == 'dyngraph'
            )
        )

        for key in time_sum.keys():
            times_df = make_df(time_sum[key], value_keys)

            times_df['group'] = model['group']
            times_df['name'] = model['name']

            times_df = times_df[[
                'dataset',
                'group',
                'name',
                'emb',
                *[f'value_{k}' for k in value_keys]
            ]]

            results[key] = pd.concat([results[key], times_df]).reset_index(
                drop=True)

    return results


def main():
    args = get_args()

    print('Loading configuration...')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    in_paths = cfg['directories']['times']
    out_path = cfg['directories']['output']

    models = cfg['models']

    times = aggregate(in_paths, models)

    os.makedirs(os.path.dirname(out_path), exist_ok=True)
    writers.export_obj_to_pickle(
        obj=times,
        filepath=out_path
    )


if __name__ == '__main__':
    main()
