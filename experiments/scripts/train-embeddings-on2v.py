"""Script for training online-n2v based graph embeddings."""
import argparse
import os
from collections import defaultdict
from timeit import default_timer as timer

import networkx as nx
import numpy as np
import yaml
from tqdm import tqdm

from dgem.embedding import base as km
from dgem.embedding.onlinen2v import wrappers as on2v_wrap
from dgem.tools.io import readers, writers


def get_args():
    """Parses arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--common-config',
        help='Common config path',
        required=True
    )
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )

    return parser.parse_args()


def run_embeddings(
        in_path,
        out_emb_path,
        model_cfg,
        model_args,
        emb_dims,
        num_runs,
        skip_last
):
    emb_times = {}

    for dataset_name in tqdm(model_args.keys(), desc='Datasets'):
        ds_path = os.path.join(in_path, dataset_name)

        emb_times[dataset_name] = defaultdict(list)

        snapshots = sorted(os.listdir(ds_path))

        if skip_last:
            snapshots = snapshots[:-1]

        snapshots_graph = [
            readers.read_pickle(os.path.join(ds_path, s_name))
            for s_name in snapshots
        ]

        if isinstance(snapshots_graph[0], (nx.DiGraph, nx.MultiDiGraph)):
            mirror = False
        else:
            mirror = True

        graph_df = on2v_wrap.snapshots_to_df(snapshots_graph)

        for rid in tqdm(range(num_runs), desc='Run', leave=False):
            online_n2v = on2v_wrap.create_model(
                model_name=model_cfg['name'],
                args=model_args[dataset_name],
                emb_dim=emb_dims[dataset_name],
                mirror=mirror,
            )
            partial_data = on2v_wrap.init(online_n2v, graph_df)

            for sid, s_name in enumerate(tqdm(snapshots,
                                              desc='Snapshots',
                                              leave=False)):
                emb_name = s_name.replace('G', 'F')

                out_dir = os.path.join(out_emb_path, dataset_name, str(rid))
                os.makedirs(out_dir, exist_ok=True)

                st_time = timer()

                emb = on2v_wrap.embed(
                    on2v=online_n2v,
                    snapshot_data=partial_data[partial_data['sid'] == sid]
                )
                end_time = timer()
                emb = emb.set_index('index')
                emb = {
                    str(vec[0]): np.array(vec[1:])
                    for vec in emb[1:].itertuples()
                }
                emb = km.KeyedModel(
                    size=emb_dims[dataset_name],
                    node_emb_vectors=emb,
                )

                emb.to_pickle(filepath=os.path.join(out_dir, emb_name))
                emb_times[dataset_name][emb_name].append(end_time - st_time)

        emb_times[dataset_name] = dict(emb_times[dataset_name])

    return emb_times


def main():
    """Runs script."""
    # Get args
    args = get_args()

    with open(args.common_config, 'r') as fobj:
        common_cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    in_snapshots_path = common_cfg['directories']['snapshots-input']
    out_emb_path = os.path.join(
        common_cfg['directories']['emb-output'],
        cfg['model']['group'],
        cfg['model']['name'],
    )
    out_times_path = os.path.join(
        common_cfg['directories']['times-output'],
        cfg['model']['group'],
        cfg['model']['name'],
    )

    emb_dims = common_cfg['emb-dims']
    num_runs = cfg['runs']

    model_cfg = cfg['model']
    model_args = cfg['args']

    print('Full snapshots embeddings')
    full_times = run_embeddings(
        in_path=os.path.join(in_snapshots_path, 'full/'),
        out_emb_path=os.path.join(out_emb_path, 'full/'),
        model_cfg=model_cfg,
        model_args=model_args,
        emb_dims=emb_dims,
        num_runs=num_runs,
        skip_last=False,
    )

    # Combine time measurements
    dataset_names = full_times.keys()
    emb_times = {
        k: {**full_times[k]}
        for k in dataset_names
    }

    # Save time measurements
    os.makedirs(out_times_path, exist_ok=True)
    writers.export_obj_to_pickle(
        obj=emb_times,
        filepath=os.path.join(out_times_path, 'embedding.pkl')
    )


if __name__ == '__main__':
    main()
