import argparse
import os
import pickle
from collections import defaultdict
from timeit import default_timer as timer
from typing import Dict, Tuple, Union

import networkx as nx
import yaml
from tqdm import tqdm

from dgem.embedding.incremental import calibration
from dgem.tools.graph import nxtools
from dgem.tools.io import readers, writers


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config',
                        help='Config path',
                        required=True)
    return parser.parse_args()


def export_snapshots(in_snapshots, path, increment=1,
                     cumulative=False, start_from=0):
    os.makedirs(path, exist_ok=True)
    alias = 'G_{gid}_{gid2}'
    for snapshot_id, snapshot_graph in enumerate(in_snapshots):
        if cumulative:
            out_name = (alias.format(gid=0,
                                     gid2=start_from + snapshot_id + increment))
        else:
            out_name = (alias.format(gid=snapshot_id,
                                     gid2=start_from + snapshot_id + increment))

        out_path = os.path.join(path, out_name)
        writers.export_nxgraph_to_gpickle(snapshot_graph, out_path)


def export_activity(activity, path):
    os.makedirs(path, exist_ok=True)
    for snapshot_id, snapshot_activity in enumerate(activity):
        out_path = os.path.join(path, f'G_{snapshot_id}_{snapshot_id + 1}')
        with open(out_path, 'wb') as f:
            pickle.dump(snapshot_activity, f)


def get_activity(graph: nx.Graph) -> Tuple[Dict[Union[int, str], float], float]:
    start_time = timer()
    activity = dict(calibration.get_activity_dict(graph))
    end_time = timer()
    time_diff = end_time - start_time
    return activity, time_diff


def main():
    args = get_args()
    print('Loading configuration...')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    in_path = cfg['directories']['input']
    out_path = cfg['directories']['output']

    activity_times = defaultdict(dict)
    print('Splitting graphs...')
    for ds_nm in tqdm(cfg['datasets']):
        graph = readers.read_nx_gpickle(os.path.join(
            in_path, f'{ds_nm}.gpkl'
        ))

        snapshots = nxtools.split_graph(
            graph=graph,
            cumulative=False,
            **cfg['split-cfg']
        )
        nodes_activity = []
        for snap_id, snap in enumerate(snapshots[:-1]):
            na, nt = get_activity(snap)
            nodes_activity.append(na)
            activity_times[ds_nm][f'G_{snap_id}_{snap_id + 1}'] = nt

        full_snapshots = nxtools.split_graph(
            graph=graph,
            cumulative=True,
            **cfg['split-cfg']
        )[1:-1]

        export_snapshots(
            in_snapshots=snapshots,
            path=os.path.join(out_path, 'partial/', f'{ds_nm}/'),
            cumulative=False
        )
        export_activity(
            activity=nodes_activity,
            path=os.path.join(out_path, 'activity/', f'{ds_nm}/'),
        )
        export_snapshots(
            in_snapshots=full_snapshots,
            path=os.path.join(out_path, 'full/', f'{ds_nm}/'),
            cumulative=True,
            start_from=1
        )

    activity_times_path = cfg['directories']['activity-times-output']
    os.makedirs(activity_times_path, exist_ok=True)
    writers.export_obj_to_pickle(
        obj=activity_times,
        filepath=os.path.join(activity_times_path, 'activity-times.pkl')
    )


if __name__ == '__main__':
    main()
