import argparse
import os
import pickle
from collections import defaultdict

import yaml
from tqdm import tqdm

from dgem.embedding import KeyedModel
from dgem.embedding.incremental import calibration as cl
from dgem.embedding.incremental.fildne import BasicFILDNE, GeneralisedFILDNE
from dgem.tools import utils
from dgem.tools.io import readers, writers


def get_args():
    """Return args."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )

    return parser.parse_args()


def read_activity(path):
    """Reads activity."""
    activity = {}
    for snapshot in os.listdir(path):
        with open(os.path.join(path, snapshot), 'rb') as f:
            activity[snapshot] = pickle.load(f)
    return activity


def read_embeddings(path):
    """Reads embeddings."""
    unsorted = [
        (snapshot, KeyedModel.from_file(os.path.join(path, snapshot)))
        for snapshot in os.listdir(path)
    ]
    return list(sorted(unsorted, key=lambda x: int(x[0][2])))


def eval_task_model(data, embedding, cfg):
    """Evaluates task model."""
    model_cls = utils.import_fn(cfg['module'])
    clf = model_cls(embedding)
    clf.fit(data['x_train'], data['y_train'])
    report = clf.validate(data['x_test'], data['y_test']).as_dict()
    return report


def calculate_basic_fildne(embeddings, alpha):
    """Calculates Basic FILDNE."""
    fildne = BasicFILDNE(
        window_size=2,
        alpha=alpha
    )
    embedding = fildne.predict(embeddings)
    return embedding


def calculate_generalised_fildne(embeddings, last_snapshot, prior_distribution):
    """Calculates Generalised FILDNE."""
    fildne = GeneralisedFILDNE(window_size=len(embeddings))
    fildne.fit(
        embeddings,
        last_snapshot,
        prior_distribution
    )
    return fildne.predict(embeddings)


def perform_generalised_fildne_expirements(
        ds_name, emb_path, activity, snapshots, lp_datasets, cfg
):
    """Performs Generalised FILDNE experiments."""
    fildne_results = defaultdict(dict)

    for run_id in range(cfg["runs"]):
        embeddings = read_embeddings(os.path.join(emb_path, str(run_id)))

        for snapshot_num in range(9, 10):
            sub_embeddings = embeddings[:snapshot_num]
            sub_snapshots = snapshots[:snapshot_num]
            sub_lp_dataset = lp_datasets[snapshot_num - 1]

            calibrated_embeddings, _ = cl.calibrate(
                embeddings=[emb[1] for emb in sub_embeddings],
                activity=[
                    activity[ds_name][x[0].replace('F', 'G')]
                    for x in sub_embeddings
                ],
                num_workers=cfg['workers'],
                ref_nodes_config=cfg['datasets'][ds_name]['calibration']
            )

            gf_cfg = cfg['parameters']['generalisedfildne']
            for prior in gf_cfg['prior_distribution']:
                if prior not in fildne_results[f'F_0_{snapshot_num}']:
                    fildne_results[f'F_0_{snapshot_num}'][prior] = []

                fildne_embeddings = calculate_generalised_fildne(
                    calibrated_embeddings,
                    last_snapshot=sub_snapshots[-1],
                    prior_distribution=prior
                )
                fildne_results[f'F_0_{snapshot_num}'][prior].append(
                    eval_task_model(
                        embedding=fildne_embeddings,
                        data=sub_lp_dataset,
                        cfg=cfg['clf']
                    )
                )
    return fildne_results


def perform_basic_fildne_expirements(
        ds_name, emb_path, activity, lp_datasets, cfg
):
    """Performs Basic FILDNE experiments"""
    basic_fildne_results = defaultdict(dict)

    for run_id in range(cfg["runs"]):
        embeddings = read_embeddings(os.path.join(emb_path, str(run_id)))

        fildne_embeddings = embeddings[0][1]
        for alpha in cfg['parameters']['basicfildne']['alpha']:
            for snapshot_num in range(2, 10):
                curr_embedding = embeddings[snapshot_num - 1][1]
                sub_lp_dataset = lp_datasets[snapshot_num - 1]
                calibrated_embeddings, _ = cl.calibrate(
                    embeddings=[fildne_embeddings, curr_embedding],
                    activity=[
                        activity[ds_name][
                            f'G_{snapshot_num - 2}_{snapshot_num - 1}'
                        ],
                        activity[ds_name][
                            f'G_{snapshot_num - 1}_{snapshot_num}'
                        ],
                    ],
                    num_workers=cfg['workers'],
                    ref_nodes_config=cfg['datasets'][ds_name]['calibration']
                )
                fildne_embeddings = calculate_basic_fildne(
                    embeddings=calibrated_embeddings,
                    alpha=alpha
                )
                if alpha not in basic_fildne_results[f'F_0_{snapshot_num}']:
                    basic_fildne_results[f'F_0_{snapshot_num}'][alpha] = []

                basic_fildne_results[f'F_0_{snapshot_num}'][alpha].append(
                    eval_task_model(
                        embedding=fildne_embeddings,
                        data=sub_lp_dataset,
                        cfg=cfg['clf']
                    )
                )

    return basic_fildne_results


def main():
    """Main fn."""
    args = get_args()
    print('Loading configuration...')
    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    activity_path = cfg['directories']['input']['activity']
    activity = {
        dataset: read_activity(os.path.join(activity_path, dataset))
        for dataset in os.listdir(activity_path)
    }
    results = {}
    for ds_name in tqdm(cfg['datasets'].keys()):
        snapshots_path = os.path.join(
            cfg['directories']['input']['snapshots'],
            ds_name
        )
        lp_ds_path = os.path.join(
            cfg['directories']['input']['datasets'],
            ds_name
        )
        snapshots = [
            readers.read_pickle(os.path.join(snapshots_path, s))
            for s in sorted(os.listdir(snapshots_path))
        ]
        lp_datasets = [
            readers.read_pickle(
                os.path.join(lp_ds_path, f'0/DS_{idx}_{idx + 1}')
            )
            for idx in range(1, 10)
        ]

        results[ds_name] = {}
        for emb_name in tqdm(cfg['embeddings']):
            results[ds_name][emb_name] = {}

            emb_group, emb_model = emb_name.split('/')
            emb_path = cfg['directories']['input']['embeddings'].format(
                group=emb_group, name=emb_model, ds_name=ds_name
            )
            if not os.path.exists(emb_path):
                print(emb_path)
                continue

            results[ds_name][emb_name]['generalised-fildne'] = (
                perform_generalised_fildne_expirements(
                    ds_name=ds_name,
                    emb_path=emb_path,
                    activity=activity,
                    snapshots=snapshots,
                    lp_datasets=lp_datasets,
                    cfg=cfg
                )
            )
            results[ds_name][emb_name]['basic-fildne'] = (
                perform_basic_fildne_expirements(
                    ds_name=ds_name,
                    emb_path=emb_path,
                    activity=activity,
                    lp_datasets=lp_datasets,
                    cfg=cfg
                )
            )

    os.makedirs(os.path.dirname(cfg['directories']['output']), exist_ok=True)
    writers.export_obj_to_pickle(
        obj=results,
        filepath=cfg['directories']['output']
    )


if __name__ == '__main__':
    main()
