import argparse
import os
import pickle

import yaml
from tqdm import tqdm

from dgem.embedding import KeyedModel
from dgem.embedding.incremental import calibration as cl
from dgem.embedding.incremental.fildne import GeneralisedFILDNE
from dgem.tools import utils
from dgem.tools.io import readers, writers
from dgem.tools.graph import scoring


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )

    return parser.parse_args()


def read_activity(path):
    activity = {}
    for snapshot in os.listdir(path):
        with open(os.path.join(path, snapshot), 'rb') as f:
            activity[snapshot] = pickle.load(f)
    return activity


def read_embeddings(path):
    unsorted = [
        (snapshot, KeyedModel.from_file(os.path.join(path, snapshot)))
        for snapshot in os.listdir(path)
    ]
    return list(sorted(unsorted, key=lambda x: int(x[0][2])))


def eval_task_model(data, embedding, module_name):
    model_cls = utils.import_fn(module_name)
    clf = model_cls(embedding)
    clf.fit(data['x_train'], data['y_train'])
    report = clf.validate(data['x_test'], data['y_test']).as_dict()
    return report


def calculate_fildne(embeddings, last_snapshot):
    fildne = GeneralisedFILDNE(window_size=len(embeddings))
    fildne.fit(embeddings, last_snapshot, prior_distribution='uniform')
    return fildne.predict(embeddings)


def eval_calibration_scenario(
    ds_name, run_id,
    embeddings, activity, snapshots, lp_datasets,
    calibration_cfg, clf_module
):
    CALIBRATION_PARAMETERS = {
        'firstN': lambda c: [
            {'name': 'firstN', 'multiplier': m, 'max-percent': mp}
            for m in c['multiplier']
            for mp in c['max-percent']
        ],
        'percent': lambda c: [
            {'name': 'percent', 'percent': p}
            for p in c['percent']
        ],
        'threshold': lambda c: [
            {'name': 'threshold', 'threshold': th}
            for th in c['threshold']
        ],
    }

    results = []

    snapshot_idx = 9

    ccfgs = CALIBRATION_PARAMETERS[calibration_cfg['name']](calibration_cfg)

    for ccfg in tqdm(ccfgs, desc='Parameter value', leave=False):
        ac = [
            activity[emb[0].replace('F', 'G')]
            for emb in embeddings[:snapshot_idx]
        ]
        embs = [emb[1] for emb in embeddings[:snapshot_idx]]

        # Prepare calibration parameters for storing
        calibration_name = ccfg['name']
        calibration_params = {
            k: v
            for k, v in ccfg.items()
            if k != 'name'
        }

        # Get number of reference nodes
        rn_counts = []
        for idx in range(1, snapshot_idx):
            rn_scores = scoring.get_nodes_scores_for_current_snapshot(
                prev_snapshot_activity=ac[idx - 1],
                snapshot_activity=ac[idx],
                scoring_fn=scoring.basic_scoring_function
            )
            rnc = cl.get_reference_node_count(
                name=ccfg['name'],
                max_n_nodes=len(rn_scores),
                cfg=ccfg,
                emb_dim=embs[0].emb_dim,
                scores=rn_scores,
            )

            rn_counts.append((rnc, len(rn_scores.keys())))

        try:
            # Compute calibration and link prediction AUC
            calibrated_embeddings, _ = cl.calibrate(
                embeddings=embs,
                activity=ac,
                num_workers=-1,
                ref_nodes_config=ccfg,
            )
            auc = eval_task_model(
                embedding=calculate_fildne(
                    embeddings=calibrated_embeddings,
                    last_snapshot=snapshots[snapshot_idx]
                ),
                data=lp_datasets[snapshot_idx - 1],
                module_name=clf_module,
            )
        except:  # noqa: E722
            auc = None

        results.append((
            ds_name, snapshot_idx, rn_counts, run_id,
            calibration_name, calibration_params,
            auc,
        ))

    return results


def main():
    args = get_args()

    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    snapshots_path = cfg['directories']['input']['snapshots']
    embeddings_path = cfg['directories']['input']['embeddings']
    activity_path = cfg['directories']['input']['activity']
    lp_datasets_path = cfg['directories']['input']['datasets']

    clf_module = cfg['clf']['module']

    results = []

    for ds_name in tqdm(cfg['datasets'], desc='Dataset'):
        activity = read_activity(os.path.join(activity_path, ds_name))
        snapshots = [
            readers.read_pickle(os.path.join(snapshots_path, ds_name, s))
            for s in sorted(os.listdir(os.path.join(snapshots_path, ds_name)))
        ]
        lp_datasets = [
            readers.read_pickle(os.path.join(
                lp_datasets_path, ds_name, f'0/DS_{idx}_{idx + 1}'
            ))
            for idx in range(1, 10)
        ]

        for scenario in tqdm(cfg['calibration-parameters'],
                             desc='Calibration scenario',
                             leave=False):
            for run_id in tqdm(range(cfg['runs']), desc='Run', leave=False):
                embeddings = read_embeddings(os.path.join(
                    embeddings_path.format(ds_name=ds_name), str(run_id)
                ))

                ress = eval_calibration_scenario(
                    ds_name=ds_name,
                    run_id=run_id,
                    embeddings=embeddings,
                    activity=activity,
                    snapshots=snapshots,
                    lp_datasets=lp_datasets,
                    calibration_cfg=scenario,
                    clf_module=clf_module,
                )

                results.extend(ress)

    output_path = cfg['directories']['output']
    os.makedirs(os.path.dirname(output_path), exist_ok=True)
    writers.export_obj_to_pickle(obj=results, filepath=output_path)


if __name__ == '__main__':
    main()
