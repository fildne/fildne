"""Script for training Matrix Factorization based graph embeddings."""
import argparse
import importlib
import os
from collections import defaultdict
from timeit import default_timer as timer

import yaml
from tqdm import tqdm

from dgem.tools.io import readers, writers


def get_args():
    """Parses arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--common-config',
        help='Common config path',
        required=True
    )
    parser.add_argument(
        '--config',
        help='Config path',
        required=True
    )

    return parser.parse_args()


def create_embedding_model(model_cfg, model_args, emb_dim):
    """Imports class from given module."""
    module, cls = model_cfg['module'].rsplit('.', maxsplit=1)
    model_cls = getattr(importlib.import_module(module), cls)
    args = {**model_cfg.get('args', {}), **model_args}
    model = model_cls(dimensions=emb_dim, **args)
    return model


def run_embeddings(in_path, out_emb_path, model_cfg, model_args,
                   emb_dims, num_runs, skip_last):
    emb_times = {}

    for dataset_name in tqdm(model_args.keys(), desc='Datasets'):
        ds_path = os.path.join(in_path, dataset_name)

        emb_times[dataset_name] = defaultdict(list)

        snapshots = sorted(os.listdir(ds_path))

        if skip_last:
            snapshots = snapshots[:-1]

        for s_name in tqdm(snapshots, desc='Snapshots', leave=False):
            s_path = os.path.join(ds_path, s_name)
            snapshot = readers.read_pickle(s_path)

            emb_name = s_name.replace('G', 'F')

            for rid in tqdm(range(num_runs), desc='Run', leave=False):
                out_dir = os.path.join(out_emb_path, dataset_name, str(rid))
                os.makedirs(out_dir, exist_ok=True)

                emb_model = create_embedding_model(
                    model_cfg=model_cfg,
                    model_args=model_args[dataset_name],
                    emb_dim=emb_dims[dataset_name]
                )

                st_time = timer()

                emb = emb_model.embed(snapshot)

                end_time = timer()

                emb.to_pickle(filepath=os.path.join(out_dir, emb_name))
                emb_times[dataset_name][emb_name].append(end_time - st_time)

        emb_times[dataset_name] = dict(emb_times[dataset_name])

    return emb_times


def main():
    """Runs script."""
    # Get args
    args = get_args()

    with open(args.common_config, 'r') as fobj:
        common_cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    in_snapshots_path = common_cfg['directories']['snapshots-input']
    out_emb_path = os.path.join(
        common_cfg['directories']['emb-output'],
        cfg['model']['group'],
        cfg['model']['name'],
    )
    out_times_path = os.path.join(
        common_cfg['directories']['times-output'],
        cfg['model']['group'],
        cfg['model']['name'],
    )

    emb_dims = common_cfg['emb-dims']
    num_runs = cfg['runs']

    model_cfg = cfg['model']
    model_args = cfg['args']

    print('Partial snapshots embeddings')
    partial_times = run_embeddings(
        in_path=os.path.join(in_snapshots_path, 'partial/'),
        out_emb_path=os.path.join(out_emb_path, 'partial/'),
        model_cfg=model_cfg,
        model_args=model_args,
        emb_dims=emb_dims,
        num_runs=num_runs,
        skip_last=True,
    )

    print('Full snapshots embeddings')
    full_times = run_embeddings(
        in_path=os.path.join(in_snapshots_path, 'full/'),
        out_emb_path=os.path.join(out_emb_path, 'full/'),
        model_cfg=model_cfg,
        model_args=model_args,
        emb_dims=emb_dims,
        num_runs=num_runs,
        skip_last=False,
    )

    # Combine time measurements
    dataset_names = partial_times.keys()
    emb_times = {
        k: {**partial_times[k], **full_times[k]}
        for k in dataset_names
    }

    # Save time measurements
    os.makedirs(out_times_path, exist_ok=True)
    writers.export_obj_to_pickle(
        obj=emb_times,
        filepath=os.path.join(out_times_path, 'embedding.pkl')
    )


if __name__ == '__main__':
    main()
