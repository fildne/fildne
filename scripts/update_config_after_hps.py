"""Updates model hyper-parameters after hps search."""
import argparse
import json
import os

import yaml

O2V_DEFAULT_PARAMS = {
    'temp-noise': False,
    'loss': "square",
    'init': "uniform",
}
STREAMWALK_DEFAULT_PARAMS = {
    'cutoff': 0,
    **O2V_DEFAULT_PARAMS
}
SOR_HPS_PARAMS_MAPPING = {
    'is-decayed': [False, True],
    'is-fw': [False, True],
    'onlymirror': [False, True],
    'neg-rate': [5, 10, 15],
    'hash-num': [10, 20, 30],
    'hash-type': ['mod', 'mul', 'map'],
    'incr-condition': [False, True],
}
STREAMWALK_HPS_PARAMS_MAPPING = {
    'is-decayed': [False, True],
    'is-fw': [False, True],
    'onlymirror': [False, True],
    'max-length': [2, 3, 4],
    'k': [4, 8, 12],
    'neg-rate': [5, 10, 15],
}
TNE_HPS_PARAMS_MAPPING = {
    'batch_size': [8, 16, 32, 64, 128, 256],
    'do_align': [True, False]
}
DGI_HPS_PARAMS_MAPPING = {
    'nb_epochs': list(range(20, 500 + 1, 20))
}
N2V_HPS_DEFAULT_ARGS = {
    'workers': 24,
    'w2v_args': {
        'epochs': 5,
        'min_count': 1,
        'sg': 1,
        'workers': 24
    }
}
N2V_HPS_PARAMS_MAPPING = {
    'nb_walks_per_node': list(range(10, 100 + 1, 10)),
    'walk_length': list(range(10, 100 + 1, 10))
}
CTNDE_HPS_PARAMS_MAPPING = {
    'nb_walks_per_node': list(range(10, 50 + 1, 10)),
    'max_length': list(range(10, 100 + 1, 10))
}
LINE_HPS_PARAMS_MAPPING = {
    'negative_samples': list(range(2, 10 + 1, 1))
}
CTDNE_HPS_DEFAULT_ARGS = {
    'initial_edge_sampling_fn': 'dgem.embedding.random_walk.edge_sampling.edge_uniform',  # noqa: E501
    'edge_sampling_fn': 'dgem.embedding.random_walk.edge_sampling.edge_uniform',
    'w2v_args': {
        'epochs': 5,
        'min_count': 1,
        'sg': 1,
        'workers': 24,
    },
    'workers': 24
}
LINE_DEFAULT_ARGS = {
    'order': 'FILL_MANUALLY'
}
AERNN_HPS_PARAMS_MAPPING = {
    'n_batch': [64, 128, 256]
}
METHODS = {
    'sor': {
        'default': O2V_DEFAULT_PARAMS,
        'mapping': SOR_HPS_PARAMS_MAPPING
    },

    'streamwalk': {
        'default': STREAMWALK_DEFAULT_PARAMS,
        'mapping': STREAMWALK_HPS_PARAMS_MAPPING
    },

    'tne': {
        'default': {},
        'mapping': TNE_HPS_PARAMS_MAPPING
    },

    'dgi': {
        'default': {},
        'mapping': DGI_HPS_PARAMS_MAPPING
    },

    'hope': {
        'default': {},
        'mapping': {}
    },

    'ctdne': {
        'default': CTDNE_HPS_DEFAULT_ARGS,
        'mapping': CTNDE_HPS_PARAMS_MAPPING
    },

    'n2v': {
        'default': N2V_HPS_DEFAULT_ARGS,
        'mapping': N2V_HPS_PARAMS_MAPPING
    },

    'line': {
        'default': LINE_DEFAULT_ARGS,
        'mapping': LINE_HPS_PARAMS_MAPPING
    },

    'aernn': {
        'default': {},
        'mapping': AERNN_HPS_PARAMS_MAPPING
    }
}
MODULES = {
    'dgi': 'dgem.embedding.gcn.dgi.DGIEmbedding',
    'hope': 'dgem.embedding.mf.HOPEEmbedding',
    'ctdne': 'dgem.embedding.random_walk.ctdne.CTDNEEmbedding',
    'line': 'dgem.embedding.other.LINE',
    'n2v': 'dgem.embedding.random_walk.n2v.Node2VecEmbedding',
    'aernn': 'dgem.embedding.dyngraph.aernn.DynGraphAERNN'
}


def get_args():
    """Parses script arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--model-name',
        help='Name of the model',
        required=True,
        type=str,
    )
    parser.add_argument(
        '--model-group',
        help='Name of the model group',
        required=True,
        type=str,
    )
    parser.add_argument(
        '--nb-runs',
        help='Number of runs',
        required=True,
        type=int,
    )
    return parser.parse_args()


def main():
    """Performs parameters update."""
    args = get_args()
    model_name = args.model_name
    model_group = args.model_group

    if model_name not in METHODS.keys():
        raise ValueError(f'Model {model_name} unrecognized.')

    path = f'data/hps/{model_name}/'
    results_path = {
        it: os.path.join(path, f'{it}/best_config.json')
        for it in os.listdir(path) if os.path.isdir(os.path.join(path, it))
    }

    output = {
        'model': {
            'group': model_group,
            'name': model_name
        },
        'args': {},
        'runs': args.nb_runs
    }
    if model_name in MODULES:
        output['model']['module'] = MODULES[model_name]

    default_args = METHODS[model_name]['default']
    args_mapping = METHODS[model_name]['mapping']

    for ds, res_ds_path in results_path.items():
        with open(res_ds_path, 'r') as f:
            hps_cfg = json.load(f)

        ds_model_args = {
            **default_args
        }

        for p, p_value in hps_cfg.items():
            if p in args_mapping.keys():
                ds_model_args[p] = args_mapping[p][p_value]
            else:
                ds_model_args[p] = p_value

        output['args'][ds] = ds_model_args

    output_path = f'experiments/configs/train-embeddings/' \
                  f'{model_group}/{model_name}.yml'

    with open(output_path, 'w') as fobj:
        yaml.dump(data=output, stream=fobj)


if __name__ == '__main__':
    main()
