import argparse
import importlib
import os
from copy import deepcopy

import networkx as nx
import tensorflow as tf
import yaml
from memory_profiler import memory_usage

from dgem.embedding.tnodeembed.src.wrapper import TNEEmbedding
from dgem.tools.io import readers, writers

tf.config.threading.set_intra_op_parallelism_threads(1)
tf.config.threading.set_inter_op_parallelism_threads(1)


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--ds',
        help='Dataset name',
        required=True
    )
    parser.add_argument(
        '--config',
        help='Embedding config',
        required=True
    )
    parser.add_argument(
        '--snapshot',
        help='Snapshot id',
        required=True
    )
    parser.add_argument(
        '--run',
        help='Run id',
        required=True
    )
    return parser.parse_args()


def create_embedding_model(model_cfg, model_args, emb_dim):
    """Imports class from given module."""
    module, cls = model_cfg['module'].rsplit('.', maxsplit=1)
    model_cls = getattr(importlib.import_module(module), cls)
    args = {**model_cfg.get('args', {}), **model_args}
    model = model_cls(dimensions=emb_dim, **args)
    return model


def train_embedding(
        n2v_model, n2v_args, embeddings_path, dims, snapshots_path, ds_path,
        model_cfg, graph_path, task
):
    graph = readers.read_nx_gpickle(graph_path)
    emb_model = create_embedding_model(
        model_cfg=n2v_model,
        model_args=n2v_args,
        emb_dim=dims
    )
    emb = emb_model.embed(graph)
    del graph
    embeddings = []
    if embeddings_path:
        embeddings = [readers.read_pickle(file) for file in embeddings_path]

    embeddings.append({
        'vectors': emb.to_dict(),
    })

    ds = readers.read_pickle(ds_path)

    batch_size = model_cfg.pop('batch_size')
    snapshots = [
        readers.read_nx_gpickle(path)
        for path in snapshots_path
    ]

    directed = False
    if nx.is_directed(snapshots[-1]):
        directed = True

    tne_model = TNEEmbedding(
        args=model_cfg,
        directed=directed,
        verbose=0
    )
    res, time = tne_model.eval(
        snapshots=snapshots,
        embeddings=embeddings,
        lp_dataset=ds,
        batch_size=batch_size,
        task=task
    )

    return emb


def main():
    args = get_args()
    snapshot_last_id = int(args.snapshot.split('_')[-1])

    ds_path = f'data/task/lp/datasets/{args.ds}/0/' \
              f'DS_{snapshot_last_id}_{snapshot_last_id + 1}'

    snapshots_path = [
        f'data/snapshots/partial/{args.ds}/G_{it}_{it + 1}'
        for it in range(snapshot_last_id)
    ]

    if snapshot_last_id == 1:
        graph_path = snapshots_path[0]
    else:
        graph_path = f'data/snapshots/full/{args.ds}/G_0_{snapshot_last_id}'

    n2v_emb_path = f'data/mm/streaming/tne/{args.ds}/{args.run}/emb/'
    embeddings_path = [
        os.path.join(
            n2v_emb_path,
            f'F_0_{it + 1}'
        )
        for it in range(0, snapshot_last_id - 1)
    ]

    n2v_config = "experiments/configs/train-embeddings/rw/n2v.yml"
    with open(n2v_config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
    n2v_cfg = deepcopy(cfg['model'])
    n2v_args = deepcopy(cfg['args'][args.ds])
    del cfg
    if 'workers' in n2v_args.keys():
        n2v_args['workers'] = 1

    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)

    model_cfg = deepcopy(cfg['args'][args.ds])
    task = deepcopy(cfg['task'])
    del cfg

    dimmensions_cfg = 'experiments/configs/train-embeddings/common.yml'
    with open(dimmensions_cfg, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
    dims = deepcopy(cfg['emb-dims'][args.ds])
    del cfg

    fn = lambda: train_embedding(  # noqa: E731
        n2v_model=n2v_cfg,
        n2v_args=n2v_args,
        embeddings_path=embeddings_path,
        snapshots_path=snapshots_path,
        ds_path=ds_path,
        model_cfg=model_cfg,
        task=task,
        dims=dims,
        graph_path=graph_path
    )

    (peak_mem, ret_emb) = memory_usage(
        proc=fn,
        interval=.01,
        include_children=True,
        multiprocess=True,
        max_usage=False,
        retval=True
    )

    output_path = f'data/mm/streaming/tne/{args.ds}/{args.run}/'
    os.makedirs(output_path, exist_ok=True)
    writers.export_obj_to_pickle(peak_mem, filepath=os.path.join(
        f'{output_path}/{args.snapshot}'
    ))

    emb_output_path = os.path.join(output_path, 'emb/')
    os.makedirs(emb_output_path, exist_ok=True)
    emb_id = args.snapshot.replace('G', 'F')
    ret_emb.to_pickle(filepath=os.path.join(
        f'{emb_output_path}/{emb_id}'
    ))
    print(max(peak_mem))


if __name__ == '__main__':
    main()
