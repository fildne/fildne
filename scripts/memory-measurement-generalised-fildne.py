import argparse
import importlib
import os
from copy import deepcopy

import yaml
from memory_profiler import memory_usage

from dgem.embedding import KeyedModel
from dgem.embedding.incremental import calibration as cl
from dgem.embedding.incremental.fildne import GeneralisedFILDNE
from dgem.tools.io import readers, writers


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--ds',
        help='Dataset name',
        required=True
    )
    parser.add_argument(
        '--config',
        help='Embedding config',
        required=True
    )
    parser.add_argument(
        '--emb-config',
        help='Base embedding config',
        required=True
    )
    parser.add_argument(
        '--snapshot',
        help='Snapshot id',
        required=True
    )
    parser.add_argument(
        '--run',
        help='Run id',
        required=True
    )
    return parser.parse_args()


def create_embedding_model(model_cfg, model_args, emb_dim):
    """Imports class from given module."""
    module, cls = model_cfg['module'].rsplit('.', maxsplit=1)
    model_cls = getattr(importlib.import_module(module), cls)
    args = {**model_cfg.get('args', {}), **model_args}
    model = model_cls(dimensions=emb_dim, **args)
    return model


def train_embedding(
        calibration_args, fildne_cfg, emb_cfg, emb_args, dims,
        snapshots_path, activity_path, embeddings
):
    embeddings = [
        KeyedModel.from_file(it) for it in embeddings
    ]

    graph = readers.read_nx_gpickle(snapshots_path[-1])
    emb_model = create_embedding_model(
        model_cfg=emb_cfg,
        model_args=emb_args,
        emb_dim=dims
    )
    snapshot_emb = emb_model.embed(graph)

    activity = [
        readers.read_pickle(activity_path[0]),
        readers.read_pickle(activity_path[1])
    ]
    snapshot_emb, times = cl.calibrate(
        embeddings=[
            embeddings[-1], snapshot_emb
        ],
        activity=activity,
        ref_nodes_config=calibration_args,
        num_workers=1,
    )
    embeddings.append(snapshot_emb[-1])

    fildne = GeneralisedFILDNE(window_size=len(embeddings))
    fildne.fit(
        embeddings,
        graph,
        prior_distribution=fildne_cfg['prior']
    )
    fildne_embedding = fildne.predict(embeddings)  # noqa: F841
    return embeddings[-1]


def main():
    args = get_args()
    snapshot_last_id = int(args.snapshot.split('_')[-1])

    activity_path = [
        f'data/snapshots/activity/{args.ds}/'
        f'G_{snapshot_last_id - 2}_{snapshot_last_id - 1}',
        f'data/snapshots/activity/{args.ds}/'
        f'G_{snapshot_last_id - 1}_{snapshot_last_id}'
    ]

    snapshots_path = [
        f'data/snapshots/partial/{args.ds}/'
        f'G_{snapshot_last_id - 1}_{snapshot_last_id}'
    ]

    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
        calibration_cfg = deepcopy(cfg['args'][args.ds]['calibration'])
    fildne_cfg = deepcopy(cfg['args'][args.ds]['fildne'])
    del cfg

    with open(args.emb_config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
    model_cfg = deepcopy(cfg['model'])
    model_args = deepcopy(cfg['args'][args.ds])
    del cfg
    if 'workers' in model_args.keys():
        model_args['workers'] = 1
    if model_cfg['name'] == 'sge':
        model_args['n_jobs'] = 1
    if model_cfg['name'] == 'dgi':
        import torch
        torch.set_num_threads(1)
    if model_cfg['name'] == 'line':
        import tensorflow as tf
        tf.config.threading.set_intra_op_parallelism_threads(1)
        tf.config.threading.set_inter_op_parallelism_threads(1)

    dimmensions_cfg = 'experiments/configs/train-embeddings/common.yml'
    with open(dimmensions_cfg, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
    dims = deepcopy(cfg['emb-dims'][args.ds])
    del cfg

    embeddings = [
        f'data/mm/{model_cfg["group"]}/'
        f'{model_cfg["name"]}/{args.ds}/{args.run}/emb/'
        f'F_0_1'
    ]

    for emb_id in range(1, snapshot_last_id - 1):
        emb_path = f'data/mm/fildne/{model_cfg["group"]}/' \
                   f'{model_cfg["name"]}/{args.ds}/{args.run}/emb/'

        embeddings.append(
            os.path.join(emb_path, f'F_{emb_id}_{emb_id + 1}')
        )

    fn = lambda: train_embedding(  # noqa: E731
        calibration_args=calibration_cfg,
        fildne_cfg=fildne_cfg,
        emb_cfg=model_cfg,
        emb_args=model_args,
        snapshots_path=snapshots_path,
        activity_path=activity_path,
        embeddings=embeddings,
        dims=dims
    )

    (peak_mem, ret_emb) = memory_usage(  # noqa: E731
        proc=fn,
        interval=.01,
        include_children=True,
        multiprocess=True,
        max_usage=False,
        retval=True
    )

    output_path = f'data/mm/fildne/{model_cfg["group"]}/' \
                  f'{model_cfg["name"]}/{args.ds}/{args.run}/'
    os.makedirs(output_path, exist_ok=True)
    writers.export_obj_to_pickle(peak_mem, filepath=os.path.join(
        f'{output_path}/{args.snapshot}'
    ))

    emb_output_path = os.path.join(output_path, 'emb/')
    os.makedirs(emb_output_path, exist_ok=True)
    ret_emb.to_pickle(
        filepath=os.path.join(
            f'{emb_output_path}/F_{snapshot_last_id - 1}_{snapshot_last_id}'
        )
    )
    print(max(peak_mem))


if __name__ == '__main__':
    main()
