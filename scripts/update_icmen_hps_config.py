"""Updates model hyper-parameters after hps search."""
import argparse
import json
import os

import yaml

FILDNE_PARAMS_MAPPING = {
    'activity': 'calibration',
    'name': 'calibration',
    'percent': 'calibration',
    'multiplier': 'calibration',
    'max_percent': 'calibration',
    'threshold': 'calibration',
    'prior': 'fildne',
    'alpha': 'fildne'
}
FILDNE_TYPE_MAPPING = {
    'basicfildne': 'basic',
    'fildne': 'generalised'
}

FILDNE_HPS_PARAMS_MAPPING = {
    'fildne': {
        'activity': ['percent', 'firstN', 'threshold'],
        'prior': ['uniform', 'increase']
    },
    'basicfildne': {
        'activity': ['percent', 'firstN', 'threshold'],
    }
}


def get_args():
    """Parses script arguments."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--model-name',
        help='Name of the model',
        required=True,
        type=str,
    )
    parser.add_argument(
        '--fildne-method',
        help='Name of the fildne method',
        required=True,
        type=str,
    )

    return parser.parse_args()


def main():
    """Performs parameters update."""
    args = get_args()
    model_name = args.model_name
    fildne_method = args.fildne_method

    if fildne_method not in FILDNE_TYPE_MAPPING.keys():
        raise ValueError(f'FILDNE method {fildne_method} unrecognized.')

    fildne_type = FILDNE_TYPE_MAPPING[fildne_method]
    path = f'data/hps/{fildne_method}/{model_name}'

    results_path = {
        it: os.path.join(path, f'{it}/best_config.json')
        for it in os.listdir(path)
    }

    output = {
        'model': {
            'group': model_name.split('/')[0],
            'name': model_name.split('/')[1]
        },
        'fildne-type': fildne_type,
        'args': {}
    }
    for ds, res_ds_path in results_path.items():
        with open(res_ds_path, 'r') as f:
            hps_cfg = json.load(f)

        for p, mapping in FILDNE_HPS_PARAMS_MAPPING[fildne_method].items():
            p_name = p
            if p == 'activity':
                p_name = 'name'

            hps_cfg[p_name] = mapping[hps_cfg[p]]

        output_cfg = {
            'calibration': {},
            'fildne': {}
        }
        for p, pval in hps_cfg.items():
            p_name = p
            if p == 'max_percent':
                p_name = 'max-percent'
            elif p == 'activity':
                continue
            output_cfg[FILDNE_PARAMS_MAPPING[p]][p_name] = pval
        output['args'][ds] = output_cfg

        output_path = f'experiments/configs/run-fildne/' \
                      f'{fildne_type}/{model_name}.yml'
        with open(output_path, 'w') as fobj:
            yaml.dump(data=output, stream=fobj)


if __name__ == '__main__':
    main()
