import argparse
import os
from copy import deepcopy

import networkx as nx
import yaml
from memory_profiler import memory_usage

from dgem.embedding.onlinen2v import wrappers as on2v_wrap
from dgem.tools.io import readers, writers


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--ds',
        help='Dataset name',
        required=True
    )
    parser.add_argument(
        '--config',
        help='Embedding config',
        required=True
    )
    parser.add_argument(
        '--snapshot',
        help='Snapshot id',
        required=True
    )
    parser.add_argument(
        '--run',
        help='Run id',
        required=True
    )
    return parser.parse_args()


def train_embedding(
        model_cfg, model_args, snapshots_path, dims, prev_model, snapshot_id,
        nodes
):
    snapshots = [
        readers.read_nx_gpickle(path)
        for path in snapshots_path
    ]
    if not prev_model:
        if isinstance(snapshots[0], (nx.DiGraph, nx.MultiDiGraph)):
            mirror = False
        else:
            mirror = True

        online_n2v = on2v_wrap.create_model(
            model_name=model_cfg,
            args=model_args,
            emb_dim=dims,
            mirror=mirror,
        )
        online_n2v.learner.set_all_words(nodes)
        online_n2v.sampled_pairs = []
        online_n2v.sum_train_time = 0.0
    else:
        online_n2v = readers.read_pickle(prev_model)

    graph_df = on2v_wrap.snapshots_to_df(snapshots)
    graph_df['sid'] = snapshot_id - 1
    graph_df = graph_df[graph_df["src"] != graph_df["trg"]]
    graph_df = graph_df.sort_values(by='time')
    emb = on2v_wrap.embed(  # noqa: F841
        on2v=online_n2v,
        snapshot_data=graph_df
    )
    return online_n2v


def main():
    args = get_args()
    snapshot_last_id = int(args.snapshot.split('_')[-1])

    snapshots_path = [
        f'data/snapshots/partial/{args.ds}/'
        f'G_{snapshot_last_id - 1}_{snapshot_last_id}'
    ]

    dimmensions_cfg = 'experiments/configs/train-embeddings/common.yml'
    with open(dimmensions_cfg, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
    dims = deepcopy(cfg['emb-dims'][args.ds])
    del cfg

    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
    model_cfg = deepcopy(cfg['model']['name'])
    model_args = deepcopy(cfg['args'][args.ds])
    del cfg

    nodes = None
    prev_model = None
    if snapshot_last_id == 1:
        graph = readers.read_nx_gpickle(
            f'data/snapshots/full/{args.ds}/G_0_9'
        )
        nodes = [str(it) for it in graph.nodes()]
        del graph
    else:
        prev_model = f'data/mm/streaming/{model_cfg}/' \
                     f'{args.ds}/{args.run}/emb//F_0_{snapshot_last_id - 1}'

    fn = lambda: train_embedding(  # noqa: E731
        model_cfg=model_cfg,
        model_args=model_args,
        snapshots_path=snapshots_path,
        dims=dims,
        nodes=nodes,
        prev_model=prev_model,
        snapshot_id=snapshot_last_id
    )

    (peak_mem, ret_emb) = memory_usage(
        proc=fn,
        interval=.01,
        include_children=True,
        multiprocess=True,
        max_usage=False,
        retval=True
    )

    output_path = f'data/mm/streaming/{model_cfg}/{args.ds}/{args.run}/'
    os.makedirs(output_path, exist_ok=True)
    writers.export_obj_to_pickle(peak_mem, filepath=os.path.join(
        f'{output_path}/{args.snapshot}'
    ))

    emb_output_path = os.path.join(output_path, 'emb/')
    os.makedirs(emb_output_path, exist_ok=True)
    writers.export_obj_to_pickle(
        obj=ret_emb,
        filepath=os.path.join(
            f'{emb_output_path}/F_0_{snapshot_last_id}'
        )
    )
    print(max(peak_mem))


if __name__ == '__main__':
    main()
