from typing import Dict, Union, List, Callable

from matplotlib import pyplot as plt
import networkx as nx
import numpy as np
import pandas as pd
import seaborn as sns

import dgem.embedding.incremental.calibration
from dgem.tools.graph import nxtools
from dgem.tools.graph.scoring import (
    basic_scoring_function as scoring_function,
)

PATH = './data/raw'

DATASETS = [
    'fb-forum',
    'fb-messages',
    'ia-enron-employees',
    'ia-hypertext',
    'ia-radoslaw-email',
    # 'ca-cit-HepTh',
    # 'ia-enron-email-dynamic',
    # 'tech-as-topology',
    # 'sx-mathoverflow'
]


def get_activity(graph: nx.Graph) -> Dict[Union[int, str], float]:
    return dict(dgem.embedding.incremental.calibration.get_activity_dict(graph))


def plot_activity(
        activity: List[Dict[Union[int, str], float]],
        dataset_name: str,
        ax: plt.Axes
):
    num_nodes = len(get_nodes_from_stream(activity))
    y = [len(overlap) for overlap in get_nodes_overlap(activity)]
    ax.scatter(
        [x - 0.5 for x in range(1, len(y) + 1)],
        list(map(lambda x: x / num_nodes, y)),
        s=20
    )
    for idx, value in enumerate(y, 1):
        ax.text(idx - 0.58, value / num_nodes - 0.05, s=str(value))
    ax.set_ylim((0, 1))
    ax.set_ylabel('Overlap fractions', fontsize=14)
    ax.set_title(f'Dataset: {dataset_name} - {num_nodes} nodes', fontsize=20)
    values = []
    idxs = []
    for idx, batch in enumerate(activity, 1):
        values += list(batch.values())
        idxs += [idx for _ in batch.keys()]
    d = pd.DataFrame.from_dict({
        'idx': idxs,
        'values': list(map(float, values))
    })
    ax2 = ax.twinx()
    sns.boxenplot(x='idx', y='values', data=d, ax=ax2)
    ax2.grid(False)
    ax2.set_ylabel('Activity distribution', fontsize=14)
    ax.set_xlabel('Snapshot ID', fontsize=14)


def get_nodes_overlap(activity):
    return [set(batch.keys()).intersection(activity[idx - 1].keys())
            for idx, batch in enumerate(activity[1:], 1)]


def get_nodes_from_stream(activity: List[Dict[Union[int, str], float]]):
    return set.union(*[set(batch.keys()) for batch in activity])


def plot_rankings_similarity(
        activity: List[Dict[Union[int, str], float]],
        function: Callable,
        ax: plt.Axes
):
    percentiles = [[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]]
    d = pd.DataFrame(columns=['idx', 'scores'])
    for idx, batch in enumerate(activity[1:], 1):
        overlaping_nodes = (set(batch.keys())
                            .intersection(activity[idx - 1].keys()))
        scores = [function(batch[node], activity[idx - 1][node])
                  for node in overlaping_nodes]
        percentiles.append([np.sum(np.array(scores) <= s)
                            for s in percentiles[0]])
        d = pd.concat([
            d,
            pd.DataFrame.from_dict({
                'idx': [idx + 0.5 for _ in overlaping_nodes],
                'scores': scores
            })
        ])
    df = pd.DataFrame(percentiles[1:], columns=list(map(str, percentiles[0])))
    print(df)
    sns.boxenplot(x='idx', y='scores', data=d, ax=ax)
    ax.set_xlabel('Intersection ID', fontsize=14)
    ax.set_xlim(ax.get_xlim()[0] - 0.5, ax.get_xlim()[1] + 0.5)


def main(dataset, path=PATH, split_type='time'):
    graph = nx.read_gpickle(f'{path}/{dataset}.gpkl')
    graphs_stream = nxtools.split_graph(graph, 10, split_type)
    nodes_activity = [get_activity(batch) for batch in graphs_stream]
    print('Num nodes: ', [batch.number_of_nodes() for batch in graphs_stream])
    print('Num edges: ', [batch.number_of_edges() for batch in graphs_stream])
    fig, ax = plt.subplots(ncols=1, nrows=2, figsize=(18, 12))
    plot_activity(nodes_activity, dataset, ax.ravel()[0])
    plot_rankings_similarity(nodes_activity, scoring_function, ax.ravel()[1])
    plt.show()


if __name__ == '__main__':
    for dataset in DATASETS:
        main(dataset)
