#!/usr/bin/env bash

for model in 'dyngraph/aernn'; do
  CONFIG="experiments/configs/train-embeddings/${model}.yml"
   for ds in 'bitcoin-alpha' 'bitcoin-otc' 'fb-forum' 'fb-messages' 'ia-enron-employees' 'ia-radoslaw-email' 'ia-hypertext'; do
    for snapshot in 'G_0_2' 'G_0_3' 'G_0_4' 'G_0_5' 'G_0_6' 'G_0_7' 'G_0_8' 'G_0_9'; do
      for run in 0 1 2 3 4; do
        PYTHONPATH=. python3 scripts/memory-measurement-dyngraph.py --ds "${ds}" --config "${CONFIG}" --snapshot "${snapshot}" --run "${run}"
      done
    done
  done
done

