import argparse
import importlib
import os
from copy import deepcopy

import tensorflow as tf
import yaml
from memory_profiler import memory_usage

from dgem.tools.io import readers, writers

tf.config.threading.set_intra_op_parallelism_threads(1)
tf.config.threading.set_inter_op_parallelism_threads(1)


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--ds',
        help='Dataset name',
        required=True
    )
    parser.add_argument(
        '--config',
        help='Embedding config',
        required=True
    )
    parser.add_argument(
        '--snapshot',
        help='Snapshot id',
        required=True
    )
    parser.add_argument(
        '--run',
        help='Run id',
        required=True
    )
    return parser.parse_args()


def create_embedding_model(model_cfg, model_args, emb_dim):
    """Imports class from given module."""
    module, cls = model_cfg['module'].rsplit('.', maxsplit=1)
    model_cls = getattr(importlib.import_module(module), cls)
    args = {**model_cfg.get('args', {}), **model_args}
    model = model_cls(dimensions=emb_dim, **args)
    return model


def train_embedding(
        model_cfg, model_args, snapshots_path, dims, nodes
):
    snapshots = [
        readers.read_nx_gpickle(path)
        for path in snapshots_path
    ]
    emb_model = create_embedding_model(
        model_cfg=model_cfg,
        model_args=model_args,
        emb_dim=dims
    )

    emb = emb_model.embed(snapshots, nodes)
    return emb


def main():
    args = get_args()
    snapshot_last_id = int(args.snapshot.split('_')[-1])

    snapshots_path = [
        f'data/snapshots/partial/{args.ds}/G_{it}_{it + 1}'
        for it in range(snapshot_last_id)
    ]

    graph_nodes = list(readers.read_pickle(
        os.path.join(
            'data/snapshots/full',
            f'{args.ds}/G_0_{snapshot_last_id}')
    ).nodes())

    dimmensions_cfg = 'experiments/configs/train-embeddings/common.yml'
    with open(dimmensions_cfg, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
    dims = deepcopy(cfg['emb-dims'][args.ds])
    del cfg

    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
    model_cfg = deepcopy(cfg['model'])
    model_args = deepcopy(cfg['args'][args.ds])
    del cfg

    fn = lambda: train_embedding(  # noqa: E731
        model_cfg=model_cfg,
        model_args=model_args,
        snapshots_path=snapshots_path,
        dims=dims,
        nodes=graph_nodes,
    )

    (peak_mem, ret_emb) = memory_usage(
        proc=fn,
        interval=.01,
        include_children=True,
        multiprocess=True,
        max_usage=False,
        retval=True
    )

    output_path = f'data/mm/dyngraph/aernn/{args.ds}/{args.run}/'
    os.makedirs(output_path, exist_ok=True)
    writers.export_obj_to_pickle(peak_mem, filepath=os.path.join(
        f'{output_path}/{args.snapshot}'
    ))

    print(max(peak_mem))


if __name__ == '__main__':
    main()
