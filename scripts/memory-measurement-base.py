import argparse
import importlib
import os
from copy import deepcopy

import yaml
from memory_profiler import memory_usage
from dgem.tools.io import readers, writers


def create_embedding_model(model_cfg, model_args, emb_dim):
    """Imports class from given module."""
    module, cls = model_cfg['module'].rsplit('.', maxsplit=1)
    model_cls = getattr(importlib.import_module(module), cls)
    args = {**model_cfg.get('args', {}), **model_args}
    model = model_cls(dimensions=emb_dim, **args)
    return model


def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--ds',
        help='Dataset name',
        required=True
    )
    parser.add_argument(
        '--snapshot',
        help='Snapshot id',
        required=True
    )
    parser.add_argument(
        '--snapshot-type',
        help='Snapshot type',
        required=False,
        default='full'
    )
    parser.add_argument(
        '--config',
        help='Embedding config',
        required=True
    )
    parser.add_argument(
        '--run',
        help='Run id',
        required=True
    )
    return parser.parse_args()


def train_embedding(model_cfg, model_args, snapshot_path, dims):
    graph = readers.read_nx_gpickle(snapshot_path)
    emb_model = create_embedding_model(
        model_cfg=model_cfg,
        model_args=model_args,
        emb_dim=dims
    )
    emb = emb_model.embed(graph)
    return emb


def main():
    args = get_args()
    snapshot_last_id = int(args.snapshot.split('_')[-1])
    if snapshot_last_id == 1:
        snapshot_path = f'data/snapshots/partial/' \
                        f'/{args.ds}/{args.snapshot}'
    else:
        snapshot_path = f'data/snapshots/{args.snapshot_type}' \
                        f'/{args.ds}/{args.snapshot}'

    with open(args.config, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
    model_cfg = deepcopy(cfg['model'])
    model_args = deepcopy(cfg['args'][args.ds])
    del cfg
    if 'workers' in model_args.keys():
        model_args['workers'] = 1
    if model_cfg['name'] == 'sge':
        model_args['n_jobs'] = 1
    if model_cfg['name'] == 'dgi':
        import torch
        torch.set_num_threads(1)
    if model_cfg['name'] == 'line':
        import tensorflow as tf
        tf.config.threading.set_intra_op_parallelism_threads(1)
        tf.config.threading.set_inter_op_parallelism_threads(1)

    dimmensions_cfg = 'experiments/configs/train-embeddings/common.yml'
    with open(dimmensions_cfg, 'r') as fobj:
        cfg = yaml.load(fobj, Loader=yaml.FullLoader)
    dims = deepcopy(cfg['emb-dims'][args.ds])
    del cfg

    fn = lambda: train_embedding(  # noqa: E731
        model_cfg=model_cfg,
        model_args=model_args,
        snapshot_path=snapshot_path,
        dims=dims
    )

    (peak_mem, ret_emb) = memory_usage(
        proc=fn,
        interval=.01,
        include_children=True,
        multiprocess=True,
        max_usage=False,
        retval=True
    )

    output_path = f'data/mm/{model_cfg["group"]}/' \
                  f'{model_cfg["name"]}/{args.ds}/{args.run}/'
    os.makedirs(output_path, exist_ok=True)
    writers.export_obj_to_pickle(peak_mem, filepath=os.path.join(
        f'{output_path}/{args.snapshot}'
    ))

    emb_output_path = os.path.join(output_path, 'emb/')
    os.makedirs(emb_output_path, exist_ok=True)
    emb_id = args.snapshot.replace('G', 'F')
    ret_emb.to_pickle(filepath=os.path.join(
        f'{emb_output_path}/{emb_id}'
    ))


if __name__ == '__main__':
    main()
