#!/usr/bin/env bash

for model in 'rw/n2v' 'rw/ctdne' 'mf/lle' 'mf/hope' 'mf/sge' 'gcn/dgi' 'other/line'; do
  CONFIG="experiments/configs/run-fildne/generalised/${model}.yml"
  BASE_CONFIG="experiments/configs/train-embeddings/${model}.yml"
  for ds in 'bitcoin-alpha' 'bitcoin-otc' 'fb-forum' 'fb-messages' 'ia-enron-employees' 'ia-radoslaw-email' 'ia-hypertext'; do

    if [ "${model}" == "mf/lle" ]; then
       if [ "${ds}" == "bitcoin-alpha" ] || [ "${ds}" == "bitcoin-otc" ] || [ "${ds}" == "fb-messages" ]; then
         continue
       fi
    fi

    if [ "${ds}" == "ppi" ]; then
      if [ "${model}" == "mf/hope" ] || [ "${model}" == "mf/sge" ] || [ "${model}" == "mf/lle" ] || [ "${model}" == "rw/ctdne" ] ; then
         continue
       fi
    fi
    for snapshot in 'G_0_2' 'G_0_3'  'G_0_4' 'G_0_5' 'G_0_6' 'G_0_7' 'G_0_8' 'G_0_9'; do
      for run in 0 1 2 3 4; do
        PYTHONPATH=. python3 scripts/memory-measurement-generalised-fildne.py --ds "${ds}" --config "${CONFIG}" --emb-config "${BASE_CONFIG}" --snapshot "${snapshot}" --run "${run}"
      done
    done
  done
done
