"""Code for DGI node feature extractor class."""
import os

import networkx as nx
import numpy as np
import scipy.sparse as sp
import torch
import torch.nn as nn
from tqdm import tqdm

from .models import DGI
from .utils import process
from dgem.embedding.gcn import base


class DGIEmbedding(base.BaseGCN):
    """Class for DGI based node feature extraction."""

    def __init__(self, dimensions, batch_size=1, nb_epochs=50, patience=50,
                 lr=0.001, l2_coef=0.0, drop_prob=0.0, sparse=True,
                 nonlinearity='prelu'):
        """Initialise DGI model."""
        super().__init__(dimensions)
        self.batch_size = batch_size
        self.nb_epochs = nb_epochs
        self.patience = patience
        self.lr = lr
        self.l2_coef = l2_coef
        self.drop_prob = drop_prob
        self.sparse = sparse
        self.nonlinearity = nonlinearity

    def _embed(self, graph):
        """Make node embedding."""
        adj = nx.adjacency_matrix(graph, weight=False)

        features = np.eye(graph.number_of_nodes())
        features = process.preprocess_features(features, dense=False)

        nb_nodes = features.shape[0]
        ft_size = features.shape[1]
        adj = process.normalize_adj(adj + sp.eye(adj.shape[0]))

        if self.sparse:
            sp_adj = process.sparse_mx_to_torch_sparse_tensor(adj)
        else:
            adj = (adj + sp.eye(adj.shape[0])).todense()

        features = torch.FloatTensor(features[np.newaxis])
        if not self.sparse:
            adj = torch.FloatTensor(adj[np.newaxis])

        model = DGI(ft_size, self.dimensions, self.nonlinearity)
        optimiser = torch.optim.Adam(
            model.parameters(),
            lr=self.lr,
            weight_decay=self.l2_coef
        )

        if torch.cuda.is_available():
            print('Using CUDA')
            model.cuda()
            features = features.cuda()
            if self.sparse:
                sp_adj = sp_adj.cuda()
            else:
                adj = adj.cuda()

        b_xent = nn.BCEWithLogitsLoss()
        cnt_wait = 0
        best = 1e9
        os.makedirs('./tmp/', exist_ok=True)
        pbar = tqdm(range(self.nb_epochs), desc='DGI fit', leave=False)

        for epoch in pbar:
            model.train()
            optimiser.zero_grad()

            idx = np.random.permutation(nb_nodes)
            shuf_fts = features[:, idx, :]

            lbl_1 = torch.ones(self.batch_size, nb_nodes)
            lbl_2 = torch.zeros(self.batch_size, nb_nodes)
            lbl = torch.cat((lbl_1, lbl_2), 1)

            if torch.cuda.is_available():
                shuf_fts = shuf_fts.cuda()
                lbl = lbl.cuda()

            logits = model(features, shuf_fts, sp_adj if self.sparse else adj,
                           self.sparse, None, None, None)

            loss = b_xent(logits, lbl)

            if epoch == 0:
                torch.save(model.state_dict(), './tmp/best_dgi.pkl')

            if loss < best:
                best = loss
                cnt_wait = 0
                torch.save(model.state_dict(), './tmp/best_dgi.pkl')
            else:
                cnt_wait += 1

            if cnt_wait == self.patience:
                break

            loss.backward()
            optimiser.step()

        pbar.close()

        model.load_state_dict(torch.load('./tmp/best_dgi.pkl'))
        os.remove('./tmp/best_dgi.pkl')
        os.rmdir('./tmp')

        embeds, _ = model.embed(
            features, sp_adj if self.sparse else adj,
            self.sparse,
            None
        )
        if torch.cuda.is_available():
            out = embeds.cpu().numpy()[0]
        else:
            out = embeds.numpy()[0]

        return out
