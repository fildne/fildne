"""Code for base class for GCN embedding methods."""
import abc

import numpy as np

from dgem.embedding import base as km


class BaseGCN(abc.ABC):
    """Base class for GCN graph embedding."""

    def __init__(self, dimensions):
        """Inits BaseGCN."""
        self.dimensions = dimensions

    def embed(self, graph):
        """Creates features based on existing data features."""
        old_node_labels = sorted([int(node) for node in graph.nodes()])
        new_node_labels = np.arange(0, len(old_node_labels))

        rev_mapping = dict(zip(new_node_labels, old_node_labels))

        emb = self._embed(graph)
        emb = {str(rev_mapping[idx]): vec for idx, vec in enumerate(emb)}

        return km.KeyedModel(
            size=self.dimensions,
            node_emb_vectors=emb,
        )

    @abc.abstractmethod
    def _embed(self, graph):
        pass
