import networkx as nx
import numpy as np
import tensorflow as tf
from . import openne_graph as ne_graph
from . import openne_line as line

from dgem.embedding import base as km


class LINE:
    """LINE graph embedding."""

    def __init__(self, dimensions, negative_samples, order, epochs=50):
        """Inits LINE."""
        self.dimensions = dimensions
        self.negative_samples = negative_samples
        self.epochs = epochs
        self.order = order

    @classmethod
    def _preprocess(cls, graph):
        nx.set_edge_attributes(graph, 1, 'weight')
        openne_graph = ne_graph.Graph()
        openne_graph.read_g(graph)
        return openne_graph

    def _embed(self, graph):
        tf.reset_default_graph()
        graph = self._preprocess(graph)
        emb = line.LINE(
            graph=graph,
            rep_size=self.dimensions,
            epoch=self.epochs,
            negative_ratio=self.negative_samples,
            order=self.order,
            batch_size=500
        )
        emb.get_embeddings()

        return emb.vectors

    def embed(self, graph):
        """Creates features based on existing data features."""
        old_node_labels = sorted([int(node) for node in graph.nodes()])
        new_node_labels = np.arange(0, len(old_node_labels))

        rev_mapping = dict(zip(new_node_labels, old_node_labels))

        graph = nx.relabel_nodes(
            graph, dict(zip(old_node_labels, new_node_labels))
        )
        emb = self._embed(graph)

        emb = {
            str(rev_mapping[idx]): vec
            for idx, vec in emb.items()
        }

        return km.KeyedModel(
            size=self.dimensions,
            node_emb_vectors=emb,
        )
