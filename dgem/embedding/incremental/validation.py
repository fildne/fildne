"""Validation embedding for training and evaluation of FILDNE."""
import warnings

import networkx as nx  # noqa: F401
import numpy as np
from sklearn import exceptions as sk_exc

from dgem.tasks.link_prediction import dataset as lp_ds
from dgem.tasks import models as models

# Do not show scikit warnings
warnings.filterwarnings("ignore", category=sk_exc.UndefinedMetricWarning)


def get_correct_edge_predictions(embeddings, snapshot):
    """Obtains class counts of correct edge prediction experiment.

    To obtain the class counts, link prediction experiments are executed on
    given embeddings using dataset generated from given graph snapshot.

    :param embeddings: List of all snapshot node embeddings
    :type embeddings: list
    :param snapshot: Graph snapshot to perform link prediction on
    :type snapshot: nx.Graph
    :return: List of class counts
    :rtype: list
    """
    # Create dataset for learning logistic regression
    dataset = lp_ds.mk_link_prediction_dataset(
        graph=snapshot,
        split_proportion=0.75
    )

    x_train, y_train = dataset[0], dataset[1]
    x_test, y_test = dataset[2], dataset[3]

    # Extract true edges
    true_edges = []

    for x, y in zip(x_train, y_train):
        if y[0] == 1:
            true_edges.append(x)

    for x, y in zip(x_test, y_test):
        if y[0] == 1:
            true_edges.append(x)

    true_edges = np.array(true_edges)

    # Prepare validation embedding
    vms = [models.LogisticRegressionModel(emb) for emb in embeddings]

    for vm in vms:
        vm.fit(x_train, y_train)

    # Count correct predictions
    correct_predictions = [0] * len(embeddings)

    preds = [vm.predict(true_edges) for vm in vms]
    # Zip together outcomes from different embedding for one given edge
    preds = list(zip(*preds))

    for pred in preds:
        # More than one embedding "predicted" the edge correctly
        if sum(list(pred)) > 1:
            idx = np.random.choice([i for i, p in enumerate(pred) if p == 1])
            correct_predictions[idx] += 1
        # Only one embedding "predicted" the edge correctly
        elif sum(list(pred)) == 1:
            idx = pred.index(1)
            correct_predictions[idx] += 1

    return correct_predictions
