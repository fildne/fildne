"""Module for embedding calibration."""
import numpy as np
from timeit import default_timer as timer
from typing import List, Dict, Tuple, Union
from scipy.linalg import orthogonal_procrustes

from dgem.embedding.base import KeyedModel
from dgem.tools.graph import scoring


class OrthogonalProcrustesRegressor:
    """Creates regressor that solves the orthogonal Procrustes problem."""

    def __init__(self):
        """Makes placeholder transformation matrix."""
        self._r = None

    def fit(self, a, b):
        """Finds transformation matrix."""
        self._r = orthogonal_procrustes(a, b)[0]

    def predict(self, a):
        """Applies transformation matrix."""
        return np.matmul(a, self._r)


def get_activity_dict(graph):
    """Returns nodes activity dict of the given graph.

    :param graph: Input graph
    :type graph: nx.Graph
    :return: Degree dict
    :rtype: dict
    """
    degree_dict = dict()
    for node in graph.nodes():
        degree_dict[node] = len(graph.edges(node))
    return degree_dict


def get_reference_node_count(name, max_n_nodes, cfg, emb_dim, scores):
    """Returns number of reference nodes."""
    if name == 'percent':
        return int(max_n_nodes * cfg['percent'])
    elif name == 'firstN':
        return min(
            int(emb_dim * cfg['multiplier']),
            int(max_n_nodes * cfg['max-percent'])
        )
    elif name == 'threshold':
        return sum([score <= cfg['threshold'] for score in scores.values()])
    else:
        raise Exception(
            f'{name} reference node selection scheme not implemented!'
        )


def calibrate(embeddings: List[KeyedModel],
              activity: List[Dict[Union[int, str], float]],
              ref_nodes_config, num_workers
              ) -> Tuple[List[KeyedModel], List[float]]:
    """Calibrates embedded vectors to fit to a reference embedding space."""
    assert len(embeddings) == len(activity)
    if not ref_nodes_config['name']:
        return embeddings, [0 for _ in embeddings]
    regression_model = OrthogonalProcrustesRegressor()
    calibrated_embeddings = embeddings[:1]
    calibrated_times = []
    for idx, embedding in enumerate(embeddings[1:], 1):
        cl_st_time = timer()
        ref_nodes_scores = (
            scoring.get_nodes_scores_for_current_snapshot(
                prev_snapshot_activity=activity[idx - 1],
                snapshot_activity=activity[idx],
                scoring_fn=scoring.basic_scoring_function
            )
        )
        ref_nodes_count = get_reference_node_count(
            ref_nodes_config['name'],
            len(ref_nodes_scores),
            ref_nodes_config,
            embedding.emb_dim,
            ref_nodes_scores,
        )
        reference_nodes = scoring.get_reference_nodes(
            scores_dict=ref_nodes_scores,
            nb_ref_nodes=ref_nodes_count,
        )
        calibrated_embeddings.append(
            _calibrate(
                embedding,
                calibrated_embeddings[idx - 1],
                reference_nodes,
                regression_model
            )
        )
        cl_en_time = timer()
        calibrated_times.append(cl_en_time - cl_st_time)

    return calibrated_embeddings, calibrated_times


def _calibrate(
        embedding,
        reference_embedding,
        reference_nodes,
        regression_model
):
    """Calibrates embedded vectors to fit to a reference embedding space.

    :param embedding: Input embedding to calibrate
    :type embedding: KeyedModel
    :param reference_embedding: Reference embedding that embedding will
                                be calibrated to
    :type reference_embedding: KeyedModel
    :param reference_nodes: List of reference nodes
    :type reference_nodes: list
    :param regression_model: Regression models used to calibrate
                             embedding space
    :return: Calibrated Word2Vec based embedding model
    :rtype: KeyedModel
    """
    nodes = embedding.nodes
    emb_array = embedding.to_numpy(nodes)
    fit_emb_array = embedding.to_numpy(reference_nodes)
    ref_emb_array = reference_embedding.to_numpy(reference_nodes)

    regression_model.fit(fit_emb_array, ref_emb_array)
    calibrated_emb_array = regression_model.predict(emb_array)

    return KeyedModel(
        size=calibrated_emb_array.shape[1],
        node_emb_vectors=dict(zip(nodes, calibrated_emb_array))
    )
