"""Module for implementations of FILDNE models."""
from collections import defaultdict

import networkx as nx  # noqa: F401
import numpy as np

from dgem.embedding.base import KeyedModel
from . import estimators as es
from . import validation as val


class AbstractFILDNE:
    """Base class for both FILDNE models."""

    def __init__(self, window_size, parameters):
        """Inits AbstractFILDNE.

        :param window_size: Number of graph snapshot embedding to combine
        :type window_size: int
        :param parameters: List of FILDNE parameters
        :type parameters: list
        """
        self._window_size = window_size
        self._parameters = parameters

    def predict(self, input_embeddings):
        """Combines multiple snapshot node embeddings.

        Based on node embeddings of consecutive graph snapshots, this method
        calculates a combined node embedding.

        :param input_embeddings: List of snapshots node embeddings
        :type input_embeddings: list
        :return: Predicted node embedding
        :rtype: KeyedModel
        """
        assert len(input_embeddings) == self._window_size

        # Gather node vectors
        node_vectors = defaultdict(lambda: [None] * len(input_embeddings))
        for idx, emb in enumerate(input_embeddings):
            for node in emb.nodes:
                node_vectors[node][idx] = emb.get_vector(node)

        # Construct new embedding
        emb_predicted = KeyedModel(size=input_embeddings[0].emb_dim)

        for node, vectors in node_vectors.items():
            emb_predicted.add_node(node, self._combine_vectors(vectors))

        return emb_predicted

    def _combine_vectors(self, vectors):
        """Combines multiple embedding vectors for a single node.

        :param vectors: List of input vectors
        :type vectors: list
        :return: Combined vector
        :rtype: list
        """
        pass

    @property
    def parameters(self):
        """Returns FILDNE model parameters."""
        return self._parameters


class BasicFILDNE(AbstractFILDNE):
    """Implementation of the Basic FILDNE model."""

    def __init__(self, window_size, alpha):
        """Inits BasicFILDNE.

        :param window_size: Number of graph snapshot embedding to combine
        :type window_size: int
        :param alpha: Alpha combination parameter
        :type alpha: float
        """
        parameters = [alpha, 1 - alpha]
        super(BasicFILDNE, self).__init__(window_size, parameters)

    def _combine_vectors(self, vectors):
        """Combines multiple embedding vectors for a single node.

        :param vectors: List of input vectors
        :type vectors: list
        :return: Combined vector
        :rtype: list
        """
        vectors = [vec for vec in vectors if vec is not None]

        if len(vectors) == 1:
            return list(vectors[0])

        combined_vector = vectors[0]
        alpha = self._parameters

        for vec in vectors[1:]:
            combined_vector = alpha[0] * combined_vector + alpha[1] * vec

        return list(combined_vector)


class GeneralisedFILDNE(AbstractFILDNE):
    """Implementation of the Generalised FILDNE model."""

    def __init__(self, window_size):
        """Inits GeneralisedFILDNE.

        :param window_size: Number of graph snapshot embedding to combine
        :type window_size: int
        """
        parameters = list(np.zeros(window_size))
        super(GeneralisedFILDNE, self).__init__(window_size, parameters)

    def fit(self, embeddings, last_snapshot, prior_distribution):
        """Estimates the FILDNE model parameters.

        The estimation of the FILDNE model parameters uses link prediction
        experiment on consecutive snapshot node embeddings using the most
        recent graph snapshot. The results are fed into a Dirichlet-Multinomial
        model with a prior knowledge generated from a given prior distribution.

        :param embeddings: List of snapshot node embeddings
        :type embeddings: list
        :param last_snapshot: Most recent graph snapshot
        :type last_snapshot: nx.Graph
        :param prior_distribution: Name of prior distribution to use
        :type prior_distribution: str
        :return: FILDNE model
        """
        corr_pred = val.get_correct_edge_predictions(embeddings, last_snapshot)

        if prior_distribution == 'uniform':
            alpha = [1.0] * self._window_size
        elif prior_distribution == 'increase':
            alpha = list(range(1, self._window_size + 1))
        else:
            raise RuntimeError('Unknown prior strategy:', prior_distribution)

        # Get parameters using Dirichlet
        dirichlet = es.DirichletMultinomialModel(
            alpha=alpha,
            N=corr_pred,
            dim=self._window_size,
        )

        self._parameters = dirichlet.map()

        return self

    def _combine_vectors(self, vectors):
        """Combines multiple embedding vectors for a single node.

        :param vectors: List of input vectors
        :type vectors: list
        :return: Combined vector
        :rtype: list
        """
        pv = [(p, v) for p, v in zip(self._parameters, vectors)
              if v is not None]

        if len(pv) == 0:
            raise RuntimeError("combine_vectors() called with no vectors!")

        parameters, vectors = zip(*pv)

        if len(vectors) == 1:
            return list(vectors[0])

        parameters = norm(np.array(parameters))
        vectors = np.array(vectors)

        weighed_vectors = (vectors.T * parameters).T
        combined_vector = np.sum(weighed_vectors, axis=0)

        return list(combined_vector)


def norm(s):
    """Re-normalizes input array so the elements sum up to 1.

    :param s: Input array
    :type s: np.ndarray
    :return: Normalized array
    :rtype: np.ndarray
    """
    if not np.any(s):  # Contains only zeros
        return s
    return s / sum(s)
