"""Parameter estimators for FILDNE model."""
import numpy as np


class DirichletMultinomialModel:
    """Bayesian model for estimation of FILDNE model parameters."""

    def __init__(self, alpha, N, dim):
        """Inits DirichletMultinomialModel.

        :param alpha: Prior knowledge values
        :type alpha: list
        :param N: Likelihood values
        :type N: list
        :param dim: Dimensionality of the model
        :type dim: int
        """
        self._alpha = np.array(alpha)
        self._N = np.array(N)
        self._dim = dim

    def map(self):
        """Calculates the MAP (Maximum A Posteriori) estimate."""
        N = sum(self._N)
        alpha0 = sum(self._alpha)
        _map = (self._N + self._alpha - 1) / (N + alpha0 - self._dim)
        return _map

    def mle(self):
        """Calculates the MLE (Maximum Likelihood Estimate)."""
        _mle = self._N / sum(self._N)
        return _mle
