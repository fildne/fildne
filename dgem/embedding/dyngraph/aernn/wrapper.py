"""Wrapper for AERNN dyngraph model."""
import logging
import os
from collections import defaultdict

import keras
import networkx as nx
from dynamicgem.embedding.dynAERNN import DynAERNN

from dgem.embedding.base import KeyedModel

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


def convert_to_digraph(in_graph, nodes):
    """Converts graph to digraph."""
    edges = defaultdict(int)
    directed = True if in_graph.is_directed() else False

    for f, t in in_graph.edges():
        edges[(f, t)] += 1

    G = nx.DiGraph()
    G.add_nodes_from(nodes)

    for (f, t), w in edges.items():
        G.add_edge(f, t, weight=w)
        if not directed:
            G.add_edge(t, f, weight=w)

    return G


class DynGraphAERNN:
    """Base class for DynGraphAERNN graph embedding."""

    def __init__(self, dimensions, beta, n_iter, n_batch, xeta):
        """Inits BaseMFE."""
        self.d = dimensions
        self.beta = beta
        self.nu1 = 1e-6
        self.nu2 = 1e-6
        self.rho = 0.3
        self.xeta = xeta
        self.n_iter = n_iter
        self.n_batch = n_batch
        self.n_aeunits = [500, 300]
        self.n_lstmunits = [500, self.d]
        self.modelfile = None
        self.weightfile = None
        self.savefilesuffix = None

    @staticmethod
    def _preprocess(graphs, nodes):
        """Preprocesses data."""
        graphs = [convert_to_digraph(graph, nodes) for graph in graphs]
        return graphs

    @staticmethod
    def _postprocess(emb, nodes):
        """Postprocesses data."""
        processed_emb = {}

        for node, emb_vec in zip(nodes, emb):
            processed_emb[str(node)] = emb_vec

        return KeyedModel(
            size=emb[0].shape[0],
            node_emb_vectors=processed_emb
        )

    def embed(self, graphs, nodes):
        """Performs graph embedding."""
        graph = self._preprocess(graphs, nodes)
        if self.n_batch > len(nodes):
            self.n_batch = len(nodes)
        model = DynAERNN(
            n_prev_graphs=len(graphs) - 1,
            beta=self.beta,
            n_iter=self.n_iter,
            n_batch=self.n_batch,
            xeta=self.xeta,
            rho=self.rho,
            n_aeunits=self.n_aeunits,
            n_lstmunits=self.n_lstmunits,
            d=self.d,
            nu1=self.nu1,
            nu2=self.nu2,
            modelfile=self.modelfile,
            weightfile=self.weightfile,
            savefilesuffix=self.savefilesuffix
        )

        emb, _ = model.learn_embeddings(graph)
        emb = self._postprocess(
            emb, nodes
        )
        keras.backend.clear_session()
        return emb
