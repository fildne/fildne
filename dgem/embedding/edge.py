"""Module for handling edge embeddings operations."""
import numpy as np

from .base import KeyedModel  # noqa: F401


def hadamard_op(u, v):
    """Calculates hadamard edge representation between two node vectors.

    :param u: Input array
    :type u: np.ndarray
    :param v: Input array
    :type v: np.ndarray
    :return: The hadamard edge representation
    :rtype: np.ndarray
    """
    return u * v


def average_op(u, v):
    """Calculates average edge representation between two node vectors.

    :param u: Input array
    :type u: np.ndarray
    :param v: Input array
    :type v: np.ndarray
    :return: The average edge representation
    :rtype: np.ndarray
    """
    return (u + v) / 2


def weighted_l1_op(u, v):
    """Calculates l1 edge representation between two node vectors.

    :param u: Input array
    :type u: np.ndarray
    :param v: Input array
    :type v: np.ndarray
    :return: The weighted-l1 edge representation
    :rtype: np.ndarray
    """
    return np.abs(u - v)


def weighted_l2_op(u, v):
    """Calculates l2 edge representation between two node vectors.

    :param u: Input array
    :type u: np.ndarray
    :param v: Input array
    :type v: np.ndarray
    :return: The weighted-l2 edge representation
    :rtype: np.ndarray
    """
    return np.abs(u - v) ** 2


def calculate_edge_embedding(keyed_model, edges, op):
    """Calculates edge embedding.

    :param keyed_model: Input KeyedModel
    :type keyed_model: KeyedModel
    :param edges: List of edges
    :type edges: list
    :param op: Edge representation operation
    :return: List of embedded edges
    :rtype: list
    """
    representations = []

    for u, v in edges:
        e_u = keyed_model.get_vector(str(u))
        e_v = keyed_model.get_vector(str(v))

        representations.append(op(e_u, e_v))

    return representations
