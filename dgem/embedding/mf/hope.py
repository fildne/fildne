"""Code for HOPEEmbedding."""
from gem.embedding import hope

from dgem.embedding.mf import base


class HOPEEmbedding(base.BaseMFE):
    """Wrapper for GEM's HOPE algorithm."""

    def __init__(self, dimensions, beta, **kwargs):
        """Inits HOPEEmbedding."""
        super().__init__(dimensions)
        self.model = hope.HOPE(
            beta=beta,
            d=self.dimensions,
            **kwargs
        )

    def _embed(self, graph):
        y, _ = self.model.learn_embedding(
            graph=graph,
            is_weighted=False
        )
        return y
