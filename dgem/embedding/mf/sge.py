"""Code for SpectralGraphEmbedding."""
import networkx as nx
import numpy as np
from sklearn import manifold as sk_md
from dgem.embedding import base as km

from .base import BaseMFE


class SpectralGraphEmbedding(BaseMFE):
    """Wrapper for Sklearn's SpectralEmbedding."""

    def __init__(self, dimensions, **kwargs):
        """Inits SpectralGraphEmbedding."""
        super().__init__(dimensions)
        self._model = sk_md.SpectralEmbedding(
            n_components=self.dimensions,
            affinity='precomputed',
            **kwargs
        )

    def embed(self, graph):
        """Embeds Graph."""
        graph = nx.Graph(graph)
        graph = max(nx.connected_component_subgraphs(graph), key=len)

        old_node_labels = sorted([int(node) for node in graph.nodes()])
        new_node_labels = np.arange(0, len(old_node_labels))

        rev_mapping = dict(zip(new_node_labels, old_node_labels))

        emb = self._embed(graph)
        emb = {str(rev_mapping[idx]): vec for idx, vec in enumerate(emb)}

        return km.KeyedModel(
            size=self.dimensions,
            node_emb_vectors=emb,
        )

    def _embed(self, graph):
        adj = nx.adjacency_matrix(graph, weight=None).toarray()
        return self._model.fit_transform(adj)
