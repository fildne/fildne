"""Code for LocallyLinearGraphEmbedding."""
import networkx as nx
import numpy as np
from gem.embedding import lle
from dgem.embedding import base as km
from dgem.embedding.mf import base


class LocallyLinearGraphEmbedding(base.BaseMFE):
    """Wrapper for Sklearn's LocallyLinearGraphEmbedding."""

    def __init__(self, dimensions, is_weighted):
        """Inits LocallyLinearGraphEmbedding."""
        super().__init__(dimensions)
        self.model = lle.LocallyLinearEmbedding(
            d=self.dimensions,
        )
        self.is_weighted = is_weighted

    def embed(self, graph):
        """Performs graph embedding."""
        graph = nx.Graph(graph)

        old_node_labels = sorted([int(node) for node in graph.nodes()])
        new_node_labels = np.arange(0, len(old_node_labels))
        rev_mapping = dict(zip(new_node_labels, old_node_labels))

        graph = nx.from_scipy_sparse_matrix(
            nx.adjacency_matrix(graph, weight=None)
        )
        graph = max(nx.connected_component_subgraphs(graph), key=len)

        emb = self._embed(graph)
        emb = {str(rev_mapping[idx]): vec for idx, vec in enumerate(emb)}

        return km.KeyedModel(
            size=self.dimensions,
            node_emb_vectors=emb,
        )

    def _embed(self, graph):
        y, _ = self.model.learn_embedding(
            graph=graph,
            is_weighted=self.is_weighted
        )

        return y
