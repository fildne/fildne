"""Code for base class of all matrix factorization based methods."""
import abc

import numpy as np

from dgem.embedding import base as km


class BaseMFE:
    """Base class for Matrix Factorization graph embedding."""

    def __init__(self, dimensions):
        """Inits BaseMFE."""
        self.dimensions = dimensions

    def embed(self, graph):
        """Performs graph embedding."""
        old_node_labels = sorted([int(node) for node in graph.nodes()])
        new_node_labels = np.arange(0, len(old_node_labels))

        rev_mapping = dict(zip(new_node_labels, old_node_labels))

        emb = self._embed(graph)
        emb = {str(rev_mapping[idx]): vec for idx, vec in enumerate(emb)}

        return km.KeyedModel(
            size=self.dimensions,
            node_emb_vectors=emb,
        )

    @abc.abstractmethod
    def _embed(self, graph):
        pass
