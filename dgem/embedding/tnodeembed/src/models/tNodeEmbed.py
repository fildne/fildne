import networkx as nx
import numpy as np
from scipy.linalg import orthogonal_procrustes

from keras.layers import Input, LSTM, Dense, Activation, Concatenate, Lambda
from keras.models import Model
from keras.optimizers import Adam

from dgem.embedding.tnodeembed.src.models.task_model import TaskModel
from dgem.embedding.tnodeembed.src.utils.graph_utils import get_graph_times


class tNodeEmbed(TaskModel):
    def __init__(self, graph_nx, task):
        '''
        tNodeEmbed init
        Args:
            graph_nx: networkx - holding the temporal graph
            task: str - name of the task. either 'temporal_link_prediction' or 'node_classification'
        '''
        super(tNodeEmbed, self).__init__(task=task)

        self.graph_nx = graph_nx
        times = get_graph_times(self.graph_nx)
        self.train_time_steps = times

    @staticmethod
    def _get_model(task, input_shape, learning_rate, latent_dim=128, num_classes=1):
        '''
        Given the task, return the desired architecture of training
        Args:
            task: string - of the tasks name, either 'temporal_link_prediction' or 'node_classification'
            input_shape: tuple - shape of a singe sample
            latent_dim: int - the size of the LSTM latent space
            num_classes: int - number of classes. Relevant only if task=='node_classification'
        Returns:
            keras model of tNodeEmbed
        '''
        if task == 'TLP':
            inputs = Input(shape=input_shape)

            lmda_lyr1 = Lambda(lambda x: x[:, 0, :, :], output_shape=input_shape[1:])(inputs)
            lmda_lyr2 = Lambda(lambda x: x[:, 1, :, :], output_shape=input_shape[1:])(inputs)

            lstm_lyr = LSTM(latent_dim, return_sequences=False, activation='relu')
            lstm_lyr1 = lstm_lyr(lmda_lyr1)
            lstm_lyr2 = lstm_lyr(lmda_lyr2)
            concat_lyr = Concatenate(axis=-1)([lstm_lyr1, lstm_lyr2])

            fc_lyr1 = Dense(latent_dim, activation='relu')(concat_lyr)
            fc_lyr2 = Dense(1)(fc_lyr1)
            soft_lyr = Activation('sigmoid')(fc_lyr2)

            model = Model(inputs, soft_lyr)
            optimizer = Adam(lr=learning_rate)
            model.compile(optimizer=optimizer, loss='binary_crossentropy', metrics=['accuracy'])
        elif task == 'NC':
            inputs = Input(shape=input_shape)

            lstm_lyr = LSTM(latent_dim)(inputs)

            fc_lyr = Dense(num_classes)(lstm_lyr)
            soft_lyr = Activation('softmax')(fc_lyr)

            model = Model(inputs, soft_lyr)
            model.compile(optimizer='adam', loss='categorical_crossentropy', metrics=['accuracy'])
        else:
            print(task)
            raise Exception('unknown task for _get_model')
        return model

    def do_align(self):
        '''
        Given a graph that went through 'initialize_embeddings', align all time step embeddings to one another
        Args:
            graph_nx: networkx - the given graph
            times: list - of the times we want to work with, if None then calculates for all times in the graph

        Returns:
            graph_nx: networkx - with aligned embeddings in its attributes for each node
        '''
        times = self.train_time_steps
        graph_nx = self.graph_nx

        node2Q_t_1 = nx.get_node_attributes(graph_nx, times[0])
        Q_t_1 = np.array([node2Q_t_1[node] for node in node2Q_t_1])
        for time in times[1:]:
            node2Q_t = nx.get_node_attributes(graph_nx, time)
            Q_t = np.array([node2Q_t[node] for node in node2Q_t_1])
            R_t, _ = orthogonal_procrustes(Q_t, Q_t_1)
            Q_t = np.array([node2Q_t[node] for node in node2Q_t])
            R_tQ_t = np.dot(Q_t, R_t)
            node2R_tQ_t = {node: vec for node, vec in zip(node2Q_t, R_tQ_t)}
            nx.set_node_attributes(graph_nx, node2R_tQ_t, time)
            node2Q_t_1 = node2R_tQ_t
            Q_t_1 = R_tQ_t

        return graph_nx
