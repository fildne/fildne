import logging
import os
from copy import deepcopy as copy
from math import ceil
from timeit import default_timer as timer

import keras
from networkx import nx

from dgem.embedding.tnodeembed import tNodeEmbed
from dgem.embedding.tnodeembed.src.loader import dataset_generator
from dgem.tools.metrics import ClassificationReport

logging.disable(logging.WARNING)
os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


def convert_calculated_n2v_embeddings_to_tne_format(embeddings, input_graph):
    """Converts calculated n2v embeddings to tne format."""
    graph = copy(input_graph)
    for emb_id, emb in enumerate(embeddings):
        vectors = {
            int(k): emb
            for k, emb in emb['vectors'].items()
        }
        nx.set_node_attributes(graph, name=emb_id + 1, values=vectors)
    return graph


def flatten_timestamps_in_the_graph(snapshots, directed):
    """Flattens timestamps within given graph based on snapshots."""
    edgelist = []
    for snap_id, snap in enumerate(snapshots):
        nx.set_edge_attributes(snap, snap_id + 1, 'timestamp')
        edgelist.extend(list(snap.edges(data=True)))

    create_using = nx.MultiGraph
    if directed:
        create_using = nx.MultiDiGraph

    return nx.from_edgelist(edgelist, create_using=create_using)


class TNEEmbedding:
    """Wrapper for TNE method."""

    def __init__(self, args, directed=False, verbose=0):
        args = copy(args)
        self.lr = args.pop('learning_rate')
        self.do_align = args.pop('do_align')
        self.model_args = {
            'verbose': verbose,
            **args,
        }
        self.directed = directed

    def eval(self, snapshots, embeddings, batch_size, lp_dataset, task='TLP'):
        """Evaluates embedding."""
        graph = flatten_timestamps_in_the_graph(
            snapshots, directed=self.directed
        )
        embedded_graph = convert_calculated_n2v_embeddings_to_tne_format(
            embeddings=embeddings,
            input_graph=graph
        )
        model = tNodeEmbed(
            embedded_graph,
            task=task,
        )
        st_time = timer()

        if self.do_align:
            model.do_align()

        steps_per_epoch = ceil(len(lp_dataset['x_train']) / batch_size)
        generator = dataset_generator.dataset_generator(
            lp_dataset['x_train'],
            lp_dataset['y_train'],
            model.graph_nx,
            model.train_time_steps,
            batch_size=batch_size
        )
        model.fit_generator(
            generator, steps_per_epoch, self.lr, **self.model_args
        )
        end_time = timer()
        steps = ceil(len(lp_dataset['x_test']) / batch_size)
        generator = dataset_generator.dataset_generator(
            lp_dataset['x_test'],
            lp_dataset['y_test'],
            model.graph_nx,
            model.train_time_steps,
            batch_size=batch_size,
            shuffle=False
        )
        y_score = model.predict_generator(generator, steps)

        keras.backend.clear_session()
        cr = ClassificationReport(
            y_true=lp_dataset['y_test'],
            y_pred=y_score > 0.5,
            y_score=y_score
        )
        time = end_time - st_time

        return cr, time
