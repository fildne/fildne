import numpy as np
import pandas as pd
import networkx as nx

def get_graph_T(graph_nx, min_time=-np.inf, max_time=np.inf, return_df=False):
    '''
    Given a graph with a time attribute for each edge, return the subgraph with only edges between an interval.
    Args:
        graph_nx: networkx - the given graph
        min_time: int - the minimum time step that is wanted. Default value -np.inf
        max_time: int - the maximum time step that is wanted. Default value np.inf
        return_df: bool - if True, return a DataFrame of the edges and attributes,
                          else, a networkx object

    Returns:
        sub_graph_nx: networkx - subgraph with only edges between min_time and max_time
    '''
    relevant_edges = []
    attr_keys = []

    if len(graph_nx.nodes()) == 0:
        return graph_nx

    for u, v, attr in graph_nx.edges(data=True):
        if 'time' in attr.keys():
            edge_ts = attr['time']
        else:
            edge_ts = attr['timestamp']

        if min_time < edge_ts <= max_time:
            relevant_edges.append((u, v, *attr.values()))

            if attr_keys != [] and attr_keys != attr.keys():
                raise Exception('attribute keys in \'get_graph_T\' are different')
            attr_keys = attr.keys()

    graph_df = pd.DataFrame(relevant_edges, columns=['from', 'to', *attr_keys])

    if return_df:
        node2label = nx.get_node_attributes(graph_nx, 'label')
        if len(node2label) > 0:
            graph_df['from_class'] = graph_df['from'].map(lambda node: node2label[node])
            graph_df['to_class'] = graph_df['to'].map(lambda node: node2label[node])
        return graph_df
    else:
        sub_graph_nx = nx.from_pandas_edgelist(graph_df, 'from', 'to', list(attr_keys), create_using=type(graph_nx)())

        # add node attributes
        for node, attr in graph_nx.nodes(data=True):
            if node not in sub_graph_nx:
                continue
            sub_graph_nx.nodes[node].update(attr)

        return sub_graph_nx


def get_graph_times(graph_nx):
    '''
    Return all times in the graph edges attributes
    Args:
        graph_nx: networkx - the given graph

    Returns:
        list - ordered list of all times in the graph
    '''
    timestamps = list(nx.get_edge_attributes(graph_nx, 'timestamp').values())
    if len(timestamps) == 0:
        timestamps = list(
            nx.get_edge_attributes(graph_nx, 'time').values()
        )
    return np.sort(np.unique(timestamps))


def nodes2embeddings(X, graph_nx, train_time_steps, dimensions, node2embedding=None):
    '''
    Given a np.array from any dimension where the final entry is a nodes name, change the nodes name into the
    nodes embeddings.
    Args:
        X: np.array - from any dimension holding node names
        graph_nx: networkx - the given graph
        train_time_steps: list - of time steps we want their embeddings
        dimensions: int - if embedding doesnt exist, in what size of zero-array to pad with
        node2embedding: dict - from a singe node to it's embedding. If None then it calculated during run time.

    Returns:
        np.array holding insted of node names - their embeddings
    '''
    if isinstance(X, dict):
        for k in X:
            X[k] = nodes2embeddings(X[k], graph_nx, train_time_steps, dimensions)
        return X
    elif not isinstance(X, np.ndarray):
        if node2embedding is not None:
            return node2embedding[X]
        embeddings = []
        for train_time_step in train_time_steps:
            if X in graph_nx.nodes():
                embedding = graph_nx.node[X][train_time_step] if train_time_step in graph_nx.node[X] else np.zeros(dimensions)
            else:
                embedding = np.zeros(dimensions)
            embeddings.append(embedding)
        return np.array(embeddings)
    else:
        embeddings = []
        for x in X:
            embedding = nodes2embeddings(x, graph_nx, train_time_steps, dimensions)
            embeddings.append(embedding)
        return np.array(embeddings)
