"""Wrapper for Node2Vec method.

Based on https://github.com/eliorc/node2vec
"""
import networkx as nx
from node2vec import Node2Vec

from dgem.embedding.random_walk import base


def multigraph2graph(multi_graph_nx):
    """Borrowed from TNE repository."""
    if type(multi_graph_nx) == nx.Graph or type(multi_graph_nx) == nx.DiGraph:
        return multi_graph_nx
    graph_nx = nx.DiGraph() if multi_graph_nx.is_directed() else nx.Graph()

    if len(multi_graph_nx.nodes()) == 0:
        return graph_nx

    # add edges + attributes
    for u, v, data in multi_graph_nx.edges(data=True):
        data['weight'] = 1.0

        if graph_nx.has_edge(u, v):
            graph_nx[u][v]['weight'] += data['weight']
        else:
            graph_nx.add_edge(u, v, **data)

    # add node attributes
    for node, attr in multi_graph_nx.nodes(data=True):
        if node not in graph_nx:
            continue
        graph_nx.nodes[node].update(attr)

    return graph_nx


class Node2VecEmbedding(base.BaseRW):
    """Class that wraps Node2Vec method."""

    def __init__(self, dimensions, w2v_args, walk_length, nb_walks_per_node,
                 p, q, workers):
        """Inits the Node2VecEmbedding class.

        :param dimensions: Embedding dimension
        :type dimensions: int
        :param w2v_args: Word2vec arguments
        :type w2v_args: dict
        :param walk_length: Length of single random walk sequence
        :type walk_length: int
        :param nb_walks_per_node: Number of random walk sequences starting
                                  from each node
        :type nb_walks_per_node: int
        :param p: Parameter p. Controls probability of revisiting the node
                               in a walk
        :type p: float
        :param q: Parameter q. Controls probability of visiting nodes that
                               are further away from previous node
        :type q: float
        :param workers: Number of workers
        :type workers: int
        """
        super(Node2VecEmbedding, self).__init__(dimensions, w2v_args)
        self._args = dict(
            walk_length=walk_length,
            num_walks=nb_walks_per_node,
            p=p,
            q=q,
            workers=workers,
        )

    def _walk(self, graph):
        """Generates random walks over the given graph."""
        graph = multigraph2graph(graph)
        walker = Node2Vec(graph=graph, **self._args)
        return walker.walks
