"""Module for providing basic random walk embedding functionality."""
import gensim

from dgem.embedding import base as eb


class Word2VecModel:
    """Word2Vec model."""

    def __init__(self, **kwargs):
        """Inits Word2vec model with parameters.

        :param kwargs: A dictionary with parameters of Word2Vec model
        """
        self._epochs = kwargs.pop('epochs')
        self._model = gensim.models.Word2Vec(**kwargs)

    def train(self, rw_seqs):
        """Train the Word2Vec model.

        Inits model vocabulary and trains model weights based on given random
        walk sequences.

        :param rw_seqs: Random walk sequences
        :param epochs: Number of training epochs
        :return A Keyed Model with embedded vectors.
        :rtype KeyedModel
        """
        self._model.build_vocab(rw_seqs)
        self._model.train(
            rw_seqs,
            total_examples=len(rw_seqs),
            epochs=self._epochs
        )
        return eb.KeyedModel.from_gensim_w2v_format(
            self._model.wv, fill_unknown_nodes=True
        )
