"""Implementation of CTDNE embedding method."""
import multiprocessing as mp

import networkx as nx
import numpy as np
from scipy.special.cython_special import expit
from tqdm import tqdm

from dgem.datatypes import TemporalGraphKeyedModel
from dgem.embedding.random_walk import base
from dgem.tools.graph import nxtools
from dgem.tools import utils


def convert_edge_walk_to_sequence(walk):
    """Converts edge walk to a node sequence.

    :param walk: Input walk
    :return: Random walk sequence
    :rtype: list
    """
    sequence = [str(walk[0][0])] + [str(edge[1]) for edge in walk]
    return sequence


class _CTDNEWalkStrategy:
    """Implements temporal random walking strategy.

    Derived from http://ryanrossi.com/pubs/nguyen-et-al-WWW18-BigNet.pdf paper.
    """

    def __init__(self, temporal_graph, init_edge_sampling_fn, edge_sampling_fn,
                 nb_walks_per_node, max_length, init_edge_kwargs=None,
                 sample_edge_kwargs=None, max_attempts=40):
        """Inits _CTDNEWalkStrategy the class.

        :param temporal_graph: Input graph
        :type temporal_graph: nx.MultiGraph
        :param init_edge_sampling_fn: Sampling function for starting walk edge
        :param edge_sampling_fn: General sampling function for choosing
                                 next edge
        :param nb_walks_per_node: Number of random walk sequences starting
                                  from each node
        :type nb_walks_per_node: int
        :param max_length: Length of single random walk sequence
        :type max_length: int
        :param init_edge_kwargs: Arguments for init_edge_sampling_fn
        :param sample_edge_kwargs: Arguments for edge_sampling_fn
        :param max_attempts: Maximum number of attempts to obtain temporal
                             random walk sequence
        :type max_attempts: int
        """
        self.max_attempts = max_attempts
        self.init_edge_sampling_fn = init_edge_sampling_fn
        self.edge_sampling_fn = edge_sampling_fn
        self.nb_walks_per_node = nb_walks_per_node
        self.max_length = max_length

        self.init_edge_kwargs = init_edge_kwargs or {}
        self.sample_edge_kwargs = sample_edge_kwargs or {}

        if not isinstance(temporal_graph, (nx.MultiGraph, nx.MultiDiGraph)):
            raise NotImplementedError('Graph is neither nx.MultiGraph '
                                      'nor nx.MultiDiGraph model!')

        self.temporal_graph = TemporalGraphKeyedModel.from_nx_graph(
            graph=temporal_graph,
        )
        self.edges = nxtools.get_edges(temporal_graph)

    def _sample_edge(self, node=None, ts=0, **kwargs):
        """Samples next edge in temporal random walk.

        :param node: Last node in the walk
        :type node: str
        :param ts: Input timestamp
        :param kwargs: Sampling function keyword arguments
        :return: Next edge of a given temporal random walk sequence
        """
        if not node and not ts:
            edges = self.edges
        else:
            edges = self.temporal_graph.get_temporal_edges(node, ts)

        if not edges:
            return None

        kwargs['min_time'] = ts
        return self.edge_sampling_fn(edges, **kwargs)

    def _precompute_exponential_probs(self):
        """Precomputes exponential probabilities."""
        t_min = np.min([edge[2]['timestamp'] for edge in self.edges])

        exps = np.array([expit(e[2]['timestamp'] - t_min)
                         for e in self.edges])
        probabilities = exps / np.sum(exps)
        return probabilities

    def get_walk(self, initial_edge_sampling_prob=None, start_node=None):
        """Returns temporal random walk sequence.

        :param initial_edge_sampling_prob: Precomputed initial edge sampling
                                           probabilities
        :param start_node: Random walk sequence starting node
        :type start_node: str
        :return: A temporal random walk sequence.
        :rtype: list
        """
        walk = []

        if initial_edge_sampling_prob is None:
            initial_edge = self._sample_edge(
                start_node, **self.init_edge_kwargs
            )
        else:
            initial_edge = self._sample_edge(
                probabilities=initial_edge_sampling_prob
            )

        walk.append(initial_edge)
        walk_length = 1
        while walk_length < self.max_length:
            previous_edge = walk[-1]

            _, start_node, data = previous_edge
            edge = self._sample_edge(node=start_node,
                                     ts=data['timestamp'],
                                     **self.sample_edge_kwargs)

            if not edge:
                break

            walk.append(edge)
            walk_length += 1

        return walk


class CTDNEWalker:
    """CTDNE Walker implements random walks using CTDNEWalkStrategy."""

    def __init__(self, temporal_graph, init_edge_sampling_fn, edge_sampling_fn,
                 nb_walks_per_node, min_length, max_length, workers,
                 init_edge_kwargs=None, sample_edge_kwargs=None,
                 max_attempts=40):
        """Inits CTDNEWalker.

        :param temporal_graph: Input graph
        :type temporal_graph: nx.MultiGraph
        :param init_edge_sampling_fn: Sampling function for starting walk edge
        :param edge_sampling_fn: General sampling function for choosing
                                 next edge
        :param nb_walks_per_node: Number of random walk sequences starting
                                  from each node
        :type nb_walks_per_node: int
        :param min_length: Minimum length of single random walk sequence
        :type min_length: int
        :param max_length: Maximum length of single random walk sequence
        :type max_length: int
        :param workers: Number of workers
        :type workers: int
        :param init_edge_kwargs: Arguments for init_edge_sampling_fn
        :param sample_edge_kwargs: Arguments for edge_sampling_fn
        :param max_attempts: Maximum number of attempts to obtain temporal
                             random walk sequence
        :type max_attempts: int
        """
        self.walk_args = {
            'temporal_graph': temporal_graph,
            'max_attempts': max_attempts,
            'init_edge_sampling_fn': init_edge_sampling_fn,
            'init_edge_kwargs': init_edge_kwargs,
            'sample_edge_kwargs': sample_edge_kwargs,
            'edge_sampling_fn': edge_sampling_fn,
            'nb_walks_per_node': nb_walks_per_node,
            'max_length': max_length,
        }
        self.min_length = min_length
        self._workers = workers

    def _calc_nb_ctx_windows(self, nb_nodes):
        """Calculates number of context windows to generate.

        :param nb_nodes: Number of nodes
        :type nb_nodes: int
        :return: Number of context windows
        :rtype int
        """
        nb_walks_per_node = self.walk_args.get('nb_walks_per_node')
        min_length = self.min_length
        max_length = self.walk_args.get('max_length')

        return nb_walks_per_node * nb_nodes * (max_length - min_length + 1)

    def _generate_walks_worker_fn(self, walker, target_nb_ctx_windows,
                                  worker_id, node_walks=True):
        """Generates a given number of random walks.

        :param walker: CTDNE Walker
        :type walker: _CTDNEWalkStrategy
        :param target_nb_ctx_windows: Target number of context window
                                      to generate
        :type target_nb_ctx_windows: int
        :param worker_id: ID of process
        :type worker_id: int
        :param node_walks: If true handles random walks as node walks,
                           if false handles random walks as edge walks
                           (default: True)
        :type node_walks: bool
        :return: Random walk sequences
        :rtype: list
        """
        nb_ctx_windows = 0
        min_length = self.min_length

        pbar = tqdm(
            desc=f'Worker: {worker_id}',
            total=target_nb_ctx_windows,
            leave=False
        )

        walks = []
        while target_nb_ctx_windows - nb_ctx_windows > 0:
            edge_rw = walker.get_walk()
            walk_len = len(edge_rw)
            if node_walks:
                edge_rw = convert_edge_walk_to_sequence(edge_rw)
            else:
                walk_len += 1

            if walk_len >= min_length:
                walks.append(edge_rw)
                update_ctx_windows = walk_len - min_length + 1

                nb_ctx_windows += update_ctx_windows
                pbar.update(update_ctx_windows)

        pbar.close()

        return walks

    def generate_walks(self, node_walks=True):
        """Generates random walks parallel on graph.

        :param node_walks: If true handles random walks as node walks,
                           if false handles random walks as edge walks
                           (default: True)
        :type node_walks: bool
        :return: Random walk sequences
        :rtype: list
        """
        target_nb_ctx_windows = self._calc_nb_ctx_windows(
            nb_nodes=len(list(self.walk_args['temporal_graph'].nodes()))
        )

        job_sizes = [int(target_nb_ctx_windows / self._workers)] * self._workers
        job_sizes[-1] += target_nb_ctx_windows - sum(job_sizes)

        walker = _CTDNEWalkStrategy(**self.walk_args)
        with mp.Pool(self._workers) as pool:
            results = pool.starmap(
                self._generate_walks_worker_fn,
                zip(
                    [walker, ] * self._workers,
                    job_sizes,
                    list(range(self._workers)),
                    [node_walks, ] * self._workers
                )
            )

        walks = [walk for worker_walks in results for walk in worker_walks]
        return walks


class CTDNEEmbedding(base.BaseRW):
    """Wrapper for CTDNE embedding."""

    def __init__(self, dimensions, w2v_args, nb_walks_per_node, min_length,
                 max_length, initial_edge_sampling_fn,
                 edge_sampling_fn, init_edge_kwargs=None,
                 sample_edge_kwargs=None, max_attempts=40, workers=24):
        """Inits CTDNEEmbedding.

        :param dimensions: Embedding dimension
        :type dimensions: int
        :param w2v_args: Word2vec arguments
        :type w2v_args: dict
        :param nb_walks_per_node: Number of random walk sequences starting
                                  from each node
        :type nb_walks_per_node: int
        :param min_length: Minimum length of single random walk sequence
        :type min_length: int
        :param max_length: Maximum length of single random walk sequence
        :type max_length: int
        :param initial_edge_sampling_fn: Sampling function for starting
                                         walk edge
        :param edge_sampling_fn: General sampling function for choosing
                                 next edge
        :param init_edge_kwargs: Arguments for init_edge_sampling_fn
        :param sample_edge_kwargs: Arguments for edge_sampling_fn
        :param max_attempts: Maximum number of attempts to obtain temporal
                             random walk sequence
        :type max_attempts: int
        :param workers: Number of workers
        :type workers: int
        """
        super(CTDNEEmbedding, self).__init__(dimensions, w2v_args)

        if isinstance(initial_edge_sampling_fn, str):
            initial_edge_sampling_fn = utils.import_fn(initial_edge_sampling_fn)
        if isinstance(edge_sampling_fn, str):
            edge_sampling_fn = utils.import_fn(edge_sampling_fn)

        self._args = dict(
            nb_walks_per_node=nb_walks_per_node,
            min_length=min_length,
            max_length=max_length,
            init_edge_sampling_fn=initial_edge_sampling_fn,
            init_edge_kwargs=init_edge_kwargs,
            edge_sampling_fn=edge_sampling_fn,
            sample_edge_kwargs=sample_edge_kwargs,
            max_attempts=max_attempts,
            workers=workers,
        )

    def _walk(self, graph):
        """Generates random walk sequences over graph."""
        walker = CTDNEWalker(
            temporal_graph=graph,
            **self._args,
        )
        walks = walker.generate_walks(node_walks=True)
        return walks
