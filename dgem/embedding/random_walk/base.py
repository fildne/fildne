"""Code for base class for Random Walk embedding methods."""
import abc

from dgem.embedding.random_walk import w2v


class BaseRW(abc.ABC):
    """Base class for GCN graph embedding."""

    def __init__(self, dimensions, w2v_args):
        """Inits BaseGCN."""
        self._w2v = w2v.Word2VecModel(**w2v_args, size=dimensions)

    def embed(self, graph):
        """Creates features based on existing data features."""
        walks = self._walk(graph)
        emb = self._w2v.train(walks)

        return emb

    @abc.abstractmethod
    def _walk(self, graph):
        pass
