"""Functions for edge sampling."""
import numpy as np
from scipy.special.cython_special import expit


def edge_uniform(edges, **kwargs):
    """Samples uniformly edge from list of edges.

    :param edges: Input list of edges
    :type edges: list
    :param kwargs: Sampling function keyword arguments
                   Used for compatibility reasons.
    :return: Sampled edge
    :rtype tuple
    """
    idx = np.random.choice(len(edges))
    return edges[idx]


def edge_linear(edges, **kwargs):
    """Linear edge sampling function.

    :param edges: Input list of edges
    :param kwargs: Sampling function keyword arguments
                   Used for compatibility reasons.
    :return: Sampled edge
    :rtype tuple
    """
    sorted_edges = sorted(edges,
                          key=lambda x: x[2]['timestamp'])

    indices = np.arange(1, len(edges) + 1)
    probabilities = indices / np.sum(indices)

    idx = np.random.choice(len(sorted_edges), p=probabilities)
    return sorted_edges[idx]


def edge_exponential(edges, min_time, probabilities=None):
    """Exponential edge sampling function.

    :param edges: Input list of edges
    :type edges: list
    :param min_time: Minimum time
    :param probabilities: Pre-calculated probabilities (default: None)
    :type probabilities: list
    :return: Sampled edge
    :rtype tuple
    """
    t_min = min_time
    if probabilities is None:
        exps = np.array([expit(e[2]['timestamp'] - t_min) for e in edges])
        probabilities = exps / np.sum(exps)

    # if np.sum(exps) > 0:
    idx = np.random.choice(len(edges), p=probabilities)
    return edges[idx]
