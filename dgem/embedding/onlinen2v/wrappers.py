"""Our wrappers for OnlineNode2vec."""
import networkx as nx
import pandas as pd

from dgem.embedding.onlinen2v.w2v_learners import OnlineWord2Vec
from dgem.embedding.onlinen2v.walk_sampling import StreamWalkUpdater, SecondOrderUpdater  # noqa: E501
from dgem.embedding.onlinen2v.online_node2vec_models import LazyNode2Vec
from dgem.embedding.onlinen2v import hash_utils as hu


def snapshots_to_df(snapshots):
    df = pd.DataFrame(columns=['src', 'trg', 'time', 'sid'])
    for sid, snapshot in enumerate(snapshots):
        _df = nx.to_pandas_edgelist(snapshot)[['source', 'target', 'timestamp']]
        _df.columns = ['src', 'trg', 'time']
        _df['sid'] = sid
        df = pd.concat([df, _df], ignore_index=True, sort=True)
    return df[['src', 'trg', 'time', 'sid']]


def create_model(model_name, args, emb_dim, mirror):
    is_decayed = bool(args['is-decayed'])

    half_life = float(args['half-life'])
    is_fw = bool(args['is-fw'])

    lr_rate = float(args['lr-rate'])
    neg_rate = int(args['neg-rate'])
    onlymirror = bool(args['onlymirror'])
    init = args['init']
    exportW1 = False
    interval = float(args['interval'])
    temp_noise = bool(args['temp-noise'])
    loss = args['loss']

    if model_name == 'streamwalk':
        max_length = int(args['max-length'])
        beta = float(args['beta'])
        cutoff = float(args['cutoff'])
        k = int(args['k'])
        updater = StreamWalkUpdater(
            half_life=half_life,
            max_len=max_length,
            beta=beta,
            cutoff=cutoff,
            k=k,
            full_walks=is_fw
        )
    else:
        assert model_name == 'sor'
        hash_num = int(args['hash-num'])
        hash_type = args['hash-type']
        in_edges = float(args['in-edges'])
        out_edges = float(args['out-edges'])
        incr_condition = bool(args['incr-condition'])
        if hash_type == "mod":
            hash_gen = hu.ModHashGenerator()
        elif hash_type == "mul":
            hash_gen = hu.MulHashGenerator()
        elif hash_type == "map":
            hash_gen = hu.MapHashGenerator()
        else:
            raise RuntimeError("Invalid hash config!")
        updater = SecondOrderUpdater(
            half_life=half_life,
            num_hash=hash_num,
            hash_generator=hash_gen,
            in_edges=in_edges,
            out_edges=out_edges,
            incr_condition=incr_condition
        )
    learner = OnlineWord2Vec(
        embedding_dims=emb_dim,
        loss=loss,
        lr_rate=lr_rate,
        neg_rate=neg_rate,
        mirror=mirror,
        onlymirror=onlymirror,
        init=init,
        exportW1=exportW1,
        window=2,
        interval=interval,
        temporal_noise=temp_noise,
        use_pairs=(not is_fw),
    )

    online_n2v = LazyNode2Vec(updater, learner, is_decayed)

    return online_n2v


def init(on2v, graph_df):
    partial_data = on2v.filter_edges(graph_df)
    on2v.sum_train_time = 0.0
    on2v.sampled_pairs = []

    return partial_data


def embed(on2v, snapshot_data):
    snapshot_id = snapshot_data.sid.min()

    for edge_num, row in snapshot_data.iterrows():
        current_time, sid = row["time"], row["sid"]
        source, target = str(int(row["src"])), str(int(row["trg"]))

        if sid > snapshot_id:
            on2v.lazy_train_model(current_time)
            snapshot_id = sid

        on2v.node_last_update[source] = current_time
        on2v.node_last_update[target] = current_time
        # update & sample node pairs for model training

        new_pairs = on2v.updater.process_new_edge(
            source,
            target,
            current_time
        )
        on2v.sampled_pairs += new_pairs

    current_time = snapshot_data['time'].max()
    on2v.lazy_train_model(current_time)

    emb = on2v.my_get_features(current_time)
    return emb
