"""Module for custom functions dedicated for processing networkx graphs."""
import itertools
import math
from itertools import chain
from typing import List

import networkx as nx  # noqa: F401
import numpy as np  # noqa: F401


def get_edges(graph):
    """Returns all edges of the given graph.

    :param graph: Input graph
    :type: graph: nx.Graph
    :return: List of edges
    :rtype: list
    """
    all_edges = [
        (graph.edges(node, data=True))
        for node in list(graph.nodes())
    ]
    edges = list(chain.from_iterable(all_edges))
    return edges


def sort_edges_by_time(
    graph: nx.Graph,
    timestamp_key: str = 'timestamp'
) -> List:
    """Returns sorted edges by time.

    :param graph: Input_graph
    :type graph: nx.Graph
    :param timestamp_key: Key in graph data describing timestamps
                          (default: timestamp)
    :type: timestamp_key: str
    :return: sorted edgelist
    :rtype list
    """
    sorted_list = sorted(
        graph.edges.data(),
        key=lambda edge: edge[2][timestamp_key]
    )
    return sorted_list


def split_edges(edges, split_ratio=0.5, data=False):
    """Splits list of edges into two slices.

    :param edges: List of edges
    :type edges: list
    :param split_ratio: Ratio of list split (default: 0.5)
    :type split_ratio: float
    :param data: Denotes whether edge data will be copied
    :type data: bool
    :return: Two list slices
    :rtype: (list, list)
    """
    left, right = split_list(edges, split_ratio)
    if not data:
        left = [(edge[0], edge[1]) for edge in left]
        right = [(edge[0], edge[1]) for edge in right]
    return left, right


def split_list(input_list, split_ratio):
    """Splits list into two chunks.

    :param input_list: Input list
    :type input_list: list
    :param split_ratio: Split ratio of list
    :type split_ratio: float
    :return: Two chunks of input_list
    :rtype (list, list)
    """
    margin_item = math.ceil(len(input_list) * split_ratio)

    left = input_list[:margin_item]
    right = input_list[margin_item:]
    return left, right


def split_graph(
    graph: nx.Graph,
    num_batches: int,
    split_type: str = 'time',
    cumulative: bool = False
) -> List[nx.Graph]:
    """Splits graph into stream of graphs based on edge timestamps.

    :param graph: Full attributed graph
    :param num_batches: Number of output batches
    :param split_type: Split on time vs number of edges
    :param cumulative: Cumulative vs non-cumulative batches
    :return: Stream of graphs
    """
    def grouper(num: int, iterable: list):
        """Groups `iterable` into `n` batches of equal size."""
        return np.array_split(iterable, num)

    k = get_edge_timestamp_attr_name(graph)
    edges = sort_edges_by_time(graph, k)
    if split_type == 'time':
        ks = get_unique_timestamps(edges, k)
        cuts = extract_cut_points(ks, num_batches)
        return [
            nx.from_edgelist(
                list(filter(
                    lambda x: (x[2][k] <= cut) and
                              (True if cumulative else cuts[idx] < x[2][k]),
                    edges,
                )),
                create_using=type(graph)
            ) for idx, cut in enumerate(cuts[1:])
        ]
    else:
        if cumulative:
            batches = grouper(num_batches, edges)
            return [nx.from_edgelist(
                itertools.chain(*batches[:idx + 1]),
                create_using=type(graph)
            )
                for idx in range(len(batches))]
        else:
            return [nx.from_edgelist(
                batch,
                create_using=type(graph)
            )
                for batch in grouper(num_batches, edges)]


def get_edge_timestamp_attr_name(graph: nx.Graph) -> str:
    """Returns timestamp edge attribute name."""
    return list(
        list(graph.edges.data())[0][2].keys()
    )[-1]  # TODO: Timestamp is (for now) the last key, refactor this ASAP


def get_unique_timestamps(edges: List, attribute_name: str) -> np.ndarray:
    """Returns list of unique timestamps.

    :param edges: Input list of edges
    :param attribute_name: Name of timestamp attribute
    """
    return np.unique(list(map(lambda x: x[2][attribute_name], edges)))


def extract_cut_points(timestamps, num_batches):
    """Extracts limiting timestamps for batches.

    :param timestamps: Sorted unique timestamps in the dataset
    :param num_batches: Number of output batches in graph stream
    :return: Sorted batch borders
    """
    return ([0] +
            [a[-1] for a in np.array_split(timestamps, num_batches)][:-1] +
            [timestamps.max()])
