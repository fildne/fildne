"""Module for nodes scoring functions and it's utilities."""
import numpy as np


def basic_scoring_function(x, y):
    """Sample implementation of a basic scoring function.

    :param x: Previous node degree
    :type x: int
    :param y: Current node degree
    :type y: int
    :return: node pair score
    :rtype: float
    """
    return (np.power(np.abs(x - y), 1) *
            (np.pi / 2 - np.arctan(np.maximum(x, y))))


def get_nodes_scores_for_current_snapshot(
        prev_snapshot_activity,
        snapshot_activity,
        scoring_fn
):
    """Returns nodes scores of a given snapshot with given scoring function.

    :param prev_snapshot_activity: Previous snapshot_activity
    :type prev_snapshot_activity: dict
    :param snapshot_activity: Input snapshot_activity
    :type snapshot_activity: dict
    :param scoring_fn: Nodes scoring function
    :return: Dictionary containing scores for nodes
    :rtype: dict
    """
    return {
        node: scoring_fn(prev_node_degree, snapshot_activity[node])
        for node, prev_node_degree in prev_snapshot_activity.items()
        if node in snapshot_activity.keys()
    }


def get_reference_nodes(scores_dict, nb_ref_nodes):
    """Returns reference nodes.

    :param scores_dict: A dictionary containing scores for each node
    :type scores_dict: dict
    :param nb_ref_nodes: Number of reference nodes
    :type nb_ref_nodes: int
    :return: List of reference nodes
    :rtype list
    """
    reference_nodes = sorted(scores_dict.keys(), key=scores_dict.get)
    reference_nodes = reference_nodes[:int(nb_ref_nodes)]
    reference_nodes = [str(node) for node in reference_nodes]

    return reference_nodes
