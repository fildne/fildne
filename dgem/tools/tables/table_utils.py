"""Utilities for tables generation."""
import numpy as np
import pandas as pd


def get_mean_over_runs(input_df, methods):
    """Returns mean value overruns."""
    return input_df.loc[
        methods
    ].apply(pd.Series).applymap(
        lambda x: np.mean(x)
    )


def rank_df(input_df, methods, ascending=False):
    """Ranks given dataframe."""
    run_mean = get_mean_over_runs(input_df, methods)
    ranked_df = run_mean.groupby(level=1).rank(
        ascending=ascending
    ).mean(axis=1).to_frame().reset_index().pivot(
        columns='dataset', index='method'
    )
    return ranked_df


def highlight_max(s):
    """Highlights the maximum in a Series yellow."""

    def get_mean(x):
        if isinstance(x, tuple):
            return x[0]
        return x

    s = s.replace('-', np.nan)
    mean_s = s.apply(get_mean)

    is_max = mean_s == mean_s.max()
    return ['background-color: yellow' if v else '' for v in is_max]


def get_task_results_data(path, metric):
    """Returns task results data."""
    all_results = pd.read_pickle(path)
    metric_data = all_results[metric].reset_index()
    if metric == 'ec':
        metric_data = metric_data[
            (metric_data.dataset == 'bitcoin-otc') |
            (metric_data.dataset == 'bitcoin-alpha')
        ]
    elif metric == 'gr-mAP' or metric == 'gr-distortion':
        metric_data = metric_data[metric_data.method != 'tne']
    elif metric == 'memory-measurement':
        metric_data = metric_data.dropna()

    return metric_data.set_index(['method', 'dataset'])[metric]


def get_mean_df(metric_data, metric):
    """Returns df with mean results."""
    return metric_data.to_frame().applymap(
        lambda x: np.mean([np.mean(it) for it in x.values()])
    ).reset_index().pivot(
        columns='dataset', index='method', values=metric
    ).copy()


def mean_std_tuple_to_str(x):
    """Converts mean, std to str."""
    if isinstance(x, tuple):
        return f'${x[0]} ± {x[1]}$'
    return '-'


def highlight_green(val):
    """Highlights green."""
    color = 'green' if val > 1.0 else 'red'
    return 'color: %s' % color


def get_comparison_method(idx, compare_method=None):
    """Parses comparison method from idx str."""
    if not compare_method:
        compare_method = idx[0].split('_')[-1]
    return compare_method, idx[1]


def comparison_function(x, mean_df, comp_method=None):
    """Comparison fn."""
    comp_method = get_comparison_method(
        x.name, comp_method
    )
    return x / mean_df.loc[comp_method]


def fildne_comparison(input_df, methods, compare_method=None):
    """FILDNE comparison fn."""
    out_df = input_df.loc[methods].apply(
        lambda x: comparison_function(x, compare_method),
        axis=1
    ).mean(axis=1).reset_index().pivot(
        columns='dataset', index='method'
    )
    if not compare_method:
        out_df.index = [
            f"{it} vs {it.split('_')[-1]}"
            for it in out_df.index
        ]
    else:
        out_df.index = [
            f"{it} vs {compare_method}"
            for it in out_df.index
        ]
    return out_df
