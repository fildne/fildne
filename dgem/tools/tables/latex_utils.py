"""Utilities for latex tables."""
import numpy as np


def reformat_pd_latex_output(
        latex: str, base_methods_count: int, mean_rank_column: bool = False
) -> str:
    """Corrects pandas latex output."""
    lines = latex.splitlines()
    lines[2] = '&  bitcoin &  bitcoin &  fb &  fb & ' \
               ' enron & hypertext09 & radoslaw \\\\'
    if mean_rank_column:
        lines[2] = '&  bitcoin &  bitcoin &  fb &  fb & ' \
                   ' enron & hypertext09 & radoslaw & mean rank \\\\'
        lines[3] = '& alpha & otc & forum & messages & ' \
                   'employees & email &\\\\'
    else:
        lines[2] = "&  bitcoin &  bitcoin &  fb &  fb &  " \
                   "enron & hypertext09 & radoslaw \\\\"
        lines[3] = "& alpha & otc & forum & messages " \
                   " employees & email  \\\\"

    headline_end_line = 4
    indexes = [
        headline_end_line + (base_methods_count * 2) - (base_methods_count - 1),
        headline_end_line + (base_methods_count * 3) - (base_methods_count - 2),
        headline_end_line + (base_methods_count * 4) - (base_methods_count - 3),
    ]
    if lines[4] != r'\midrule':
        lines.insert(4, r'\midrule')
    for idx in indexes:
        lines.insert(idx, r'\midrule')

    return '\n'.join(
        lines
    ).replace('NaN', '---').replace('±', r'\pm').replace(r'\$', '$')


def fill_blank_items(x, sign_plus=False):
    """Fills blank items."""
    values = []
    for it in x:
        if isinstance(it, tuple):
            it = it[0]
        try:
            value = float(it)
        except:  # noqa
            if not sign_plus:
                value = -1_000_000
            else:
                value = 1_000_000

        if np.isnan(value):
            if not sign_plus:
                value = -1_000_000
            else:
                value = 1_000_000

        values.append(value)

    return np.array(values)


def get_top_score_underlined(x, metric):
    """Underlines top score accross sd."""
    x_name = x.name
    if x_name != 'mean rank':

        if metric in ['lp', 'ec', 'gr-mAP']:
            x = fill_blank_items(x.values, sign_plus=False)
            max_id = np.argmax(x)
        else:
            x = fill_blank_items(x.values, sign_plus=True)
            max_id = np.argmin(x)
    else:
        x = x.values

        max_id = -1

    output = []
    for i in range(len(x)):
        if i == max_id:
            output.append(f'$\\underline{{{x[max_id]:.2f}}}$')
        elif -1_000_000 < x[i] < 1_000_000:
            output.append('%.2f' % x[i])
        else:
            output.append('$\\times$')
    return output


def get_top_tuple_underlined(x, metric):
    """Underlines top score accross sd."""
    if x.name != 'mean rank':
        x = x.values

        if metric in ['lp', 'ec', 'gr-mAP']:
            mean = fill_blank_items(x.copy(), sign_plus=False)
            max_id = np.argmax(mean)
        else:
            mean = fill_blank_items(x.copy(), sign_plus=True)
            max_id = np.argmin(mean)

    else:
        x = x.values
        max_id = -1

    output = []
    for i in range(len(x)):
        if i == max_id:
            output.append(f'$\\underline{{{x[i][0]} ± {x[i][1]}}}$')
        elif isinstance(x[i], tuple):
            output.append(f'${x[i][0]} ± {x[i][1]}$')
        else:
            output.append('$\\times$')
    return output


def get_top_method_rank_bold(x):
    """Bolds top methods ranks."""
    if x.name == 'mean rank':
        x = fill_blank_items(x.values, sign_plus=True)
        max_ids = x.argsort()[:3]
    else:
        return x

    output = []
    for i in range(len(x)):
        if i in max_ids:
            output.append(f'$\\textbf{{{x[i]:.2f}}}$')
        elif x[i] < 1_000_000:
            output.append('%.2f' % x[i])
        else:
            output.append('$\\times$')
    return output


def get_top_method_bold(line):
    """Bolds methods names with top ranks."""
    if 'textbf' in line:
        chunked_line = line.split('&')
        chunked_line[0] = f'$\\textbf{{{chunked_line[0].strip()}}}$'
        line = '&'.join(chunked_line)

    return line


def get_styled_table(input_table, metric, base_methods):
    """Returns styled table."""
    table = input_table.copy().apply(
        lambda x: get_top_score_underlined(x, metric), axis=0
    ).apply(
        lambda x: get_top_method_rank_bold(x), axis=0
    ).to_latex()
    latex_str = reformat_pd_latex_output(
        latex=table,
        base_methods_count=len(base_methods),
        mean_rank_column=True
    ).replace('textbackslash ', '').replace('\{', '{').replace(r'\}', '}')  # noqa
    latex_lines = [get_top_method_bold(line) for line in latex_str.split('\n')]
    for line in latex_lines:
        print(line)


def get_styled_last_snapshot_table(input_table, metric, base_methods):
    """Returns styled table."""
    table = input_table.copy().apply(
        lambda x: get_top_tuple_underlined(x, metric), axis=0
    ).to_latex()
    latex_str = reformat_pd_latex_output(
        latex=table,
        base_methods_count=len(base_methods),
        mean_rank_column=False
    ).replace('textbackslash ', '').replace('\{', '{').replace('\}', '}')  # noqa

    print(latex_str)
