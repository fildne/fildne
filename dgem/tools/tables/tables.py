"""Script for generating tables."""

import numpy as np
from . import table_utils as tu


def percentage_style(x):
    """Percantage style fn."""
    return np.round(x * 100, 2)


def numeric_value_style(x, decimal_places=2):
    """Numeric_value style fn."""
    return np.round(x, decimal_places)


def get_all_methods(methods, baselines):
    """Returs all methods."""
    return [
        *methods,
        *[f'basic-fildne_{it}' for it in methods],
        *[f'generalised-fildne_{it}' for it in methods],
        *baselines
    ]


def get_metric_styling(metric):
    """Returns metric styling fn with args."""
    args = {}
    if metric in ['lp', 'ec', 'gr-mAP']:
        styling_fn = percentage_style
    else:
        styling_fn = numeric_value_style

    if metric in ['gr-distortion']:
        args['decimal_places'] = 3

    return styling_fn, args


def get_mean_results_table(
        input_df, metric_data, methods, methods_names_mapping, metric
):
    """Returns dataframe with mean aggregated results."""
    styling_fn, args = get_metric_styling(metric)

    if metric in ['lp', 'ec', 'gr-mAP']:
        ascending = False
    else:
        ascending = True

    df = input_df.loc[methods].copy()
    df = df.applymap(lambda x: styling_fn(x, **args))
    df = df.fillna('-')
    df['mean rank'] = tu.rank_df(
        metric_data, methods, ascending=ascending
    ).mean(axis=1).round(2)
    df.index = [methods_names_mapping[it] for it in df.index]
    return df


def get_last_snapshot_results_table(
        metric_data, methods, methods_names_mapping, metric
):
    """Return table with last snapshot results."""
    styling_fn, args = get_metric_styling(metric)

    df = metric_data.to_frame().applymap(
        lambda x: (
            styling_fn(np.mean(x['F_0_9']), **args),
            styling_fn(np.std(x['F_0_9']), **args)
        )
    )
    df = df.reset_index().pivot(index='method', columns='dataset')
    df = df.loc[methods]
    df.index = [methods_names_mapping[it] for it in df.index]

    return df
