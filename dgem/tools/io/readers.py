"""Module containing various file readers."""
import os
import pickle
from typing import List, Dict

import networkx as nx


def read_pickle(filepath):
    """Reads python pickle.

    :param filepath: Path of the file
    :type filepath: str
    :return: Read object
    :rtype: object
    """
    with open(filepath, 'rb') as file:
        obj = pickle.load(file)
    return obj


def read_nx_multi_graph_from_edgelist(filepath, delimiter=' ',
                                      create_using=nx.MultiGraph(), data=True):
    """Reads networkx MultiGraph from edgelist file.

    :param filepath: Path of the file
    :type filepath: str
    :param delimiter: edges delimiter (default: ' ')
    :type delimiter: str
    :param create_using: Networkx graph constructor (default: nx.MultiGraph)
    :param data: Whether edge data is used (default: True)
    :type data: str
    :return: Networkx MultiGraph
    :rtype: nx.MultiGraph
    """
    kwargs = {
        'delimiter': delimiter,
        'create_using': create_using,
        'nodetype': int,
    }

    if data:
        kwargs['data'] = data

    return nx.read_edgelist(filepath, **kwargs)


def read_nx_gpickle(filepath):
    """Reads networkx graph pickle file.

    :param filepath: Path of the file
    :type filepath: str
    :return: Networkx Graph
    :rtype: nx.Graph
    """
    return nx.read_gpickle(filepath)


def read_snapshots_list(path: str) -> List[nx.Graph]:
    """Reads graph pickled snapshots.

    :param path: Input path
    """
    nb_snapshots = len(os.listdir(path))

    snapshots = [
        read_nx_gpickle(
            os.path.join(path, f'G_{snap_id}_{snap_id + 1}')
        )
        for snap_id in range(nb_snapshots)
    ]
    return snapshots


def read_snapshots(path: str) -> Dict[str, nx.Graph]:
    """Reads graph pickled snapshots.

    :param path: Input path
    """
    snapshots = dict()

    for snap_key in os.listdir(path):
        snapshots[snap_key] = read_nx_gpickle(
            os.path.join(path, snap_key)
        )
    return snapshots
