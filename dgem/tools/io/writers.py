"""Module containing various file writers."""
import pickle

import networkx as nx


def export_nxgraph_to_gpickle(nx_graph, filepath):
    """Exports networkx graph to gpickle file.

    :param nx_graph: Input Graph
    :type nx_graph: nx.Graph
    :param filepath: Export path
    :type filepath: str
    """
    nx.write_gpickle(nx_graph, filepath)


def export_obj_to_pickle(obj, filepath):
    """Export object to python pickle.

    :param obj: Object
    :type obj: object
    :param filepath: Export path
    :type filepath: str
    """
    with open(filepath, 'wb') as file:
        pickle.dump(obj, file)
