"""Module for definition of LoadSaveMixin."""
import pickle


class LoadSaveMixin:
    """Class mixin for loading and saving the whole class using pickle."""

    @classmethod
    def load(cls, filename):
        """Loads the pickled object from a file.

        For a sample class `T`, this method should be used as follows:
        `obj = T.load(filename)`. This function checks also, whether the loaded
        object has the type of the calling class.

        :param filename: Name of the file, where the object was pickled to.
        :type filename: str
        :return: Unpickled object
        """
        with open(filename, 'rb') as fp:
            obj = pickle.load(fp)
            assert isinstance(obj, cls)
            return obj

    def save(self, filename):
        """Saves an object to a file.

        :param filename: Name of the file, where the object should be
                         pickled to.
        :type filename: str
        """
        with open(filename, 'wb') as fp:
            pickle.dump(self, fp)
