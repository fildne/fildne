"""Statistical functions."""
import numpy as np

from .utils import add_eps


def jeffrey_divergence(p, q, items_count):
    """Calculates Jeffrey Divergence between two histograms.

    :param p: Input histogram
    :param q: Input histogram
    :param items_count: Number of items
    :return: Jeffrey divergence value
    :rtype: float
    """
    jd = 0.0

    for i in range(items_count):
        jd += p[i] * np.log(add_eps(p[i]) / add_eps(q[i]))
        jd += q[i] * np.log(add_eps(q[i]) / add_eps(p[i]))

    return jd
