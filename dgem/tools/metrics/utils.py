"""Utilities for calculating metrics."""
import numpy as np


def add_eps(input_array, dtype=np.float32):
    """Adds epsilon to given array.

    :param input_array: Input array
    :type input_array: np.ndarray
    :param dtype: Floating point datatype (default: np.float32)
    :return: Array with added epsilon
    """
    return input_array + np.finfo(dtype).eps
