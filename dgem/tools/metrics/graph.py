"""Graph metrics."""
import numpy as np
from scipy.stats import entropy

from dgem.tools.metrics.utils import add_eps


def calculate_node_entropy(node_connections):
    """Calculates entropy of the node.

    :param node_connections: Array of node connections
    :type node_connections: np.ndarray
    :return: Calculated entropy
    :rtype float
    """
    norm = node_connections / add_eps(np.sum(node_connections), np.float32)
    node_entropy = entropy(norm)
    return node_entropy


def calculate_graph_entropy(adj_matrix):
    """Calculate entropy of the graph.

    :param adj_matrix: Adjacency matrix of graph
    :return: List of graph nodes entropies
    :rtype: list
    """
    entropies = []
    for node_id in range(len(adj_matrix)):
        node_entropy = calculate_node_entropy(adj_matrix[node_id])
        entropies.append(node_entropy)

    return entropies
