"""Distance computations."""
import numpy as np

from . import similarity


def euclidean(u, v):
    """Calculates euclidean distance between array u and array(s) v.

    Calculates chunk of distance matrix. For obtaining whole matrix use:
    sklearn.metrics.pairwise_distances.
    :param u: 1-D input array
    :type u: np.ndarray
    :param v: 1-D or 2-D input array
    :type v: np.ndarray
    :return: The euclidean distance between arrays u and v
    :rtype: np.ndarray
    """
    dim_v = len(v.shape)
    assert len(u.shape) == 1, "Array u should be 1-D"
    assert dim_v in [1, 2], "Array v should be 1-D or 2-D"

    return np.sqrt((np.subtract(u, v) ** 2).sum(axis=dim_v - 1))


def cosine(u, v):
    """Calculates cosine distance between array u and array(s) v.

    Calculates chunk of distance matrix. For obtaining whole matrix use:
    sklearn.metrics.pairwise_distances.
    :param u: 1-D input array
    :type u: np.ndarray
    :param v: 1-D or 2-D input array
    :type v: np.ndarray
    :return: The cosine distance between arrays u and v
    :rtype: np.ndarray
    """
    return 1 - similarity.cosine_similarity(u, v)
