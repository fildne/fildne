"""Similarity functions."""

import numpy as np


def cosine_similarity(u, v):
    """Calculates cosine similarity between array u and array(s) v.

    :param u: 1-D input array
    :type u: np.ndarray
    :param v: 1-D or 2-D input array
    :type v: np.ndarray
    :return: The cosine similarity between arrays u and v
    :rtype: np.ndarray
    """
    dim_v = len(v.shape)
    assert len(u.shape) == 1, "Array u should be 1-D"
    assert dim_v in [1, 2], "Array v should be 1-D or 2-D"

    return (u * v).sum(axis=dim_v - 1) / np.sqrt(
        np.power(v, 2).sum(axis=dim_v - 1) * (np.power(u, 2).sum(axis=0))
    )
