"""Miscellaneous utilities function module."""
import importlib


def import_fn(module_str):
    """Imports function or class from given module."""
    module, fn = module_str.rsplit('.', maxsplit=1)
    fn = getattr(importlib.import_module(module), fn)
    return fn
