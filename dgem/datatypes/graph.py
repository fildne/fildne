"""Module for custom wrappers of graph data types."""
from collections import defaultdict

import networkx as nx  # noqa: F401


class GraphKeyedModel:
    """A base class for graphs.

    GraphKeyedModel stores nodes as keys and edges as values in python dict.
    """

    def __init__(self, edge_dict):
        """Inits GraphKeyedModel with dict.

        :param edge_dict: A dictionary containing the mapping of nodes to
                          edges, that start in a particular node.
        :type edge_dict: dict
        """
        self.edge_dict = edge_dict

    def get_edges(self, node):
        """Returns list of edges starting in a given node.

        :param node: Id of the node
        :type node: str
        :return: List of edges
        :rtype: list
        """
        return self.edge_dict.get(node)

    @classmethod
    def from_nx_graph(cls, graph):
        """Converts networkx Graph to GraphKeyedModel.

        :param graph: NetworkX Graph
        :type graph: nx.Graph
        :return: GraphKeyedModel object
        :rtype: GraphKeyedModel
        """
        edges = list(graph.edges(data=True))
        edge_dict = defaultdict(list)

        if graph.is_directed():
            for edge in edges:
                edge_dict[edge[0]].append(edge)
        else:
            for edge in edges:
                edge_dict[edge[0]].append(edge)
                edge_dict[edge[1]].append((edge[1], edge[0], edge[2]))

        return cls(edge_dict=edge_dict)


class TemporalGraphKeyedModel(GraphKeyedModel):
    """Extension of GraphKeyedModel to proper handling of temporal graph data.

    It adds a method, that allows the retrieval of all edges that start in
    a given node and whose timestamps are after a provided timestamp.
    """

    def __init__(self, edge_dict):
        """Inits TemporalGraphKeyedModel with dict.

        :param edge_dict: A dict containing the mapping of nodes to edges,
                          that start in a particular node.
        :type edge_dict: dict
        """
        super(TemporalGraphKeyedModel, self).__init__(edge_dict)

    def get_temporal_edges(self, node, ts):
        """Returns list of edges with timestamps that are greater than ts.

        :param node: Id of the node
        :type node: str
        :param ts: Input timestamp
        :type ts: int
        :return: List of edges with timestamps > input_timestamp
        :rtype: list
        """
        edges = self.get_edges(node)
        if not edges:
            return None

        return [edge for edge in edges if edge[2]['timestamp'] > ts]
