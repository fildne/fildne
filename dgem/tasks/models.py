"""Module for handling models for classification over edges."""
import abc

import numpy as np
from sklearn import linear_model as sk_lm
from sklearn import ensemble

from dgem.embedding import edge as ee
from dgem.embedding.base import KeyedModel  # noqa: F401
from dgem.tools.metrics import ClassificationReport


class AbstractEdgeClassificationModel(abc.ABC):
    """Abstract classification model for edge classification."""

    @abc.abstractmethod
    def __init__(self, clf, embedding, operator=ee.hadamard_op):
        """Inits model with embedding data.

        :param embedding: Input keyed embedding data
        :type embedding: KeyedModel
        :param operator: Edge embedding operator
        """
        self._clf = clf
        self._emb = embedding
        self._ee_op = operator

    def fit(self, x, y):
        """Fits model with data.

        :param x: Input edges
        :type x: np.ndarray
        :param y: Input labels
        :type y: np.ndarray
        """
        self._clf.fit(X=self._to_edge_emb(x), y=y.transpose()[0])

    def predict(self, x):
        """Predicts the existence of given edges.

        :param x: Input edges
        :type x: np.ndarray
        :return: Predictions
        :rtype: np.ndarray
        """
        return self._clf.predict(self._to_edge_emb(x))

    def predict_proba(self, x):
        """Predicts scores.

        :param x: Input edges
        :type x: np.ndarray
        :return: Predictions
        :rtype: np.ndarray
        """
        return self._clf.predict_proba(self._to_edge_emb(x))

    def _to_edge_emb(self, x, ):
        """Returns embedding of the edge.

        :param x: Input edges
        :rtype x: list
        :return: List of edge embeddings
        :rtype: np.ndarray
        """
        return np.array(ee.calculate_edge_embedding(
            self._emb, x, self._ee_op
        ))

    def validate(self, x, y):
        """Validates data and returns classification report.

        :param x: Input edges
        :type x: np.ndarray
        :param y: Input labels
        :type y: np.ndarray
        :return: Classification metrics
        :rtype ClassificationReport
        """
        return ClassificationReport(
            y_true=y,
            y_pred=self.predict(x),
            y_score=self.predict_proba(x)[:, 1]
        )


class LogisticRegressionModel(AbstractEdgeClassificationModel):
    """Edge Classification model based on logistic regression."""

    def __init__(self, embedding, args=None, operator=ee.hadamard_op):
        """Inits model with embedding data.

        :param embedding: Input keyed embedding data
        :type embedding: KeyedModel
        :param operator: Edge embedding operator
        """
        args = args if args else {}
        super(LogisticRegressionModel, self).__init__(
            clf=sk_lm.LogisticRegression(solver='liblinear', **args),
            embedding=embedding,
            operator=operator
        )


class RandomForestModel(AbstractEdgeClassificationModel):
    """Edge classification model based on random forest."""

    def __init__(self, embedding, operator=ee.hadamard_op):
        """Inits model with embedding data.

        :param embedding: Input keyed embedding data
        :type embedding: KeyedModel
        :param operator: Edge embedding operator
        """
        super(RandomForestModel, self).__init__(
            clf=ensemble.RandomForestClassifier(n_estimators=100),
            embedding=embedding,
            operator=operator
        )
