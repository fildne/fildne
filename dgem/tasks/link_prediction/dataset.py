"""Module for custom utilities for dataset processing in link prediction."""
import itertools
from copy import deepcopy
import numpy as np
import sklearn.utils as sk_utils

import networkx as nx

from dgem.tools.graph import nxtools


def sample_negative_edges(nodes, graph, nb_samples):
    """Samples number of non-existing edges from given graph."""
    u = deepcopy(list(nodes))
    v = deepcopy(list(nodes))
    np.random.shuffle(u)
    np.random.shuffle(v)

    neg_edges = []
    if not nb_samples:
        raise RuntimeError('nb_samples is equal to 0')
    for edge in itertools.product(u, v):
        # Do not include existing edges
        if graph.has_edge(edge[0], edge[1]):
            continue

        # For directed graphs, when using Hadamard operator, we MUST NOT
        # include an edge if the reversed one exists in the graph, because it
        # would be noise for the classification model.
        # TODO: if case we change Hadamard operator this must be fixed
        if (isinstance(graph, (nx.DiGraph, nx.MultiDiGraph)) and
           graph.has_edge(edge[1], edge[0])):
            continue

        neg_edges.append(edge)
        if len(neg_edges) == nb_samples:
            break

    return neg_edges


def mk_link_prediction_dataset(
        graph, split_proportion, prev_nodes=None
):
    """Returns link prediction dataset from graph."""
    def post_process(x_pos, x_neg):
        x_np = np.concatenate((np.array(x_pos), np.array(x_neg)))
        y_pos = np.ones((len(x_pos), 1), dtype=int)
        y_neg = np.zeros((len(x_neg), 1), dtype=int)
        y_np = np.concatenate((y_pos, y_neg))

        x, y = sk_utils.shuffle(x_np, y_np, random_state=0)
        return x, y

    def filter_edge(edge):
        return edge[0] in prev_nodes and edge[1] in prev_nodes

    edges = nxtools.sort_edges_by_time(graph)
    if prev_nodes:
        edges = [it for it in edges if filter_edge(it)]
        av_nodes = set(graph.nodes()).intersection(set(prev_nodes))
    else:
        av_nodes = set(graph.nodes())

    train_data, test_data = nxtools.split_edges(edges, split_proportion)
    neg_train_data = sample_negative_edges(
        nodes=av_nodes,
        graph=graph,
        nb_samples=len(train_data)
    )
    neg_test_data = sample_negative_edges(
        nodes=av_nodes,
        graph=graph,
        nb_samples=len(test_data)
    )

    x_train, y_train = post_process(train_data, neg_train_data)
    x_test, y_test = post_process(test_data, neg_test_data)
    return x_train, y_train, x_test, y_test
