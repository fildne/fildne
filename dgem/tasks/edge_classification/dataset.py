"""Module for creation of edge classification dataset."""
import numpy as np

from dgem.tools.graph import nxtools


def mk_edge_classification_dataset(graph, prev_nodes, split_proportion):
    """Returns edge classification dataset from graph."""
    def parse_edge_data(ed):
        x = np.array([(e[0], e[1]) for e in ed])
        y = np.sign([e[2]['weight'] for e in ed])
        y[y == -1] = 0
        y = y.reshape(-1, 1)
        return x, y

    def filter_edge(edge):
        return edge[0] in prev_nodes and edge[1] in prev_nodes

    edges = nxtools.sort_edges_by_time(graph)
    edges = [it for it in edges if filter_edge(it)]

    train_data, test_data = nxtools.split_edges(
        edges=edges,
        split_ratio=split_proportion,
        data=True
    )
    x_train, y_train = parse_edge_data(train_data)
    x_test, y_test = parse_edge_data(test_data)

    return x_train, y_train, x_test, y_test
